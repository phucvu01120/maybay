/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BUS;

import BLL.BCodeKM;
import DTO.CodeKM;
import java.util.ArrayList;

/**
 *
 * @author PHUC
 */
public class CodeKMBUS {
    public static ArrayList<CodeKM> list_Code;
    public CodeKMBUS(){
        
    }
    public void getListCodeKM(String condition, String orderBy) throws Exception{
        if(list_Code == null){
            list_Code = new ArrayList<>();
        }
        BCodeKM data = new BCodeKM();
        list_Code = data.getCodeKM(condition, orderBy);
    }
    public void getListCodeKM(String condition) throws Exception{
        this.getListCodeKM(condition, null);
    }
    public void getListCodeKM() throws Exception{
        this.getListCodeKM(null);
    }
    public CodeKM getCodeKMByMaCode(String code){
        for(CodeKM codekm : list_Code){
            if(codekm.getMaCode().compareToIgnoreCase(code)==0){
                return codekm;
            }
        }
        return null;
    }
    public String checkCodeKM(String code){
        CodeKM codekm=this.getCodeKMByMaCode(code);
        
        if(codekm!=null){
            if(codekm.getTinhTrang().equals("Đã sử dụng")){
                return "Mã khuyến mãi đã được sử dụng";
            }
            if(codekm.getTinhTrang().equals("Đã hết hạn")){
                return "Chương trình khuyến mãi đã hết hạn";
            }
            if(codekm.getTinhTrang().equals("Chưa diễn ra")){
                return "Chương trình khuyến mãi chưa diễn ra";
            }
        } 
        else {
            return "Mã khuyến mãi không tồn tại";
        }
        return "OK";
    }
}
