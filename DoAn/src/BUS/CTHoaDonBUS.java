/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BUS;

import BLL.CTHoaDonDAO;
import DTO.CTHoaDonDTO;
import java.util.ArrayList;

/**
 *
 * @author PHUC
 */
public class CTHoaDonBUS {
    public static ArrayList<CTHoaDonDTO> list_CTHD;
    public CTHoaDonBUS(){
        
    }
    public void getListCTHD(String condition, String orderBy){
        if(list_CTHD == null){
            list_CTHD = new ArrayList<>();
        }
        CTHoaDonDAO data = new CTHoaDonDAO();
        list_CTHD = data.getCTHoaDons(condition, orderBy);
    }
    public void getListCTHD(String condition){
        this.getListCTHD(condition, null);
    }
    public void getListCTHD(){
        this.getListCTHD(null);
    }
    public void Insert(CTHoaDonDTO ct){
        CTHoaDonDAO data = new CTHoaDonDAO();
        data.Insert(ct);
        list_CTHD.add(ct);
    }
    public void Inserts(ArrayList<CTHoaDonDTO> list){
        CTHoaDonDAO data = new CTHoaDonDAO();
        data.Inserts(list);
        for(CTHoaDonDTO ct : list){
            list_CTHD.add(ct);
        }
    }
    public void Delete(CTHoaDonDTO ct){
        CTHoaDonDAO data = new CTHoaDonDAO();
        data.Delete(ct);
        list_CTHD.remove(ct);
    }
    public void Deletes(ArrayList<CTHoaDonDTO> list){
        CTHoaDonDAO data = new CTHoaDonDAO();
        data.Deletes(list);
        for(CTHoaDonDTO ct : list){
            list_CTHD.remove(ct);
        }
    }
    public void Update(CTHoaDonDTO ct){
        CTHoaDonDAO data = new CTHoaDonDAO();
        data.Update(ct);
        int i = -1;
        for(CTHoaDonDTO index : list_CTHD){
            if(index.getMaHD().compareTo(ct.getMaHD())==0 && index.getMaCB().compareTo(ct.getMaCB())==0 && index.getGheNgoi().compareTo(ct.getGheNgoi())==0){
                i = list_CTHD.indexOf(index);
            }
        }
        if(i != -1){
            list_CTHD.set(i, ct);
        }
    }
    public void Updates(ArrayList<CTHoaDonDTO> list){
        CTHoaDonDAO data = new CTHoaDonDAO();
        data.Updates(list);
        ArrayList<Integer> arr_ViTri = new ArrayList<>();
        for(CTHoaDonDTO index : list_CTHD){
            for(CTHoaDonDTO ct : list){
                if(index.getMaHD().compareTo(ct.getMaHD())==0 && index.getMaCB().compareTo(ct.getMaCB())==0 && index.getGheNgoi().compareTo(ct.getGheNgoi())==0){
                    arr_ViTri.add(list_CTHD.indexOf(index));
                }
            }
        }
        if(arr_ViTri.size()>0){
            for(int i=0; i<list.size(); i++){
                list_CTHD.set(arr_ViTri.get(i), list.get(i));
            }
        }
    }
}
