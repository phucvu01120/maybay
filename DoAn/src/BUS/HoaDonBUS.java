/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BUS;

import BLL.HoaDonDAO;
import DTO.HoaDonDTO;
import Form.panelQuanLy.panelThongKeDoanhThu;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class HoaDonBUS {
    public static ArrayList<HoaDonDTO> list_HD;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    
    public HoaDonBUS(){
        
    }
    public void getListHoaDon(String condition, String orderBy){
        if(list_HD == null)
            list_HD = new ArrayList<>();
        HoaDonDAO data = new HoaDonDAO();
        list_HD = data.getHoaDons(condition, orderBy);
    }
    public void getListHoaDon(String condition){
        this.getListHoaDon(condition, null);
    }
    public void getListHoaDon(){
        this.getListHoaDon(null);
    }
    public void Insert(HoaDonDTO hd){
        HoaDonDAO data = new HoaDonDAO();
        data.Insert(hd);
        list_HD.add(hd);
    }
    public void Inserts(ArrayList<HoaDonDTO> list){
        HoaDonDAO data = new HoaDonDAO();
        data.Inserts(list);
        for(HoaDonDTO hd : list){
            list_HD.add(hd);
        }
    }
    public void Delete(HoaDonDTO hd){
        HoaDonDAO data = new HoaDonDAO();
        data.Delete(hd);
        list_HD.remove(hd);
    }
    public void Deletes(ArrayList<HoaDonDTO> list){
        HoaDonDAO data = new HoaDonDAO();
        data.Deletes(list);
        for(HoaDonDTO hd : list){
            list_HD.remove(hd);
        }
    }
    public void Update(HoaDonDTO hd){
        HoaDonDAO data = new HoaDonDAO();
        data.Update(hd);
        int i = -1;
        for(HoaDonDTO index : list_HD){
            if(index.getMaHD().compareTo(hd.getMaHD())==0){
                i = list_HD.indexOf(index);
            }
        }
        if(i != -1){
            list_HD.set(i, hd);
        }
    }
    public void Updates(ArrayList<HoaDonDTO> list){
        HoaDonDAO data = new HoaDonDAO();
        data.Updates(list);
        ArrayList<Integer> arr_LuuViTri = new ArrayList<>();
        for(HoaDonDTO index : list_HD){
            for(HoaDonDTO hd : list){
                if(index.getMaHD().compareTo(hd.getMaHD())==0){
                    arr_LuuViTri.add(list_HD.indexOf(index));
                }
            }
        }
        if(arr_LuuViTri.size()>0){
            for(int i=0; i<arr_LuuViTri.size(); i++){
                list_HD.set(arr_LuuViTri.get(i), list.get(i));
            }
        }
    }
    public HoaDonDTO getHDByMa(String MaHD){
        for(HoaDonDTO hd : list_HD)
            if(hd.getMaHD().compareToIgnoreCase(MaHD)==0)
                return hd;
        return null;
    }
    public ArrayList<HoaDonDTO> getHDByMaNV(String MaNV){
        ArrayList<HoaDonDTO> listHDByMaNV = new ArrayList<>();
        for(HoaDonDTO hd : list_HD){
            if(hd.getMaNV().indexOf(MaNV)>=0){
                listHDByMaNV.add(hd);
            }
        }
        if(listHDByMaNV.size()>0)
            return listHDByMaNV;
        return null;
    }
    public ArrayList<HoaDonDTO> getHDByMaKH(String MaKH){
        ArrayList<HoaDonDTO> listHDByMaKH = new ArrayList<>();
        for(HoaDonDTO hd : list_HD){
            if(hd.getMaKH().indexOf(MaKH)>=0){
                listHDByMaKH.add(hd);
            }
        }
        if(listHDByMaKH.size()>0)
            return listHDByMaKH;
        return null;
    }
    public Date ngayMin_ListHoaDon(ArrayList<HoaDonDTO> list){
        try {
            Date min = sdf.parse("2100-01-01");
            Date temp = null;
            for(HoaDonDTO hd : list){
                temp = sdf.parse(hd.getNgayBan());
                if(temp.compareTo(min)<0){
                    min = temp;
                }
            }
            return min;
        } catch (ParseException ex) {
            Logger.getLogger(panelThongKeDoanhThu.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public Date ngayMax_ListHoaDon(ArrayList<HoaDonDTO> list){
        try {
            Date max = sdf.parse("1970-01-01");
            Date temp = null;
            for(HoaDonDTO hd : list){
                temp = sdf.parse(hd.getNgayBan());
                if(temp.compareTo(max)>0){
                    max = temp;
                }
            }
            return max;
        } catch (ParseException ex) {
            Logger.getLogger(panelThongKeDoanhThu.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public int TongHoaDon(ArrayList<HoaDonDTO> list){
        return list.size();
    }
    public long TongTien(ArrayList<HoaDonDTO> list){
        long sum = 0;
        for(HoaDonDTO hd : list){
            sum+= hd.getTongTien();
        }
        return sum;
    }
    
    public String convertVND(long sum){
        int dem=0;
        String temp = String.valueOf(sum);
        String tien = "";
        for(int i = temp.length()-1; i >= 0; i--){
            if(dem == 3){
                tien = tien.concat(","+temp.charAt(i));
                dem = 1;
            }
            else{
                tien = tien.concat(temp.charAt(i)+"");
                dem++;
            }
        }
        String tam = new StringBuffer(tien).reverse().toString();   
        return tam+" VND";
    }
    
}
