/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BUS;

import BLL.NhanVienDAO;
import DTO.NhanVienDTO;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class NhanVienBUS {
    public static ArrayList<NhanVienDTO> list_NV;
    public NhanVienBUS(){
        
    }
    public void getListNhanVien(String condition, String orderBy){
        try {
            if(list_NV == null)
                list_NV = new ArrayList<>();
            NhanVienDAO data = new NhanVienDAO();
            list_NV = data.getNhanViens(condition, orderBy);
        } catch (Exception ex) {
            Logger.getLogger(NhanVienBUS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void getListNhanVien(String condition){
        this.getListNhanVien(condition, null);
    }
    
    public void getListNhanVien(){
        this.getListNhanVien(null);
    }
    
    public NhanVienDTO getNVByMa(String MaNV){
        for(NhanVienDTO nv : list_NV){
            if(nv.getMaNV().compareToIgnoreCase(MaNV)==0){
                return nv;
            }
        }
        return null;
    }
    public ArrayList<NhanVienDTO> getNVByHo(String HoNV){
        ArrayList<NhanVienDTO> listNVByHo = new ArrayList<>();
        for(NhanVienDTO nv : list_NV){
            if(nv.getHoNV().indexOf(HoNV)>=0){
                listNVByHo.add(nv);
            }
        }
        if(listNVByHo.size()>0)
            return listNVByHo;
        return null;
    }
    public ArrayList<NhanVienDTO> getNVByTen(String TenNV){
        ArrayList<NhanVienDTO> listNVByTen = new ArrayList<>();
        for(NhanVienDTO nv : list_NV){
            if(nv.getTenNV().indexOf(TenNV)>=0){
                listNVByTen.add(nv);
            }
        }
        if(listNVByTen.size()>0)
            return listNVByTen;
        return null;
    }
    public ArrayList<NhanVienDTO> getNVByHoTen(String HoTen){
        ArrayList<NhanVienDTO> listByHoTen = new ArrayList<>();
        for(NhanVienDTO nv : list_NV){
            String temp = nv.getHoNV();
            temp = temp.concat(" "+nv.getTenNV());
            if(temp.indexOf(HoTen)>=0){
                listByHoTen.add(nv);
            }
        }
        if(listByHoTen.size()>0)
            return listByHoTen;
        return null;
    }
    public NhanVienDTO getNVBySdt(String Sdt){
        for(NhanVienDTO nv : list_NV){
            if(nv.getSDT().compareToIgnoreCase(Sdt)==0){
                return nv;
            }
        }
        return null;
    }
    public NhanVienDTO getNVByGmail(String mail){
        for(NhanVienDTO nv : list_NV){
            if(nv.getGmail().compareToIgnoreCase(mail)==0){
                return nv;
            }
        }
        return null;
    }
    public void Insert(NhanVienDTO nv) throws Exception{
        NhanVienDAO data = new NhanVienDAO();
        data.Insert(nv);
        list_NV.add(nv);
    }
    
    public void Inserts(ArrayList<NhanVienDTO> list) throws Exception{
        NhanVienDAO data = new NhanVienDAO();
        data.Inserts(list);
        for(NhanVienDTO nv : list){
            list_NV.add(nv);
        }
    }
    public void Delete(NhanVienDTO nv) throws Exception{
        NhanVienDAO data = new NhanVienDAO();
        data.Delete(nv);
        list_NV.remove(nv);
    }
    
    public void Deletes(ArrayList<NhanVienDTO> list) throws Exception{
        NhanVienDAO data = new NhanVienDAO();
        data.Deletes(list);
        for(NhanVienDTO nv : list){
            list_NV.remove(nv);
        }
    }
    
    public void Update(NhanVienDTO nv) throws Exception{
        NhanVienDAO data = new NhanVienDAO();
        data.Update(nv);
        int i = -1; // tìm vị trí của object cũ để thay object mới
        for(NhanVienDTO index : list_NV){
            if(index.getMaNV().compareToIgnoreCase(nv.getMaNV())==0){
                i = list_NV.indexOf(index);
            }
        }
        if(i != -1){
            list_NV.set(i, nv);
        }
    }
    
    public void Updates(ArrayList<NhanVienDTO> list) throws Exception{
        NhanVienDAO data = new NhanVienDAO();
        data.Updates(list);
        ArrayList<Integer> arr_LuuViTri = new ArrayList<>();//mảng lưu vị trí của list object cũ
        for(NhanVienDTO nv : list_NV){
            for(NhanVienDTO temp : list){
                if(nv.getMaNV().compareToIgnoreCase(temp.getMaNV())==0){
                    arr_LuuViTri.add(list_NV.indexOf(nv));
                }
            }
        }
        if(arr_LuuViTri.size()>0){
            for(int i=0; i < list.size(); i++){
                list_NV.set(arr_LuuViTri.get(i), list.get(i));
            }
        }
    }
}
