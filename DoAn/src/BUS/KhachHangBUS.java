/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BUS;

import BLL.KhachHangDAO;
import DTO.CTKhuyenMai;
import DTO.ChuyenBay;
import DTO.KhachHangDTO;
import java.util.ArrayList;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author HP
 */
public class KhachHangBUS {
    public static ArrayList<KhachHangDTO> list_KH;
    public KhachHangBUS(){
        
    }
    public void getListKH(String condition, String orderBy){
        if(list_KH == null)
            list_KH = new ArrayList<>();
        KhachHangDAO data = new KhachHangDAO();
        list_KH = data.getKhachHangs(condition, orderBy);
    }
    public void getListKH(String condition){
        this.getListKH(condition, null);
    }
    public void getListKH(){
        this.getListKH(null);
    }
    public KhachHangDTO getKHByMa(String MaKH){
        for(KhachHangDTO kh : list_KH){
            if(kh.getMaKH().compareToIgnoreCase(MaKH)==0){
                return kh;
            }
        }
        return null;
    }
    
    public void Insert(KhachHangDTO kh){
        KhachHangDAO data = new KhachHangDAO();
        data.Insert(kh);
        list_KH.add(kh);
    }
    
    public void Inserts(ArrayList<KhachHangDTO> list){
        KhachHangDAO data = new KhachHangDAO();
        data.Inserts(list);
        for(KhachHangDTO kh : list){
            list_KH.add(kh);
        }
    }
    
    public void Delete(KhachHangDTO kh){
        KhachHangDAO data = new KhachHangDAO();
        data.Delete(kh);
        list_KH.remove(kh);
    }
    
    public void Deletes(ArrayList<KhachHangDTO> list){
        KhachHangDAO data = new KhachHangDAO();
        data.Deletes(list);
        for(KhachHangDTO kh : list){
            list_KH.remove(kh);
        }
    }
    
    public void Update(KhachHangDTO kh) throws Exception{
        KhachHangDAO data = new KhachHangDAO();
        data.Update(kh);
        int i = -1; // tìm vị trí của object cũ để thay object mới
        for(KhachHangDTO index : list_KH){
            if(index.getMaKH().compareToIgnoreCase(kh.getMaKH())==0){
                i = list_KH.indexOf(index);
            }
        }
        if(i != -1){
            list_KH.set(i, kh);
        }
    }
    
    public void Updates(ArrayList<KhachHangDTO> list) throws Exception{
        KhachHangDAO data = new KhachHangDAO();
        data.Updates(list);
        ArrayList<Integer> arr_LuuViTri = new ArrayList<>();//mảng lưu vị trí của list object cũ
        for(KhachHangDTO kh : list_KH){
            for(KhachHangDTO temp : list){
                if(kh.getMaKH().compareToIgnoreCase(temp.getMaKH())==0){
                    arr_LuuViTri.add(list_KH.indexOf(kh));
                }
            }
        }
        if(arr_LuuViTri.size()>0){
            for(int i=0; i < list.size(); i++){
                list_KH.set(arr_LuuViTri.get(i), list.get(i));
            }
        }
    }
    
    public ArrayList<KhachHangDTO> getKHByHo(String HoKH){
        ArrayList<KhachHangDTO> listKHByHo = new ArrayList<>();
        for(KhachHangDTO kh : list_KH){
            if(kh.getHoKH().indexOf(HoKH)>=0){
                listKHByHo.add(kh);
            }
        }
        if(listKHByHo.size()>0)
            return listKHByHo;
        return null;
    }
    public ArrayList<KhachHangDTO> getKHByTen(String TenKH){
        ArrayList<KhachHangDTO> listKHByTen = new ArrayList<>();        
        for(KhachHangDTO kh : list_KH){
            if(kh.getTenKH().indexOf(TenKH) >= 0){
                listKHByTen.add(kh);
            }
        }
        if(listKHByTen.size()>0)
            return listKHByTen;
        return null;
    }
    
    public ArrayList<KhachHangDTO> getKHByHoTen(String HoTen){
        ArrayList<KhachHangDTO> listByHoTen = new ArrayList<>();
        for(KhachHangDTO kh : list_KH){
            String temp = kh.getHoKH();
            temp = temp.concat(" "+kh.getTenKH());
            if(temp.indexOf(HoTen) >= 0){
                listByHoTen.add(kh);
            }
        }
        if(listByHoTen.size()>0)
            return listByHoTen;
        return null;
    }
            
    public KhachHangDTO getKHBySDT(String sdt){
        for(KhachHangDTO kh : list_KH){
            if(kh.getSDT().compareToIgnoreCase(sdt)==0)
                return kh;
        }
        return null;
    }
    public KhachHangDTO getKHByGmail(String mail){        
        for(KhachHangDTO kh : list_KH)
            if(kh.getGmail().compareToIgnoreCase(mail)==0)
                return kh;
        return null;
    }
    public boolean checkMaKH(String makh){
        for(KhachHangDTO kh:list_KH){
            if(kh.getMaKH().equals(makh)){
                return false;
            }
        }
        return true;
    }
    public boolean checkKhCu(KhachHangDTO khachhang){
        for(KhachHangDTO kh:list_KH){
            if(kh.getMaKH().equals(khachhang.getMaKH()) && kh.getHoKH().equals(khachhang.getHoKH()) && kh.getTenKH().equals(khachhang.getTenKH())
                    && kh.getGioiTinh().equals(khachhang.getGioiTinh()) && kh.getNgaySinh().equals(khachhang.getNgaySinh()) 
                    && kh.getSDT().equals(khachhang.getSDT()) && kh.getGmail().equals(khachhang.getGmail()))
                return true;
        }
        return false;
    }
    public boolean checkNgaySinh(String date,String NgaySinh,int tuoi){
        int year1=Integer.parseInt(date.substring(0,4));
        int month1=Integer.parseInt(date.substring(5,7));
        int date1=Integer.parseInt(date.substring(8,10));
        int year2=Integer.parseInt(NgaySinh.substring(0,4));
        int month2=Integer.parseInt(NgaySinh.substring(5,7));
        int date2=Integer.parseInt(NgaySinh.substring(8,10));
        if((year1 - year2) > tuoi){
            return true;
        }
        if((year1 - year2)==tuoi && month1 > month2){
            return true;
        }
        if((year1 - year2)==tuoi && (month1==month2) && date2<=date1){
            return true;
        }
        return false;
    }
    public boolean isNumeric(String str) {
	return str.matches("-?\\d+(\\.\\d+)?");
    }
    public void sendEmail(String tenKH,CTKhuyenMai ctkm,String MaCode,String mailKH) throws AddressException, MessagingException {
        Properties mailServerProperties;
        Session getMailSession;
        MimeMessage mailMessage;
        // Step1: setup Mail Server
        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");
        // Step2: get Mail Session
        getMailSession = Session.getDefaultInstance(mailServerProperties, null);
        mailMessage = new MimeMessage(getMailSession);

        mailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(mailKH)); //Thay abc bằng địa chỉ người nhận

        // Bạn có thể chọn CC, BCC
        mailMessage.setSubject("CHÚC MỪNG "+tenKH+" ĐÃ NHẬN ĐƯỢC CODE KHUYẾN MÃI CỦA CT TNHH 4 THÀNH VIÊN BÁN VÉ MÁY BAY");
        String emailBody = "<p>Chào anh/chị <b>"+tenKH+"</b></p>"
                         + "<p>Công ty sắp tới sẽ có chương trình khuyến mãi mua vé máy bay</p>"
                         + "<p>Công ty xin tặng anh/chị <b>"+tenKH+"</b> mã code của chương trình khuyến mãi</p>"
                         + "<table style=\"border-spacing: 0px\" border='1'>"
                            + "<tr>"
                                + "<th style=\"padding: 10px\">Chương trình khuyến mãi</th>"
                                + "<th style=\"padding: 10px\">Mã code</th>"
                                + "<th style=\"padding: 10px\">Phần trăm khuyến mãi</th>"
                                + "<th style=\"padding: 10px\">Ngày bắt đầu</th>"
                                + "<th style=\"padding: 10px\">Ngày kết thúc</th>"
                            + "</tr>"
                            + "<tr style=\"color: red; text-align: center\">"
                                + "<td>"+ctkm.getTenCTKM()+"</td>"
                                + "<td>"+MaCode+"</td>"
                                + "<td>"+Integer.toString(ctkm.getPhanTramKM())+"%</td>"
                                + "<td>"+ctkm.getNgayBatDau()+"</td>"
                                + "<td>"+ctkm.getNgayKetThuc()+"</td>"
                            + "</tr>"
                         + "</table>"
                         + "<p>Cám ơn anh/chị <b>"+tenKH+"</b> đã sử dụng dịch vụ mua vé máy bay bên công ty</p>";
        mailMessage.setContent(emailBody,"text/html; charset=UTF-8");
        // Step3: Send mail
        Transport transport = getMailSession.getTransport("smtp");
        // Thay your_gmail thành gmail của bạn, thay your_password thành mật khẩu gmail của bạn
        transport.connect("smtp.gmail.com", "bvemaybay@gmail.com", "vemaybay123"); 
        transport.sendMessage(mailMessage, mailMessage.getAllRecipients());
        transport.close();
    }

    public static void main(String[] args) throws MessagingException {
        KhachHangBUS b = new KhachHangBUS();
    }
}
