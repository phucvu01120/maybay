/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author PHUC
 */
public class MayBay {
    private String MaMB;
    private String Mota;
    private String MaMHMB;
    private MoHinhMayBay mhmb;

    public MayBay(String MaMB, String Mota, String MaMHMB) {
        this.MaMB = MaMB;
        this.Mota = Mota;
        this.MaMHMB = MaMHMB;
    }

    public MayBay() {
    }

    public String getMaMB() {
        return MaMB;
    }

    public void setMaMB(String MaMB) {
        this.MaMB = MaMB;
    }

    public String getMota() {
        return Mota;
    }

    public void setMota(String Mota) {
        this.Mota = Mota;
    }

    public String getMaMHMB() {
        return MaMHMB;
    }

    public void setMaMHMB(String MaMHMB) {
        this.MaMHMB = MaMHMB;
    }
//*
    public MoHinhMayBay getMhmb() {
        return mhmb;
    }

    
}
