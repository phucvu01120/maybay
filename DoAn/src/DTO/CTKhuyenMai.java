/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author LENOVO
 */

public class CTKhuyenMai {
    private String MaCTKM;
    private String TenCTKM;
    private String NgayBatDau;
    private String NgayKetThuc;
    private int PhanTramKM;
    private int SLCode;
    private String TinhTrang;
    
    public CTKhuyenMai(String MaCTKM, String TenCTKM, String NgayBatDau, String NgayKetThuc, int PhanTramKM, int SLCode,String TinhTrang) {
        this.MaCTKM = MaCTKM;
        this.TenCTKM=TenCTKM;
        this.NgayBatDau = NgayBatDau;
        this.NgayKetThuc = NgayKetThuc;
        this.PhanTramKM = PhanTramKM;
        this.SLCode = SLCode;
        this.TinhTrang = TinhTrang;
    }
    public CTKhuyenMai(){};
    
    public String getMaCTKM() {
        return MaCTKM;
    }

    public void setMaCTKM(String MaCTKM) {
        this.MaCTKM = MaCTKM;
    }

    public String getTenCTKM() {
        return TenCTKM;
    }

    public void setTenCTKM(String TenCTKM) {
        this.TenCTKM = TenCTKM;
    }
    
    public String getNgayBatDau() {
        return NgayBatDau;
    }

    public void setNgayBatDau(String NgayBatDau) {
        this.NgayBatDau = NgayBatDau;
    }

    public String getNgayKetThuc() {
        return NgayKetThuc;
    }

    public void setNgayKetThuc(String NgayKetThuc) {
        this.NgayKetThuc = NgayKetThuc;
    }

    public int getPhanTramKM() {
        return PhanTramKM;
    }

    public void setPhanTramKM(int PhanTramKM) {
        this.PhanTramKM = PhanTramKM;
    }

    public int getSLCode() {
        return SLCode;
    }

    public void setSLCode(int SLCode) {
        this.SLCode = SLCode;
    }

    public String getTinhTrang() {
        return TinhTrang;
    }

    public void setTinhTrang(String TinhTrang) {
        this.TinhTrang = TinhTrang;
    }
    
}
