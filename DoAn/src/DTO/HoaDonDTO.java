/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author HP
 */
public class HoaDonDTO {
    private String MaHD;
    private String MaNV;
    private String NgayBan;
    private String MaKH;
    private int TongTien;
    public HoaDonDTO(){
        
    }

    public HoaDonDTO(String MaHD, String MaNV, String NgayBan, String MaKH, int TongTien) {
        this.MaHD = MaHD;
        this.MaNV = MaNV;
        this.NgayBan = NgayBan;
        this.MaKH = MaKH;
        this.TongTien = TongTien;
    }

    public String getMaHD() {
        return MaHD;
    }

    public void setMaHD(String MaHD) {
        this.MaHD = MaHD;
    }

    public String getMaNV() {
        return MaNV;
    }

    public void setMaNV(String MaNV) {
        this.MaNV = MaNV;
    }

    public String getNgayBan() {
        return NgayBan;
    }

    public void setNgayBan(String NgayBan) {
        this.NgayBan = NgayBan;
    }

    public String getMaKH() {
        return MaKH;
    }

    public void setMaKH(String MaKH) {
        this.MaKH = MaKH;
    }

    public int getTongTien() {
        return TongTien;
    }

    public void setTongTien(int TongTien) {
        this.TongTien = TongTien;
    }
    
    
}
