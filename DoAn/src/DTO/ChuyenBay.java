/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import BLL.BHangHangKhong;
import BLL.BMayBay;
import BLL.BSanBay;
import BLL.BTuyenBay;

/**
 *
 * @author PHUC
 */
public class ChuyenBay {
    private String MaCB;
    private String MaHang;
    private String MaTuyenBay;
    private String MaMB;
    private int SLGheThuongGia_CL;
    private int SLGhePhoThong_CL;
    private int GiaVeThuongGia;
    private int GiaVePhoThong;
    private String NgayBay;
    private String NgayBayTT;
    private String TrangThai;

    public ChuyenBay() {
    }

    public ChuyenBay(String MaCB, String MaHang, String MaTuyenBay, String MaMB, int SLGheThuongGia_CL, int SLGhePhoThong_CL, int GiaVeThuongGia, int GiaVePhoThong, String NgayBay, String NgayBayTT, String TrangThai) {
        this.MaCB = MaCB;
        this.MaHang = MaHang;
        this.MaTuyenBay = MaTuyenBay;
        this.MaMB = MaMB;
        this.SLGheThuongGia_CL = SLGheThuongGia_CL;
        this.SLGhePhoThong_CL = SLGhePhoThong_CL;
        this.GiaVeThuongGia = GiaVeThuongGia;
        this.GiaVePhoThong = GiaVePhoThong;
        this.NgayBay = NgayBay;
        this.NgayBayTT = NgayBayTT;
        this.TrangThai = TrangThai;
    }

    public String getMaCB() {
        return MaCB;
    }

    public void setMaCB(String MaCB) {
        this.MaCB = MaCB;
    }

    public String getMaHang() {
        return MaHang;
    }

    public void setMaHang(String MaHang) {
        this.MaHang = MaHang;
    }

    public String getMaTuyenBay() {
        return MaTuyenBay;
    }

    public void setMaTuyenBay(String MaTuyenBay) {
        this.MaTuyenBay = MaTuyenBay;
    }

    public String getMaMB() {
        return MaMB;
    }

    public void setMaMB(String MaMB) {
        this.MaMB = MaMB;
    }

    public int getSLGheThuongGia_CL() {
        return SLGheThuongGia_CL;
    }

    public void setSLGheThuongGia_CL(int SLGheThuongGia_CL) {
        this.SLGheThuongGia_CL = SLGheThuongGia_CL;
    }

    public int getSLGhePhoThong_CL() {
        return SLGhePhoThong_CL;
    }

    public void setSLGhePhoThong_CL(int SLGhePhoThong_CL) {
        this.SLGhePhoThong_CL = SLGhePhoThong_CL;
    }

    public int getGiaVeThuongGia() {
        return GiaVeThuongGia;
    }

    public void setGiaVeThuongGia(int GiaVeThuongGia) {
        this.GiaVeThuongGia = GiaVeThuongGia;
    }

    public int getGiaVePhoThong() {
        return GiaVePhoThong;
    }

    public void setGiaVePhoThong(int GiaVePhoThong) {
        this.GiaVePhoThong = GiaVePhoThong;
    }

    public String getNgayBay() {
        return NgayBay;
    }

    public void setNgayBay(String NgayBay) {
        this.NgayBay = NgayBay;
    }

    public String getNgayBayTT() {
        return NgayBayTT;
    }

    public void setNgayBayTT(String NgayBayTT) {
        this.NgayBayTT = NgayBayTT;
    }

    public String getTrangThai() {
        return TrangThai;
    }

    public void setTrangThai(String TrangThai) {
        this.TrangThai = TrangThai;
    }

    ///chua xai dc
    public String showCTCB() throws Exception{
        String ctcb="Chuyến bay: "+getMaCB();
        BHangHangKhong bHHK=new BHangHangKhong();
        HangHangKhong hhk=bHHK.getHangHangKhongByMaHang(getMaHang());
        ctcb+="     Hãng: "+hhk.getTenHang()+"-"+hhk.getQuocGia()+"     Máy bay: "+getMaMB()+"\n";
        BTuyenBay bTB=new BTuyenBay();
        TuyenBay tb=bTB.getByID(getMaTuyenBay());
        BSanBay bSB=new BSanBay();
        SanBay sbcc=bSB.getSanBayByMaSB(tb.getMaSBCatCanh());
        SanBay sbhc=bSB.getSanBayByMaSB(tb.getMaSBHaCanh());
        if(tb.getMaSBTrungGian().equals("")){
            ctcb+="\n"+sbcc.getTenSanBay()+"-"+sbcc.getThanhPho()+"  ----->  "+sbhc.getTenSanBay()+"-"+sbhc.getThanhPho();
            ctcb+="\n\nNgày bay:"+getNgayBay();
        }
        else{
            SanBay sbtg=bSB.getSanBayByMaSB(tb.getMaSBTrungGian());
            ctcb+="\n"+sbcc.getTenSanBay()+"-"+sbcc.getThanhPho()+"  ----->  "+sbtg.getTenSanBay()+"-"+sbtg.getThanhPho()+"  ----->  "+sbhc.getTenSanBay()+"-"+sbhc.getThanhPho();
            ctcb+="\n\nNgày bay:"+getNgayBay()+" Ngày bay tiếp tục:"+getNgayBayTT();
        }
        ctcb+="\n\nSố Ghế Thương Gia còn trống: "+getSLGheThuongGia_CL()+"  Giá vé thương gia:"+getGiaVeThuongGia();
        ctcb+="\n\nSố Ghế Phổ Thông còn trống: "+getSLGhePhoThong_CL()+"  Giá vé phổ thông:"+getGiaVePhoThong();
        ctcb+="\n\nTình trạng: "+getTrangThai();
        return ctcb;
    }
    
}
