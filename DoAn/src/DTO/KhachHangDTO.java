/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author PHUC
 */
public class KhachHangDTO {
    private String MaKH;
    private String HoKH;
    private String TenKH;
    private String SDT;
    private String Gmail;
    private String GioiTinh;
    private String NgaySinh;

    public KhachHangDTO() {
    }

    public KhachHangDTO(String MaKH, String HoKH, String TenKH, String SDT, String Gmail, String GioiTinh, String NgaySinh) {
        this.MaKH = MaKH;
        this.HoKH = HoKH;
        this.TenKH = TenKH;
        this.SDT = SDT;
        this.Gmail = Gmail;
        this.GioiTinh = GioiTinh;
        this.NgaySinh = NgaySinh;
    }

    public String getMaKH() {
        return MaKH;
    }

    public void setMaKH(String MaKH) {
        this.MaKH = MaKH;
    }

    public String getHoKH() {
        return HoKH;
    }

    public void setHoKH(String HoKH) {
        this.HoKH = HoKH;
    }

    public String getTenKH() {
        return TenKH;
    }

    public void setTenKH(String TenKH) {
        this.TenKH = TenKH;
    }

    public String getSDT() {
        return SDT;
    }

    public void setSDT(String SDT) {
        this.SDT = SDT;
    }

    public String getGmail() {
        return Gmail;
    }

    public void setGmail(String Gmail) {
        this.Gmail = Gmail;
    }

    public String getGioiTinh() {
        return GioiTinh;
    }

    public void setGioiTinh(String GioiTinh) {
        this.GioiTinh = GioiTinh;
    }

    public String getNgaySinh() {
        return NgaySinh;
    }

    public void setNgaySinh(String NgaySinh) {
        this.NgaySinh = NgaySinh;
    }
    public boolean checkGmail(){
        if(this.Gmail.equals("")){
            return true;
        }
        else{
            if(this.Gmail.contains("@")==true)
            return true;
        }
        return false;
    }
    public boolean checkSDT(){
        if(this.SDT.equals("")){
            return true;
        }
        else{
            if(this.SDT.length()==10)
            return true;
        
        }
        return false;
    }
    public boolean checkMaKH(){
        if(this.MaKH.length()==13)
            return true;
        return false;
    }
    public void sendEmailHuyCB(String macb,String lido) throws AddressException, MessagingException {
        Properties mailServerProperties;
        Session getMailSession;
        MimeMessage mailMessage;
        // Step1: setup Mail Server
        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");
        // Step2: get Mail Session
        getMailSession = Session.getDefaultInstance(mailServerProperties, null);
        mailMessage = new MimeMessage(getMailSession);

        mailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(this.getGmail())); //Thay abc bằng địa chỉ người nhận

        // Bạn có thể chọn CC, BCC
        mailMessage.setSubject("THÔNG BÁO HỦY CHUYẾN BAY "+macb);
        String emailBody = "<p>Chào anh/chị <b>"+this.getHoKH()+" "+this.getTenKH()+"</b></p>"
                         + "<p>Xin thông báo chuyến bay <b>"+macb+"</b> đã bị hủy</p>"
                         + "<p>Lý do : "+lido+" </p>"
                         + "<p>Xin lỗi quí khách vì sự bất tiện này !!</p>"                        
                         + "<p>Cám ơn anh/chị <b>"+this.getHoKH()+" "+this.getTenKH()+"</b> đã sử dụng dịch vụ, chúng tôi sẽ liên hệ nhanh nhất với quý khách để đổi chuyến bay mới</p>";
        mailMessage.setContent(emailBody,"text/html; charset=UTF-8");
        // Step3: Send mail
        Transport transport = getMailSession.getTransport("smtp");
        // Thay your_gmail thành gmail của bạn, thay your_password thành mật khẩu gmail của bạn
        transport.connect("smtp.gmail.com", "bvemaybay@gmail.com", "vemaybay123"); 
        transport.sendMessage(mailMessage, mailMessage.getAllRecipients());
        transport.close();
    }
    
    
}
