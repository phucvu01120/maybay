/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author HP
 */
public class CTHoaDonDTO {
    private String MaHD;
    private String MaCB;
    private String GheNgoi;
    private String MaHanhLy;
    private int GiaVe;
    private int GiaHanhLi;
    private int ThanhTien;

    public CTHoaDonDTO() {
    }

    public CTHoaDonDTO(String MaHD, String MaCB, String GheNgoi, String MaHanhLy, int GiaVe, int GiaHanhLi, int ThanhTien) {
        this.MaHD = MaHD;
        this.MaCB = MaCB;
        this.GheNgoi = GheNgoi;
        this.MaHanhLy = MaHanhLy;
        this.GiaVe = GiaVe;
        this.GiaHanhLi = GiaHanhLi;
        this.ThanhTien = ThanhTien;
    }

    public String getMaHD() {
        return MaHD;
    }

    public void setMaHD(String MaHD) {
        this.MaHD = MaHD;
    }

    public String getMaCB() {
        return MaCB;
    }

    public void setMaCB(String MaCB) {
        this.MaCB = MaCB;
    }

    public String getGheNgoi() {
        return GheNgoi;
    }

    public void setGheNgoi(String GheNgoi) {
        this.GheNgoi = GheNgoi;
    }

    public String getMaHanhLy() {
        return MaHanhLy;
    }

    public void setMaHanhLy(String MaHanhLy) {
        this.MaHanhLy = MaHanhLy;
    }

    public int getGiaVe() {
        return GiaVe;
    }

    public void setGiaVe(int GiaVe) {
        this.GiaVe = GiaVe;
    }

    public int getGiaHanhLi() {
        return GiaHanhLi;
    }

    public void setGiaHanhLi(int GiaHanhLi) {
        this.GiaHanhLi = GiaHanhLi;
    }

    public int getThanhTien() {
        return ThanhTien;
    }

    public void setThanhTien(int ThanhTien) {
        this.ThanhTien = ThanhTien;
    }

    
}
