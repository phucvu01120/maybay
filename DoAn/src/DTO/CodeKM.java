/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author LENOVO
 */
public class CodeKM {
    private String MaCode;
    private String TinhTrang;
    private String MaCTKM;

    public CodeKM(String MaCode, String TinhTrang, String MaCTKM) {
        this.MaCode = MaCode;
        this.TinhTrang = TinhTrang;
        this.MaCTKM = MaCTKM;
    }
    public CodeKM(){};
    public String getMaCode() {
        return MaCode;
    }

    public void setMaCode(String MaCode) {
        this.MaCode = MaCode;
    }

    public String getTinhTrang() {
        return TinhTrang;
    }

    public void setTinhTrang(String TinhTrang) {
        this.TinhTrang = TinhTrang;
    }

    public String getMaCTKM() {
        return MaCTKM;
    }

    public void setMaCTKM(String MaCTKM) {
        this.MaCTKM = MaCTKM;
    }
    
}
