/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author PHUC
 */
public class MoHinhMayBay {
    private String MaMHMB;
    private int SoGheThuongGia;
    private int SoGhePhoThong;
    private String DSGheThuongGia;
    private String DSGhePhoThong;

    public MoHinhMayBay() {
    }

    public MoHinhMayBay(String MaMHMB, int SoGheThuongGia, int SoGhePhoThong, String DSGheThuongGia, String DSGhePhoThong) {
        this.MaMHMB = MaMHMB;
        this.SoGheThuongGia = SoGheThuongGia;
        this.SoGhePhoThong = SoGhePhoThong;
        this.DSGheThuongGia = DSGheThuongGia;
        this.DSGhePhoThong = DSGhePhoThong;
    }

    public String getMaMHMB() {
        return MaMHMB;
    }

    public void setMaMHMB(String MaMHMB) {
        this.MaMHMB = MaMHMB;
    }

    public int getSoGheThuongGia() {
        return SoGheThuongGia;
    }

    public void setSoGheThuongGia(int SoGheThuongGia) {
        this.SoGheThuongGia = SoGheThuongGia;
    }

    public int getSoGhePhoThong() {
        return SoGhePhoThong;
    }

    public void setSoGhePhoThong(int SoGhePhoThong) {
        this.SoGhePhoThong = SoGhePhoThong;
    }

    public String getDSGheThuongGia() {
        return DSGheThuongGia;
    }

    public void setDSGheThuongGia(String DSGheThuongGia) {
        this.DSGheThuongGia = DSGheThuongGia;
    }

    public String getDSGhePhoThong() {
        return DSGhePhoThong;
    }

    public void setDSGhePhoThong(String DSGhePhoThong) {
        this.DSGhePhoThong = DSGhePhoThong;
    }
    
    
    
}
