/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author PHUC
 */
public class HangHangKhong {
    private String MaHang;
    private String TenHang;
    private String QuocGia;
    private String pathLoGo;

    public HangHangKhong() {
    }

    public HangHangKhong(String MaHang, String TenHang, String QuocGia, String pathLoGo) {
        this.MaHang = MaHang;
        this.TenHang = TenHang;
        this.QuocGia = QuocGia;
        this.pathLoGo = pathLoGo;
    }

    public String getMaHang() {
        return MaHang;
    }

    public void setMaHang(String MaHang) {
        this.MaHang = MaHang;
    }

    public String getTenHang() {
        return TenHang;
    }

    public void setTenHang(String TenHang) {
        this.TenHang = TenHang;
    }

    public String getQuocGia() {
        return QuocGia;
    }

    public void setQuocGia(String QuocGia) {
        this.QuocGia = QuocGia;
    }

    public String getPathLoGo() {
        return pathLoGo;
    }

    public void setPathLoGo(String pathLoGo) {
        this.pathLoGo = pathLoGo;
    }
    
}
