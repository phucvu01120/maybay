/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author LENOVO
 */
public class VeMayBay {
    
    private String MaCB;
    private String LoaiVe;
    private String GheNgoi;
    private String MaKH;
    private String MaCode;
    private String TinhTrang;

    public VeMayBay() {
    }

    public VeMayBay(String MaCB, String LoaiVe, String GheNgoi, String MaKH, String MaCode, String TinhTrang) {
        this.MaCB = MaCB;
        this.LoaiVe = LoaiVe;
        this.GheNgoi = GheNgoi;
        this.MaKH = MaKH;
        this.MaCode = MaCode;
        this.TinhTrang = TinhTrang;
    }

    public String getMaCB() {
        return MaCB;
    }

    public void setMaCB(String MaCB) {
        this.MaCB = MaCB;
    }

    public String getLoaiVe() {
        return LoaiVe;
    }

    public void setLoaiVe(String LoaiVe) {
        this.LoaiVe = LoaiVe;
    }

    public String getGheNgoi() {
        return GheNgoi;
    }

    public void setGheNgoi(String GheNgoi) {
        this.GheNgoi = GheNgoi;
    }

    public String getMaKH() {
        return MaKH;
    }

    public void setMaKH(String MaKH) {
        this.MaKH = MaKH;
    }

    public String getMaCode() {
        return MaCode;
    }

    public void setMaCode(String MaCode) {
        this.MaCode = MaCode;
    }

    public String getTinhTrang() {
        return TinhTrang;
    }

    public void setTinhTrang(String TinhTrang) {
        this.TinhTrang = TinhTrang;
    }

   

    
}