/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author LENOVO
 */
public class SanBay {
    private String MaSanBay;
    private String TenSanBay;
    private String ThanhPho;

    public SanBay(String MaSanBay, String TenSanBay, String ThanhPho) {
        this.MaSanBay = MaSanBay;
        this.TenSanBay = TenSanBay;
        this.ThanhPho = ThanhPho;
    }
    public SanBay (){};

    public String getMaSanBay() {
        return MaSanBay;
    }

    public void setMaSanBay(String MaSanBay) {
        this.MaSanBay = MaSanBay;
    }

    public String getTenSanBay() {
        return TenSanBay;
    }

    public void setTenSanBay(String TenSanBay) {
        this.TenSanBay = TenSanBay;
    }

    public String getThanhPho() {
        return ThanhPho;
    }

    public void setThanhPho(String ThanhPho) {
        this.ThanhPho = ThanhPho;
    }
    
}
