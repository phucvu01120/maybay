/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author LENOVO
 */
public class TuyenBay {
    private String MaTuyenBay;
    private String MaSBCatCanh;
    private String MaSBHaCanh;
    private String MaSBTrungGian;

    public TuyenBay(String MaTuyenBay, String MaSBCatCanh, String MaSBHaCanh, String MaSBTrungGian) {
        this.MaTuyenBay = MaTuyenBay;
        this.MaSBCatCanh = MaSBCatCanh;
        this.MaSBHaCanh = MaSBHaCanh;
        this.MaSBTrungGian = MaSBTrungGian;
    }
    public TuyenBay (){};

    public String getMaTuyenBay() {
        return MaTuyenBay;
    }

    public void setMaTuyenBay(String MaTuyenBay) {
        this.MaTuyenBay = MaTuyenBay;
    }

    public String getMaSBCatCanh() {
        return MaSBCatCanh;
    }

    public void setMaSBCatCanh(String MaSBCatCanh) {
        this.MaSBCatCanh = MaSBCatCanh;
    }

    public String getMaSBHaCanh() {
        return MaSBHaCanh;
    }

    public void setMaSBHaCanh(String MaSBHaCanh) {
        this.MaSBHaCanh = MaSBHaCanh;
    }

    public String getMaSBTrungGian() {
        return MaSBTrungGian;
    }

    public void setMaSBTrungGian(String MaSBTrungGian) {
        this.MaSBTrungGian = MaSBTrungGian;
    }
    
}
