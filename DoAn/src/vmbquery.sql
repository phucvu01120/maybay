SELECT
     vemaybay.`LoaiVe` AS vemaybay_LoaiVe,
     vemaybay.`GheNgoi` AS vemaybay_GheNgoi,
     chuyenbay.`MaCB` AS chuyenbay_MaCB,
     chuyenbay.`MaMB` AS chuyenbay_MaMB,
     chuyenbay.`NgayBay` AS chuyenbay_NgayBay,
     chuyenbay.`NgayBayTT` AS chuyenbay_NgayBayTT,
     khachhang.`MaKH` AS khachhang_MaKH,
     khachhang.`HoKH` AS khachhang_HoKH,
     khachhang.`TenKH` AS khachhang_TenKH,
     hanghangkhong.`MaHang` AS hanghangkhong_MaHang,
     hanghangkhong.`TenHang` AS hanghangkhong_TenHang,
     tuyenbay.`MaTuyenBay` AS tuyenbay_MaTuyenBay,
     sanbay.`MaSanBay` AS sanbay_MaSanBay,
     sanbay.`TenSanBay` AS sanbay_TenSanBay,
     sanbay.`ThanhPho` AS sanbay_ThanhPho,
     sanbay_A.`MaSanBay` AS sanbay_A_MaSanBay,
     sanbay_A.`TenSanBay` AS sanbay_A_TenSanBay,
     sanbay_A.`ThanhPho` AS sanbay_A_ThanhPho,
     sanbay_B.`MaSanBay` AS sanbay_B_MaSanBay,
     sanbay_B.`TenSanBay` AS sanbay_B_TenSanBay,
     sanbay_B.`ThanhPho` AS sanbay_B_ThanhPho
FROM
     `chuyenbay` chuyenbay INNER JOIN `vemaybay` vemaybay ON chuyenbay.`MaCB` = vemaybay.`MaCB`
     INNER JOIN `khachhang` khachhang ON vemaybay.`MaKH` = khachhang.`MaKH`
     INNER JOIN `hanghangkhong` hanghangkhong ON chuyenbay.`MaHang` = hanghangkhong.`MaHang`
     INNER JOIN `tuyenbay` tuyenbay ON chuyenbay.`MaTuyenBay` = tuyenbay.`MaTuyenBay`
     INNER JOIN `sanbay` sanbay ON tuyenbay.`MaSBCatCanh` = sanbay.`MaSanBay`
     INNER JOIN `sanbay` sanbay_A ON tuyenbay.`MaSBHaCanh` = sanbay_A.`MaSanBay`
     INNER JOIN `sanbay` sanbay_B ON tuyenbay.`MaSBTrungGian` = sanbay_B.`MaSanBay`
WHERE
     chuyenbay.`MaCB` = $P{MaCB}
 AND GheNgoi = $P{GheNgoi}