package ApachePOI;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {
    private static String pathfile;
    public static boolean WriteFileExcel(JTable data, String NameTableExcel, String ma, String tungay, String denngay){
            JFileChooser fs = new JFileChooser();
            fs.setCurrentDirectory(new File("D:/"));
            fs.setDialogTitle("Save a file");
            fs.setFileFilter(new FileNameExtensionFilter("excel file(*.xlsx)","xlsx"));
            fs.setFileFilter(new FileNameExtensionFilter("excel file(*.xls)","xls"));
            boolean co=true;
            while (co==true){
                int result = fs.showSaveDialog(null);
                if (result!=JFileChooser.APPROVE_OPTION){
                    return false;
                }
                else{
                    pathfile = fs.getSelectedFile().getAbsolutePath();
                    File f;
                    if (fs.getFileFilter().getDescription().equals("excel file(*.xls)")){
                        try {
                            if (!pathfile.contains(".xls")){
                                pathfile+=".xls";
                            }
                            f=new File(pathfile);
                            if(f.exists()){
                                int me = JOptionPane.showConfirmDialog(null, "File đã tồn tại, bạn có muốn ghi đè lên file cũ ?");
                                if(me==0){
                                    if(f.renameTo(f)){
                                        co=false;
                                        writeXLSExcel(data,NameTableExcel,ma,tungay,denngay);
                                        return true;
                                    }
                                    else{
                                        JOptionPane.showMessageDialog(null, "File đang mở!");
                                        continue;
                                    }
                                }
                                else continue;
                            }
                            else{
                                co=false;
                            }
                            writeXLSExcel(data,NameTableExcel,ma,tungay,denngay);
                            return true;
                        } catch (IOException ex) {
                            Logger.getLogger(Excel.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    if (fs.getFileFilter().getDescription().equals("excel file(*.xlsx)")){
                        try {
                            if (!pathfile.contains(".xlsx")){
                                pathfile+=".xlsx";
                            }
                            f=new File(pathfile);
                            if(f.exists()){
                                int me=JOptionPane.showConfirmDialog(null, "File đã tồn tại, bạn có muốn ghi đề lên file cũ");
                                if(me==0){
                                    if(f.renameTo(f)){
                                        co=false;
                                        writeXLSXExcel(data,NameTableExcel,ma,tungay,denngay);
                                        return true;
                                    }
                                    else{
                                        JOptionPane.showMessageDialog(null, "hình như file đang mở!");
                                        continue;
                                    }
                                }
                                else continue;
                            }else{
                                co=false;
                            }
                            writeXLSXExcel(data,NameTableExcel,ma,tungay,denngay);
                            return true;
                        } catch (IOException ex) {
                            Logger.getLogger(Excel.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
            return true;
        }

    public static void writeXLSExcel(JTable data, String NameTableExcel, String Ma, String tungay, String denngay) throws FileNotFoundException, IOException {
        String path = pathfile;
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(NameTableExcel);
        /////
        int rownum = 0;
        Cell cell;
        Row row;
        /////
        HSSFCellStyle style = createStyleForTitle(workbook);
        row = sheet.createRow(rownum);
        cell = row.createCell(0, CellType.STRING);
        if(tungay == null && denngay == null && (Ma.length() == 0)){
            cell.setCellValue(NameTableExcel);
        }
        else if(tungay == null && denngay == null && (Ma.length() != 0)){
            cell.setCellValue(NameTableExcel+" - "+Ma);
        }
        else if(tungay != null && denngay != null && (Ma.length() == 0)){
            cell.setCellValue(NameTableExcel+" - Từ ngày: "+ tungay+" | Đến ngày: "+denngay);
        }
        else if(tungay != null && denngay != null && (Ma.length()!=0)){
            cell.setCellValue(NameTableExcel+" - "+Ma+" - Từ ngày: "+ tungay+" | Đến ngày: "+denngay);
        }
        cell.setCellStyle(style);
        /////
        rownum++;
        row = sheet.createRow(rownum);
        /////
        TableModel model = data.getModel();
        for (int i = 0; i < model.getColumnCount(); i++) {
            cell = row.createCell(i, CellType.STRING);
            cell.setCellValue(model.getColumnName(i));
            cell.setCellStyle(style);
        }
        /////
        for (int i = 0; i < model.getRowCount(); i++) {
            rownum++;
            row = sheet.createRow(rownum);
            for (int j = 0; j < model.getColumnCount(); j++) {
                cell = row.createCell(j, CellType.STRING);
                cell.setCellValue(model.getValueAt(i, j).toString());
            }
        }
        /////
        FileOutputStream outFile = new FileOutputStream(path);
        workbook.write(outFile);
        outFile.close();
    }

    private static HSSFCellStyle createStyleForTitle(HSSFWorkbook workbook) {
        HSSFFont font = workbook.createFont();
        font.setBold(true);
        HSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        return style;
    }
    
    public static void writeXLSXExcel(JTable data, String NameTableExcel, String Ma, String tungay, String denngay) throws FileNotFoundException, IOException {
        String path = pathfile;
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(NameTableExcel);
        /////
        int rownum = 0;
        Cell cell;
        Row row;
        /////
        XSSFCellStyle style = createStyleForTitle(workbook);
        row = sheet.createRow(rownum);
        cell = row.createCell(0, CellType.STRING);
        if(tungay == null && denngay == null && (Ma.length() == 0)){
            cell.setCellValue(NameTableExcel);
        }
        else if(tungay == null && denngay == null && (Ma.length() != 0)){
            cell.setCellValue(NameTableExcel+" - "+Ma);
        }
        else if(tungay != null && denngay != null && (Ma.length() == 0)){
            cell.setCellValue(NameTableExcel+" - Từ ngày: "+ tungay+" | Đến ngày: "+denngay);
        }
        else if(tungay != null && denngay != null && (Ma.length()!=0)){
            cell.setCellValue(NameTableExcel+" - "+Ma+" - Từ ngày: "+ tungay+" | Đến ngày: "+denngay);
        }
        cell.setCellStyle(style);
        /////
        rownum++;
        row = sheet.createRow(rownum);
        /////
        TableModel model = data.getModel();
        for (int i = 0; i < model.getColumnCount(); i++) {
            cell = row.createCell(i, CellType.STRING);
            cell.setCellValue(model.getColumnName(i));
            cell.setCellStyle(style);
        }
        /////
        for (int i = 0; i < model.getRowCount(); i++) {
            rownum++;
            row = sheet.createRow(rownum);
            for (int j = 0; j < model.getColumnCount(); j++) {
                cell = row.createCell(j, CellType.STRING);
                cell.setCellValue(model.getValueAt(i, j).toString());
            }
        }
        /////
        FileOutputStream outFile = new FileOutputStream(path);
        workbook.write(outFile);
        outFile.close();
    }
    
    private static XSSFCellStyle createStyleForTitle(XSSFWorkbook workbook) {
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        XSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        return style;
    }

    public static DefaultTableModel ReadFileExcel() throws IOException{
            JFileChooser fs = new JFileChooser(new File("D:/"));
                fs.setDialogTitle("Open a file");
                fs.setFileFilter(new FileNameExtensionFilter("excel file(*.xlsx)","xlsx"));
                fs.setFileFilter(new FileNameExtensionFilter("excel file(*.xls)","xls"));
            while(true){
                int result = fs.showOpenDialog(null);
            if (result!=JFileChooser.APPROVE_OPTION){
                return null;
            }
            else{
                pathfile=fs.getSelectedFile().getAbsolutePath();
                File f=new File(pathfile);
            if (fs.getFileFilter().getDescription().equals("excel file(*.xls)")){
                    if (f.renameTo(f)){
                        return readXLSExcel();
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "File đang mở!");
                        continue;
                    }
                }
                if (fs.getFileFilter().getDescription().equals("excel file(*.xlsx)")){
                    if (f.renameTo(f)){
                        return readXLSXExcel();
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "File đang mở!");
                        continue;
                    }
            }
            }
            return null;
            }
        }
    
    public static DefaultTableModel readXLSExcel() throws FileNotFoundException, IOException{
        // Đọc một file XSL.
        FileInputStream inputStream = new FileInputStream(new File(pathfile));

        // Đối tượng workbook cho file XSL.
        HSSFWorkbook workbook = new HSSFWorkbook(inputStream);

        Sheet sheet = workbook.getSheetAt(0);
        DataFormatter dataFormatter = new DataFormatter();
        DefaultTableModel dtm = null;
        Iterator<Row> rowIterator = sheet.rowIterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (row.getRowNum() == 1) {
                int hehe = 0;
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    hehe++;
                    Cell cell = cellIterator.next();
                }
                String header[] = new String[hehe];
                int i = 0;
                cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    String cellValue = dataFormatter.formatCellValue(cell);
                    header[i] = cellValue;
                    i++;
                }
                dtm = new DefaultTableModel(header, 0);
            }
            if(row.getRowNum() > 1) {
                int hehe = 0;
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    hehe++;
                    Cell cell = cellIterator.next();
                }
                String header[] = new String[hehe];
                int i = 0;
                cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    String cellValue = dataFormatter.formatCellValue(cell);
                    header[i] = cellValue;
                    i++;
                }
                dtm.addRow(header);
            }
        }
        return dtm;
    }
    
    public static DefaultTableModel readXLSXExcel() throws FileNotFoundException, IOException {
        // Đọc một file XSL.
        FileInputStream inputStream = new FileInputStream(new File(pathfile));

        // Đối tượng workbook cho file XSL.
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

        Sheet sheet = workbook.getSheetAt(0);
        DataFormatter dataFormatter = new DataFormatter();
        DefaultTableModel dtm = null;
        Iterator<Row> rowIterator = sheet.rowIterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (row.getRowNum() == 1) {
                int hehe = 0;
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    hehe++;
                    Cell cell = cellIterator.next();
                }
                String header[] = new String[hehe];
                int i = 0;
                cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    String cellValue = dataFormatter.formatCellValue(cell);
                    header[i] = cellValue;
                    i++;
                }
                dtm = new DefaultTableModel(header, 0);
            }
            if(row.getRowNum() > 1) {
                int hehe = 0;
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    hehe++;
                    Cell cell = cellIterator.next();
                }
                String header[] = new String[hehe];
                int i = 0;
                cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    String cellValue = dataFormatter.formatCellValue(cell);
                    header[i] = cellValue;
                    i++;
                }
                dtm.addRow(header);
            }
        }
        return dtm;
    }    
    
}
