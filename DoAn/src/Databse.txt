CREATE TABLE sanbay(
	MaSanBay nvarchar(3) NOT NULL PRIMARY KEY,
    	TenSanBay nvarchar(100) NOT NULL,
    	ThanhPho nvarchar(50) NOT NULL
)
CREATE TABLE tuyenbay(
	MaTuyenBay nvarchar(20) NOT NULL PRIMARY KEY,
	MaSBCatCanh nvarchar(3) NOT NULL,
	MaSBHaCanh nvarchar(3) NOT NULL, 
	MaSBTrungGian nvarchar(3) ,
	MaSBTrungGian nvarchar(3),
	FOREIGN KEY (MaSBCatCanh) REFERENCES sanbay(MaSanBay),
	FOREIGN KEY (MaSBHaCanh) REFERENCES sanbay(MaSanBay),
	FOREIGN KEY (MaSBTrungGian) REFERENCES sanbay(MaSanBay)
)

CREATE TABLE nhanvien(
    MaNV nvarchar(5) PRIMARY KEY,
    TenNV nvarchar(100),
    GioiTinh nvarchar(5),
    NgaySinh Date,
    DiaChi nvarchar(300),
    SDT nvarchar(10),
    Gmail nvarchar(80),
    NgayBatDau Date,
    ChucVu nvarchar(30),
    Luong int,
    MatKhau nvarchar(30),
    pathImg varchar(200)
)
CREATE TABLE mohinhmaybay(
    MaMHMB nvarchar(20) PRIMARY KEY,
    SoGheThuongGia int NOT NULL,
    SoGhePhoThong int NOT NULL,
    DSGheThuongGia varchar(100) NOT NULL,
    DSGhePhoThong varchar(100) NOT NULL
)
CREATE TABLE hanghangkhong(
    MaHang nvarchar(2) PRIMARY KEY,
    TenHang nvarchar(100),
    QuocGia nvarchar(50),
    pathLoGo varchar(150)
)
CREATE TABLE maybay(
    MaMB varchar(6) PRIMARY KEY,
    MoTa nvarchar(200),
    MaMHMB nvarchar(20), 
    CONSTRAINT fk_mhmb_MaMHMB
    FOREIGN KEY (MaMHMB)
    REFERENCES mohinhmaybay (MaMHMB)
)   
CREATE TABLE chuyenbay(
    MaCB nvarchar(10) PRIMARY KEY,
    MaHang nvarchar(3) NOT NULL,	
    MaTuyenBay nvarchar(20) NOT NULL,  
    MaMB varchar(6) NOT NULL,
    SLGheThuongGia_CL int NOT NULL,
    SLGhePhoThong_CL int NOT NULL,
    GiaVeThuongGia int NOT NULL,
    GiaVePhoThong int NOT NULL,
    NgayBay DATETIME NOT NULL,
    ThoiGianDung nvarchar(20),
    TrangThai nvarchar(30) NOT NULL,
    FOREIGN KEY (MaTuyenBay) REFERENCES tuyenbay(MaTuyenBay),
    FOREIGN KEY (MaHang) REFERENCES hanghangkhong(MaHang),
    FOREIGN KEY (MaMB) REFERENCES maybay(MaMB)    
)
CREATE TABLE hanhly(
	MaHanhLy varchar(5) PRIMARY KEY,
	KhoiLuong int not null,
	Gia int not null
)
CREATE TABLE vemaybay(
    MaVe varchar(5) PRIMARY KEY,
    LoaiVe nvarchar(20) not null,
    MaCB nvarchar(10) not null,
    GheNgoi varchar(50) not null,
    MaKH varchar(10),
    MaHanhLy varchar(5) not null,
    MaCode varchar(10),     
    TinhTrang nvarchar(50) not null,
    FOREIGN KEY (MaCB) REFERENCES chuyenbay (MaCB),
    FOREIGN KEY (MaHanhLy) REFERENCES hanhly (MaHanhLy),
    FOREIGN KEY (MaKH) REFERENCES khachhang (MaKH),
    FOREIGN KEY (MaCode) REFERENCES codekm (MaCode)
)
CREATE TABLE chuongtrinhkm (
  MaCTKM varchar(10) NOT NULL,
  TenCTKM nvarchar(90) not null,
  NgayBatDau date NOT NULL,
  NgayKetThuc date NOT NULL,
  PhanTramKM int(10) NOT NULL,
  SLCode int(3) NOT NULL,
  TinhTrang nvarchar(100) not null
PRIMARY KEY (MaCTKM)
)
CREATE TABLE codekm (
    MaCode varchar(10) PRIMARY KEY,
    TinhTrang varchar(255),
    MaCTKM varchar(10),
    CONSTRAINT MaCTKM
   	FOREIGN KEY (MaCTKM)
   	REFERENCES chuongtrinhkm (MaCTKM)
)
 