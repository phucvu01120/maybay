/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.MyConnectUnit;
import DTO.CTKhuyenMai;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author LENOVO
 */
public class BCTKhuyenMai {
    MyConnectUnit connect;
    public BCTKhuyenMai(){
        this.connect=DAL.DAL.getDAL();
    }
    public ArrayList<CTKhuyenMai> getCTKhuyenMai(String Condition, String OrderBy) throws Exception{
        ResultSet rs = connect.Select("chuongtrinhkm", Condition, OrderBy);
        ArrayList<CTKhuyenMai> listCTKM = new ArrayList<>();
        while(rs.next()){
            CTKhuyenMai ctkm = new CTKhuyenMai();
            ctkm.setMaCTKM(rs.getString(1));
            ctkm.setTenCTKM(rs.getString(2));
            ctkm.setNgayBatDau(rs.getString(3));
            ctkm.setNgayKetThuc(rs.getString(4));
            ctkm.setPhanTramKM(rs.getInt(5));
            ctkm.setSLCode(rs.getInt(6));
            ctkm.setTinhTrang(rs.getString(7));
            listCTKM.add(ctkm);
        }
        return listCTKM;
    }
    public ArrayList<CTKhuyenMai> getCTKhuyenMai(String Condition) throws Exception{
        return getCTKhuyenMai(Condition,null);
    }
    public ArrayList<CTKhuyenMai> getCTKhuyenMai() throws Exception{
        return getCTKhuyenMai(null);
    }
    public void Insert(CTKhuyenMai ctkm) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("MaCTKM",ctkm.getMaCTKM());
        map.put("TenCTKM",ctkm.getTenCTKM());
        map.put("NgayBatDau",ctkm.getNgayBatDau());
        map.put("NgayKetThuc",ctkm.getNgayKetThuc());
        map.put("PhanTramKM",ctkm.getPhanTramKM());
        map.put("SLCode",ctkm.getSLCode());
        map.put("TinhTrang",ctkm.getTinhTrang());
        connect.insert("chuongtrinhkm", map);
    }
    public void Inserts(ArrayList<CTKhuyenMai> listCTKM) throws Exception{
        for(CTKhuyenMai ctkm:listCTKM){
            this.Insert(ctkm);
        }
    }
    public CTKhuyenMai getByMaCTKM(String mactkm) throws Exception{
        ArrayList<CTKhuyenMai> list=this.getCTKhuyenMai("MaCTKM='"+mactkm+"'");
        if(list.size()>0){
            return list.get(0);
        } 
        return null;
    }
    public CTKhuyenMai getByTenCTKM(String tenctkm) throws Exception{
        ArrayList<CTKhuyenMai> list=this.getCTKhuyenMai("TenCTKM='"+tenctkm+"'");
        if(list.size()>0){
            return list.get(0);
        } 
        return null;
    }
    public void Update(CTKhuyenMai ctkm) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("MaCTKM",ctkm.getMaCTKM());
        map.put("TenCTKM",ctkm.getTenCTKM());
        map.put("NgayBatDau",ctkm.getNgayBatDau());
        map.put("NgayKetThuc",ctkm.getNgayKetThuc());
        map.put("PhanTramKM",ctkm.getPhanTramKM());
        map.put("SLCode",ctkm.getSLCode());
        map.put("TinhTrang",ctkm.getTinhTrang());
        connect.update("chuongtrinhkm",map,"MaCTKM='"+ctkm.getMaCTKM()+"'");
    }
    public void Updates(ArrayList<CTKhuyenMai> listCTKM) throws Exception{
        for(CTKhuyenMai ctkm:listCTKM){
            this.Update(ctkm);
        }
    }
    public void Delete(CTKhuyenMai ctkm) throws Exception{
        this.connect.delete("chuongtrinhkm", "MaCTKM='"+ctkm.getMaCTKM()+"'");
    }
    public void Deletes(ArrayList<CTKhuyenMai> listCTKM) throws Exception{
        for(CTKhuyenMai ctkm:listCTKM){
            this.Delete(ctkm);
        }
    }
    public boolean KiemTraCTKM(ArrayList<CTKhuyenMai> listCTKM,String mactkm,String tenctkm){
        for(CTKhuyenMai Ctkm : listCTKM){
            if(Ctkm.getMaCTKM().equalsIgnoreCase(mactkm) || Ctkm.getTenCTKM().equalsIgnoreCase(tenctkm)){
                return false;
            }
        }
        return true;
    }
    public boolean KiemmTraNgay(String NgayBatDau, String NgayKetThuc){
        int year1=Integer.parseInt(NgayBatDau.substring(0,4));
        int month1=Integer.parseInt(NgayBatDau.substring(5,7));
        int date1=Integer.parseInt(NgayBatDau.substring(8,10));
        int year2=Integer.parseInt(NgayKetThuc.substring(0,4));
        int month2=Integer.parseInt(NgayKetThuc.substring(5,7));
        int date2=Integer.parseInt(NgayKetThuc.substring(8,10));
        if(year1==year2 && month1==month2 && date1<date2){
            return true;
        }
        if(year1==year2 && month1<month2){
            return true;
        }
        if(year1<year2){
            return true;
        }
        else{
            return false;
        }
    }
    public boolean checkTinhTrang(String date,String NgayCTKM){
        int year1=Integer.parseInt(date.substring(0,4));
        int month1=Integer.parseInt(date.substring(5,7));
        int date1=Integer.parseInt(date.substring(8,10));
        int year2=Integer.parseInt(NgayCTKM.substring(0,4));
        int month2=Integer.parseInt(NgayCTKM.substring(5,7));
        int date2=Integer.parseInt(NgayCTKM.substring(8,10));
        if(year1==year2 && month1==month2 && date1>=date2){
            return true;
        }
        if(year1==year2 && month1>month2){
            return true;
        }
        if(year1>year2){
            return true;
        }
        return false;
    }
}
