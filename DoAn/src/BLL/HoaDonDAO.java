/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.MyConnectUnit;
import DTO.HoaDonDTO;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class HoaDonDAO {
    MyConnectUnit connect;
    public HoaDonDAO(){
        this.connect = DAL.DAL.getDAL();
    }
    public ArrayList<HoaDonDTO> getHoaDons(String condition, String orderBy){
        ArrayList<HoaDonDTO> listHD = new ArrayList<>();
        try {
            ResultSet rs = this.connect.Select("hoadon", condition, orderBy);
            while(rs.next()){
                HoaDonDTO hd = new HoaDonDTO();
                hd.setMaHD(rs.getString(1));
                hd.setMaNV(rs.getString(2));
                hd.setNgayBan(rs.getString(3));
                hd.setMaKH(rs.getString(4));
                hd.setTongTien(Integer.parseInt(rs.getString(5)));
                listHD.add(hd);
            }
        } catch (Exception ex) {
            Logger.getLogger(HoaDonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listHD;
    }
    public ArrayList<HoaDonDTO> getHoaDons(String condition){
        return this.getHoaDons(condition, null);
    }
    public ArrayList<HoaDonDTO> getHoaDons(){
        return this.getHoaDons(null);
    }
    public void Insert(HoaDonDTO hd){
        try {
            HashMap<String, Object> map = new HashMap<>();
            map.put("MaHD", hd.getMaHD());
            map.put("MaNV", hd.getMaNV());
            map.put("NgayBan", hd.getNgayBan());
            map.put("MaKH", hd.getMaKH());
            map.put("TongTien", hd.getTongTien());
            this.connect.insert("hoadon", map);
        } catch (Exception ex) {
            Logger.getLogger(HoaDonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void Inserts(ArrayList<HoaDonDTO> list){
        for(HoaDonDTO hd : list){
            this.Insert(hd);
        }
    }
    public void Delete(HoaDonDTO hd){
        try {
            this.connect.delete("hoadon", "MaHD='"+hd.getMaHD()+"'");
        } catch (Exception ex) {
            Logger.getLogger(HoaDonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void Deletes(ArrayList<HoaDonDTO> list){
        for(HoaDonDTO hd : list){
            this.Delete(hd);
        }
    }
    public void Update(HoaDonDTO hd){
        try {
            HashMap<String, Object> map = new HashMap<>();
            map.put("MaNV", hd.getMaNV());
            map.put("NgayBan", hd.getNgayBan());
            map.put("MaKH", hd.getMaKH());
            map.put("TongTien", hd.getTongTien());
            this.connect.update("hoadon", map, "MaHD='"+hd.getMaHD()+"'");
        } catch (Exception ex) {
            Logger.getLogger(HoaDonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void Updates(ArrayList<HoaDonDTO> list){
        for(HoaDonDTO hd : list){
            this.Update(hd);
        }
    }
}
