/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.MyConnectUnit;
import DTO.HangHangKhong;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author PHUC
 */
public class BHangHangKhong {
    MyConnectUnit connect;
    public BHangHangKhong(){
        this.connect=DAL.DAL.getDAL();
    }
    public ArrayList<HangHangKhong> getHangHangKhongs(String condition, String OrderBy) throws Exception{
        ResultSet rs=this.connect.Select("hanghangkhong", condition, OrderBy);
        ArrayList<HangHangKhong> list_hmb=new ArrayList<>();
        while(rs.next()){
            list_hmb.add(new HangHangKhong(rs.getString("MaHang"), rs.getString("TenHang"), rs.getString("QuocGia"), rs.getString("pathLoGo")));
        }
        return list_hmb;
    }
    public ArrayList<HangHangKhong> getHangHangKhongs(String condition) throws Exception{
        return this.getHangHangKhongs(condition, null);
    }
    public ArrayList<HangHangKhong> getHangHangKhongs() throws Exception{
        return this.getHangHangKhongs(null);
    }
    public HangHangKhong getHangHangKhongByMaHang(String mahang) throws Exception{
        ArrayList<HangHangKhong> list_hmb=this.getHangHangKhongs("MaHang='"+mahang+"'");
        if(list_hmb.size()>0){
            return list_hmb.get(0);
        }
        return null;
    }
    public void Insert(HangHangKhong hmb) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("MaHang", hmb.getMaHang());
        map.put("TenHang", hmb.getTenHang());
        map.put("QuocGia", hmb.getQuocGia());
        map.put("pathLoGo", hmb.getPathLoGo());     
        this.connect.insert("hanghangkhong", map);
    }
    public void Inserts(ArrayList<HangHangKhong> list_mb) throws Exception{
        for(HangHangKhong mb:list_mb){
            this.Insert(mb);
        }
    }
    public void Delete(HangHangKhong hmb) throws Exception{
        this.connect.delete("hanghangkhong", "MaHang='"+hmb.getMaHang()+"'");
    }
    public void Deletes(ArrayList<HangHangKhong> list_hmb) throws Exception{
        for(HangHangKhong hmb:list_hmb){
            this.Delete(hmb);
        }
    }
    public void Update(HangHangKhong hmb) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("TenHang", hmb.getTenHang());
        map.put("QuocGia", hmb.getQuocGia());
        map.put("pathLoGo", hmb.getPathLoGo());  
        this.connect.update("hanghangkhong", map, "MaHang='"+hmb.getMaHang()+"'");
    }
    public void Updates(ArrayList<HangHangKhong> list_hmb) throws Exception{
        for(HangHangKhong mb:list_hmb){
            this.Update(mb);
        }
    }
}
