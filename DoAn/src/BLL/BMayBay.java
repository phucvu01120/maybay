/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.MyConnectUnit;
import DTO.MayBay;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author PHUC
 */
public class BMayBay {
    MyConnectUnit connect;
    public BMayBay(){
        this.connect=DAL.DAL.getDAL();
    }
    public ArrayList<MayBay> getMayBays(String condition, String OrderBy) throws Exception{
        ResultSet rs=this.connect.Select("maybay", condition, OrderBy);
        ArrayList<MayBay> list_mb=new ArrayList<>();
        while(rs.next()){
            list_mb.add(new MayBay(rs.getString("MaMB"), rs.getString("MoTa"), rs.getString("MaMHMB")));
        }
        return list_mb;
    }
    public ArrayList<MayBay> getMayBays(String condition) throws Exception{
        return this.getMayBays(condition, null);
    }
    public ArrayList<MayBay> getMayBays() throws Exception{
        return this.getMayBays(null);
    }
    public MayBay getMayBayByMaMB(String mamb) throws Exception{
        ArrayList<MayBay> list_mb=this.getMayBays("MaMB='"+mamb+"'");
        if(list_mb.size()>0)
            return list_mb.get(0);
        return null;
    }
    public void Insert(MayBay mb) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("MaMB", mb.getMaMB());
        map.put("MoTa", mb.getMota());
        map.put("MaMHMB", mb.getMaMHMB());
        this.connect.insert("maybay", map);
    }
    public void Inserts(ArrayList<MayBay> list_mb) throws Exception{
        for(MayBay mb:list_mb){
            this.Insert(mb);
        }
    }
    public void Delete(MayBay mb) throws Exception{
        this.connect.delete("maybay", "MaMB='"+mb.getMaMB()+"'");
    }
    public void Deletes(ArrayList<MayBay> list_mb) throws Exception{
        for(MayBay mb:list_mb){
            this.Delete(mb);
        }
    }
    public void Update(MayBay mb) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("MaMB", mb.getMaMB());
        map.put("MoTa", mb.getMota());
        map.put("MaMHMB", mb.getMaMHMB());
        this.connect.update("maybay", map, "MaMB='"+mb.getMaMB()+"'");
    }
    public void Updates(ArrayList<MayBay> list_mb) throws Exception{
        for(MayBay mb:list_mb){
            this.Update(mb);
        }
    }
    
}
