/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.MyConnectUnit;
import DTO.NhanVienDTO;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author PHUC
 */
public class NhanVienDAO {
    MyConnectUnit connect;

    public NhanVienDAO() {
        this.connect = DAL.DAL.getDAL();
    }
    public ArrayList<NhanVienDTO> getNhanViens(String condition, String OrderBy) throws Exception{
        ResultSet rs=this.connect.Select("nhanvien", condition, OrderBy);
        ArrayList<NhanVienDTO> list_nv=new ArrayList<>();
        while(rs.next()){
            list_nv.add(new NhanVienDTO(rs.getString("MaNV"), rs.getString("HoNV"), rs.getString("TenNV"), rs.getString("GioiTinh"), rs.getString("NgaySinh"), rs.getString("DiaChi"), rs.getString("SDT")
                    , rs.getString("Gmail"), rs.getString("NgayBatDau"), rs.getString("ChucVu"), rs.getInt("Luong"), rs.getString("MatKhau"), rs.getString("pathImg")));
        }
        return list_nv;
    }
    public ArrayList<NhanVienDTO> getNhanViens(String condition) throws Exception{
        return getNhanViens(condition, null);
    }
    public ArrayList<NhanVienDTO> getNhanViens() throws Exception{
        return getNhanViens(null);
    }

    public NhanVienDTO getNhanVienByMaNV(String manv) throws Exception{
        ArrayList<NhanVienDTO> list_nv=this.getNhanViens("MaNV='"+manv+"'");
        if(list_nv.size()>0){
            return list_nv.get(0);
        }
        return null;
    }
    public void Insert(NhanVienDTO nv) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("MaNV", nv.getMaNV());
        map.put("HoNV", nv.getHoNV());
        map.put("TenNV", nv.getTenNV());
        map.put("GioiTinh", nv.getGioiTinh());
        map.put("NgaySinh", nv.getNgaySinh());
        map.put("DiaChi", nv.getDiaChi());
        map.put("SDT", nv.getSDT());
        map.put("Gmail", nv.getGmail());
        map.put("NgayBatDau", nv.getNgayBatDau());
        map.put("ChucVu", nv.getChucVu());
        map.put("Luong", nv.getLuong());
        map.put("MatKhau", nv.getMatKhau());
        map.put("pathImg", nv.getPathImg());
        this.connect.insert("nhanvien", map);
    }
    public void Inserts(ArrayList<NhanVienDTO> list_nv) throws Exception{
        for(NhanVienDTO nv:list_nv){
            this.Insert(nv);
        }
    }
    public void Delete(NhanVienDTO nv) throws Exception{
        this.connect.delete("nhanvien", "MaNV='"+nv.getMaNV()+"'");
    }
    public void Deletes(ArrayList<NhanVienDTO> list_nv) throws Exception{
        for(NhanVienDTO nv : list_nv){
            this.Delete(nv);
        }
    }
    public void Update(NhanVienDTO nv) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("HoNV", nv.getHoNV());
        map.put("TenNV", nv.getTenNV());
        map.put("GioiTinh", nv.getGioiTinh());
        map.put("NgaySinh", nv.getNgaySinh());
        map.put("DiaChi", nv.getDiaChi());
        map.put("SDT", nv.getSDT());
        map.put("Gmail", nv.getGmail());
        map.put("NgayBatDau", nv.getNgayBatDau());
        map.put("ChucVu", nv.getChucVu());
        map.put("Luong", nv.getLuong());
        map.put("MatKhau", nv.getMatKhau());
        map.put("pathImg", nv.getPathImg());
        this.connect.update("nhanvien", map, "MaNV='"+nv.getMaNV()+"'");
    }
    public void Updates(ArrayList<NhanVienDTO> list_nv) throws Exception{
        for(NhanVienDTO nv:list_nv){
            this.Update(nv);
        }
    }
    
}
