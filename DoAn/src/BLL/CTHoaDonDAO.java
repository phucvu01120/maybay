/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.MyConnectUnit;
import DTO.CTHoaDonDTO;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PHUC
 */
public class CTHoaDonDAO {
    MyConnectUnit connect;
    public CTHoaDonDAO(){
        this.connect = DAL.DAL.getDAL();
    }
    public ArrayList<CTHoaDonDTO> getCTHoaDons(String condition, String orderBy){
        ArrayList<CTHoaDonDTO> listCTHD = new ArrayList<>();
        try {
            ResultSet rs = this.connect.Select("chitiethoadon", condition, orderBy);
            while(rs.next()){
                CTHoaDonDTO ct = new CTHoaDonDTO();
                ct.setMaHD(rs.getString(1));
                ct.setMaCB(rs.getString(2));
                ct.setGheNgoi(rs.getString(3));
                ct.setMaHanhLy(rs.getString(4));
                ct.setGiaVe(Integer.parseInt(rs.getString(5)));
                ct.setGiaHanhLi(Integer.parseInt(rs.getString(6)));
                ct.setThanhTien(Integer.parseInt(rs.getString(7)));
                listCTHD.add(ct);
            }
        } catch (Exception ex) {
            Logger.getLogger(CTHoaDonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listCTHD;
    }
    public ArrayList<CTHoaDonDTO> getCTHoaDons(String condition){
        return this.getCTHoaDons(condition, null);
    }
    public ArrayList<CTHoaDonDTO> getCTHoaDons(){
        return this.getCTHoaDons(null);
    }
    public void Insert(CTHoaDonDTO ct){
        try {
            HashMap<String, Object> map = new HashMap<>();
            map.put("MaHD", ct.getMaHD());
            map.put("MaCB", ct.getMaCB());
            map.put("GheNgoi", ct.getGheNgoi());
            map.put("MaHanhLy", ct.getMaHanhLy());
            map.put("GiaVe", ct.getGiaVe());
            map.put("GiaHanhLi", ct.getGiaHanhLi());
            map.put("ThanhTien", ct.getThanhTien());
            this.connect.insert("chitiethoadon", map);
        } catch (Exception ex) {
            Logger.getLogger(CTHoaDonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void Inserts(ArrayList<CTHoaDonDTO> list){
        for(CTHoaDonDTO ct : list){
            this.Insert(ct);
        }
    }
    public void Delete(CTHoaDonDTO ct){
        try {
            this.connect.delete("chitiethoadon", "MaHD='"+ct.getMaHD()+"' AND MaCB='"+ct.getMaCB()+"' AND GheNgoi='"+ct.getGheNgoi()+"'");
        } catch (Exception ex) {
            Logger.getLogger(CTHoaDonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void Deletes(ArrayList<CTHoaDonDTO> list){
        for(CTHoaDonDTO ct : list){
            this.Delete(ct);
        }
    }
    public void Update(CTHoaDonDTO ct){
        try {
            HashMap<String, Object> map = new HashMap<>();
            map.put("MaHanhLy", ct.getMaHanhLy());
            map.put("GiaVe", ct.getGiaVe());
            map.put("GiaHanhLi", ct.getGiaHanhLi());
            map.put("ThanhTien", ct.getThanhTien());
            this.connect.update("chitiethoadon", map, "MaHD='"+ct.getMaHD()+"' AND MaCB='"+ct.getMaCB()+"' AND GheNgoi='"+ct.getGheNgoi()+"'");
        } catch (Exception ex) {
            Logger.getLogger(CTHoaDonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void Updates(ArrayList<CTHoaDonDTO> list){
        for(CTHoaDonDTO ct : list){
            this.Update(ct);
        }
    }
}

