/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.MyConnectUnit;
import DTO.ChuyenBay;
import DTO.CodeKM;
import DTO.MayBay;
import DTO.MoHinhMayBay;
import DTO.SanBay;
import DTO.TuyenBay;
import DTO.VeMayBay;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author LENOVO
 */
public class BVeMayBay {
    MyConnectUnit connect;
    public BVeMayBay(){
        connect=DAL.DAL.getDAL();
    }
    public ArrayList<VeMayBay> getVeMayBay(String Condition,String OrderBy) throws Exception{
        ArrayList<VeMayBay> list_ve=new ArrayList<>();
        ResultSet rs=this.connect.Select("vemaybay", Condition, OrderBy);
        while(rs.next()){
            list_ve.add(new VeMayBay(rs.getString("MaCB"), rs.getString("LoaiVe"), rs.getString("GheNgoi"), rs.getString("MaKH"), rs.getString("MaCode")
                    , rs.getString("TinhTrang")));
        }
        return list_ve;
    }
    public ArrayList<VeMayBay> getVeMayBay(String Condition) throws Exception{
        return this.getVeMayBay(Condition,null);
    }
    public ArrayList<VeMayBay> getVeMayBay() throws Exception{
        return this.getVeMayBay(null);
    }
    public VeMayBay getByID(String macb,String ghe) throws Exception{
        ArrayList<VeMayBay> list=this.getVeMayBay("MaCB='"+macb+"' AND GheNgoi='"+ghe+"'");
        if(list.size()>0){
            return list.get(0);
        }
        return null;
    }
    public void Insert(VeMayBay vmb) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("MaCB",vmb.getMaCB());
        map.put("LoaiVe",vmb.getLoaiVe());
        map.put("GheNgoi",vmb.getGheNgoi());
        map.put("TinhTrang",vmb.getTinhTrang());
        this.connect.insert("vemaybay", map);
    }
    public void Inserts(ArrayList<VeMayBay> listVMB) throws Exception{
        for(VeMayBay vmb:listVMB){
            this.Insert(vmb);
        }
    }
    public void creatVe(ChuyenBay cb) throws Exception{
        //
        BMayBay bMB=new BMayBay();
        MayBay mb=bMB.getMayBayByMaMB(cb.getMaMB());
        BMoHinhMayBay bMHMB=new BMoHinhMayBay();
        MoHinhMayBay mhmb=bMHMB.getByID(mb.getMaMHMB());
        ArrayList<String> dsghe_tg=chuyen(mhmb.getDSGheThuongGia());
        ArrayList<String> dsghe_pt=chuyen(mhmb.getDSGhePhoThong());
        for(int i=1;i<=mhmb.getSoGheThuongGia();i++){
            VeMayBay vmb=new VeMayBay();
            vmb.setLoaiVe("Thương gia");
            vmb.setMaCB(cb.getMaCB());
            vmb.setGheNgoi(dsghe_tg.get(i-1));
            vmb.setTinhTrang("Còn trống");
            this.Insert(vmb);
        }
        for(int j=1;j<=mhmb.getSoGhePhoThong();j++){
            VeMayBay vmb=new VeMayBay();
            vmb.setLoaiVe("Phổ thông");
            vmb.setMaCB(cb.getMaCB());
            vmb.setGheNgoi(dsghe_pt.get(j-1));
            vmb.setTinhTrang("Còn trống");
            this.Insert(vmb);
        }
    }
    public void UpdateMuaVe(VeMayBay vmb) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("MaKH", vmb.getMaKH());
        if(vmb.getMaCode()!=null){
        map.put("MaCode", vmb.getMaCode());
        BCodeKM bCODE=new BCodeKM();
        CodeKM code=bCODE.getByMaCodeKM(vmb.getMaCode());//nay tui viet them mat r ha
        bCODE.updateTT(code);
        }
        map.put("TinhTrang", vmb.getTinhTrang());
        this.connect.update("vemaybay", map, "MaCB='"+vmb.getMaCB()+"' AND GheNgoi='"+vmb.getGheNgoi()+"'");
    }
    public void UpdateMuaVes(ArrayList<VeMayBay> list) throws Exception{
        for(VeMayBay vmb:list){
            this.UpdateMuaVe(vmb);
        }
    }
    public static ArrayList<String> chuyen(String s){
        s=s+",";
        ArrayList<String> list=new ArrayList<>();
        int position=0;
        for(int i=0;i<s.length();i++){
            if(s.charAt(i)==','){
                String k=s.substring(position,i);
                list.add(k);
                position=i+1;
            }
        }
        ArrayList<String> dscn=new ArrayList<>();
        for(String a:list){
            for(int i=0;i<a.length();i++){
                String cot=Character.toString(a.charAt(0));
                if(a.charAt(i)=='-'){
                    int min=Integer.parseInt(a.substring(1,i));
                    int max=Integer.parseInt(a.substring(i+2,a.length()));
                    for(int j=min;j<=max;j++){
                        dscn.add(j+cot);
                    }
                }
            }
        }
        return dscn;
    }
    public void UpdateHuyVe(VeMayBay vmb) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("TinhTrang", "Đã bị hủy");
        this.connect.update("vemaybay", map, "MaCB='"+vmb.getMaCB()+"' AND GheNgoi='"+vmb.getGheNgoi()+"'");
    }
    public void UpdatesHuyVe(ChuyenBay cb) throws Exception{
        ArrayList<VeMayBay> list_ve=this.getVeMayBay("MaCB='"+cb.getMaCB()+"'");
        for(VeMayBay vmb:list_ve){
            this.UpdateHuyVe(vmb);
        }
    }
}
