/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.MyConnectUnit;
import DTO.ChuyenBay;
import DTO.MayBay;
import DTO.MoHinhMayBay;
import DTO.SanBay;
import DTO.TuyenBay;
import DTO.VeMayBay;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


/**
 *
 * @author PHUC
 */
public class BChuyenBay {
    MyConnectUnit connect;
    public BChuyenBay(){
        this.connect=DAL.DAL.getDAL();
    }
    public ArrayList<ChuyenBay> getChuyenBays(String Condition, String OrderBy) throws Exception{
        ResultSet rs=this.connect.Select("chuyenbay",Condition,OrderBy);
        ArrayList<ChuyenBay> list_cb=new ArrayList<>();
        while(rs.next()){
            ChuyenBay cb=new ChuyenBay(rs.getString("MaCB"), rs.getString("MaHang"), rs.getString("MaTuyenBay"), rs.getString("MaMB"), rs.getInt("SLGheThuongGia_CL")
                    , rs.getInt("SLGhePhoThong_CL"), rs.getInt("GiaVeThuongGia"), rs.getInt("GiaVePhoThong"), rs.getString("NgayBay"), rs.getString("NgayBayTT"), rs.getString("TrangThai"));
            list_cb.add(cb);
        }
        return list_cb;
    }
    public ArrayList<ChuyenBay> getChuyenBays(String Condition) throws Exception{
        return this.getChuyenBays(Condition, null);
    }
    public ArrayList<ChuyenBay> getChuyenBays() throws Exception{
        return this.getChuyenBays(null);
    }
    public ChuyenBay getCBByMaCB(String macb) throws Exception{
        ArrayList<ChuyenBay> list=getChuyenBays("MaCB='"+macb+"'");
        if(list.size()>0){
            return list.get(0);
        }
        return null;
    }
    public void Insert(ChuyenBay cb,boolean type) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("MaCB", cb.getMaCB());
        map.put("MaHang", cb.getMaHang());
        map.put("MaTuyenBay", cb.getMaTuyenBay());
        map.put("MaMB", cb.getMaMB());
        map.put("SLGheThuongGia_CL", cb.getSLGheThuongGia_CL());
        map.put("SLGhePhoThong_CL", cb.getSLGhePhoThong_CL());
        map.put("GiaVeThuongGia", cb.getGiaVeThuongGia());
        map.put("GiaVePhoThong", cb.getGiaVePhoThong());
        map.put("NgayBay", cb.getNgayBay());
        //
        if(type==true){
            map.put("NgayBayTT", cb.getNgayBayTT());
        }
        map.put("TrangThai", cb.getTrangThai());
        this.connect.insert("chuyenbay", map);
    }
    public long tinhngay(String ngaybay) throws ParseException{
        Date HomNay=new Date();
        String ngay=ngaybay.substring(0,10);
        java.util.Date NgayBay =  new SimpleDateFormat("yyyy-MM-dd").parse(ngay);
        Calendar chnay=Calendar.getInstance();
        Calendar cnbay=Calendar.getInstance();
        chnay.setTime(HomNay);
        cnbay.setTime(NgayBay);
        return (cnbay.getTime().getTime()-chnay.getTime().getTime())/(24*3600*1000);
    }
    public String capnhatTrangThai(String ngaybay, String mamb, int veconlai) throws ParseException, Exception{
        long ngayconali=tinhngay(ngaybay);
        BMayBay bMB=new BMayBay();
        MayBay mb=bMB.getMayBayByMaMB(mamb);
        BMoHinhMayBay bMHMB=new BMoHinhMayBay();
        MoHinhMayBay mhmb=bMHMB.getByID(mb.getMaMHMB());
        int tongsove = mhmb.getSoGhePhoThong()+mhmb.getSoGheThuongGia();
        int TiLeVeConLai=(veconlai/tongsove)*100;
        if(ngayconali<0){
            return "Đã cất cánh";
        }
        if(TiLeVeConLai >=70 && ngayconali<=7){
            return "Báo động";
        }
        return "Bình Thường";
    }
    public boolean checkNgaybayTT(String nbay,int hbay,String nbaytt,int hbaytt) throws ParseException{
        java.util.Date NgayBay =  new SimpleDateFormat("yyyy-MM-dd").parse(nbay);
        java.util.Date NgayBayTT =  new SimpleDateFormat("yyyy-MM-dd").parse(nbaytt);  
        Calendar calendarNgayBay=Calendar.getInstance();
        Calendar calendarNgayBayTT=Calendar.getInstance();
        calendarNgayBay.setTime(NgayBay);
        calendarNgayBayTT.setTime(NgayBayTT);
        long kc=(calendarNgayBayTT.getTime().getTime()-calendarNgayBay.getTime().getTime())/(24*3600*1000);
        if(kc<0){
            return false;
        }
        if(kc==0){
            if(hbay>(hbaytt-2)){
                return false;
            }
        }
        return true;
    }
    public void Delete(ChuyenBay cb) throws Exception{
        this.connect.delete("chuyenbay", "MaCB='"+cb.getMaCB()+"'");
    }
    public void UpdateTrangThai(ChuyenBay cb) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("TrangThai", cb.getTrangThai());
        this.connect.update("chuyenbay", map, "MaCB='"+cb.getMaCB()+"'");
    }
    public void UpdateSL(ArrayList<VeMayBay> list) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        VeMayBay vmb=list.get(0);
        ChuyenBay cb=this.getCBByMaCB(vmb.getMaCB());
        if(vmb.getLoaiVe().equals("Thương gia")){
            map.put("SLGheThuongGia_CL", (cb.getSLGheThuongGia_CL()-list.size()));
        }
        if(vmb.getLoaiVe().equals("Phổ thông")){
            map.put("SLGhePhoThong_CL", (cb.getSLGhePhoThong_CL()-list.size()));
        }
        this.connect.update("chuyenbay", map, "MaCB='"+cb.getMaCB()+"'");
    }
}
