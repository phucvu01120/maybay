/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.MyConnectUnit;
import DTO.SanBay;
import DTO.TuyenBay;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author LENOVO
 */
public class BSanBay {
    MyConnectUnit connect;
    public BSanBay(){
        connect=DAL.DAL.getDAL();
    }
    public ArrayList<SanBay> getSanBays(String Condition, String OrderBy) throws Exception{
        ResultSet rs = connect.Select("sanbay", Condition, OrderBy);
        ArrayList<SanBay> listSB = new ArrayList<>();
        while(rs.next()){
            SanBay sb = new SanBay();
            sb.setMaSanBay(rs.getString(1));
            sb.setTenSanBay(rs.getString(2));
            sb.setThanhPho(rs.getString(3));
            listSB.add(sb);
        }
        return listSB;
    }
    public ArrayList<SanBay> getSanBays(String Condition) throws Exception{
        return getSanBays(Condition,null);
    }
    public ArrayList<SanBay> getSanBays() throws Exception{
        return getSanBays(null);
    }
    public SanBay getSanBayByMaSB(String MaSanBay) throws Exception{
        ArrayList<SanBay> listSB=this.getSanBays("MaSanBay='"+MaSanBay+"'");
        if(listSB.size()>0)
            return listSB.get(0);
        return null;
    }
    public SanBay getSanBayByThanhPho(String ThanhPho) throws Exception{
        ArrayList<SanBay> listSB=this.getSanBays("ThanhPho='"+ThanhPho+"'");
        if(listSB.size()>0)
            return listSB.get(0);
        return null;
    }
    public SanBay getSBTGbyMaCB(String macb) throws Exception{
        BTuyenBay bTB=new BTuyenBay();
        TuyenBay tb=bTB.getByID(macb);
        BSanBay bSB=new BSanBay();
        SanBay sbtg=bSB.getSanBayByMaSB(tb.getMaSBTrungGian());
        return sbtg;
    }
    public void Insert(SanBay sb) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("MaSanBay",sb.getMaSanBay());
        map.put("TenSanBay",sb.getTenSanBay());
        map.put("ThanhPho",sb.getThanhPho());
        connect.insert("sanbay", map);
    }
     public void Inserts(ArrayList<SanBay> listSB) throws Exception{
        for(SanBay sb:listSB){
            this.Insert(sb);
        }
    }
    public void Update(SanBay sb) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("MaSanBay",sb.getMaSanBay());
        map.put("TenSanBay",sb.getTenSanBay());
        map.put("ThanhPho",sb.getThanhPho());
        connect.update("sanbay",map,"MaSanBay='"+sb.getMaSanBay()+"'");
    }
    public void Updates(ArrayList<SanBay> listSB) throws Exception{
        for(SanBay sb:listSB){
            this.Update(sb);
        }
    }
    public void Delete(SanBay sb) throws Exception{
        this.connect.delete("sanbay", "MaSanBay='"+sb.getMaSanBay()+"'");
    }
    public void Deletes(ArrayList<SanBay> listSB) throws Exception{
        for(SanBay sb:listSB){
            this.Delete(sb);
        }
    }
    public boolean KiemTraMaSB(ArrayList<SanBay> listSB,String masb){
        for(SanBay sb : listSB){
            if((sb.getMaSanBay().equalsIgnoreCase(masb)) || (masb.equalsIgnoreCase(""))==true){
                return false;
            }
        }
        return true;
    }
    public boolean KiemTraTenSB(ArrayList<SanBay> listSB,String tensb){
        for(SanBay sb : listSB){
            if((sb.getMaSanBay().equalsIgnoreCase(tensb)) && (tensb.equalsIgnoreCase(""))==true){
                return false;
            }
        }
        return true;
    }
}
