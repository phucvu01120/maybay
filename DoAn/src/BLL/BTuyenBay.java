/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.MyConnectUnit;
import DTO.SanBay;
import DTO.TuyenBay;
import java.util.ArrayList;
import java.sql.*;
import java.util.HashMap;

/**
 *
 * @author LENOVO
 */
public class BTuyenBay {
    MyConnectUnit connect;
    public BTuyenBay(){
        connect=DAL.DAL.getDAL();
    }
    public ArrayList<TuyenBay> getTuyenBays(String Condition, String OrderBy) throws Exception{
        ResultSet rs = connect.Select("tuyenbay", Condition, OrderBy);
        ArrayList<TuyenBay> listTB = new ArrayList<>();
        while(rs.next()){
            TuyenBay tb = new TuyenBay();
            tb.setMaTuyenBay(rs.getString(1));
            tb.setMaSBCatCanh(rs.getString(2));
            tb.setMaSBHaCanh(rs.getString(3));
            tb.setMaSBTrungGian(rs.getString(4));
            listTB.add(tb);
        }
        return listTB;
    }
     public ArrayList<TuyenBay> getTuyenBays(String Condition) throws Exception{
         return getTuyenBays(Condition,null);
     }
     public ArrayList<TuyenBay> getTuyenBays() throws Exception{
         return getTuyenBays(null);
     }
    public TuyenBay getByID(String matb) throws Exception{
        ArrayList<TuyenBay> list=this.getTuyenBays("MaTuyenBay='"+matb+"'");
        if(list.size()>0){
            return list.get(0);
        } 
        return null;
     }
     public void Insert(TuyenBay tb) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("MaTuyenBay",tb.getMaTuyenBay());
        map.put("MaSBCatCanh",tb.getMaSBCatCanh());
        map.put("MaSBHaCanh",tb.getMaSBHaCanh());
        map.put("MaSBTrungGian",tb.getMaSBTrungGian());
        
        connect.insert("tuyenbay", map);
    }
    public void Inserts(ArrayList<TuyenBay> listTB) throws Exception{
        for(TuyenBay tb:listTB){
            this.Insert(tb);
        }
    }
    public void Update(TuyenBay tb) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        //map.put("MaTuyenBay",tb.getMaTuyenBay());
        map.put("MaSBCatCanh",tb.getMaSBCatCanh());
        map.put("MaSBHaCanh",tb.getMaSBHaCanh());
        map.put("MaSBTrungGian",tb.getMaSBTrungGian());
        connect.update("tuyenbay",map,"MaTuyenBay='"+tb.getMaTuyenBay()+"'");
    }
    public void Updates(ArrayList<TuyenBay> listTB) throws Exception{
        for(TuyenBay tb:listTB){
            this.Update(tb);
        }
    }
    public void Delete(TuyenBay tb) throws Exception{
        this.connect.delete("tuyenbay", "MaTuyenBay='"+tb.getMaTuyenBay()+"'");
    }
    public void Deletes(ArrayList<TuyenBay> listSB) throws Exception{
        for(TuyenBay tb:listSB){
            this.Delete(tb);
        }
    }
    
    public boolean KiemTraMaTB(ArrayList<TuyenBay> listTB,String matb){
        for(TuyenBay tb : listTB){
            if(tb.getMaTuyenBay().equalsIgnoreCase(matb) || matb.equalsIgnoreCase("")==true){
                return false;
            }
        }
        return true;
    }
    //wrong
    public boolean KiemTraTrungNhau(ArrayList<TuyenBay> listTB, TuyenBay tuyenbay){
        for(TuyenBay tb : listTB){
            if(tb.getMaSBCatCanh().equalsIgnoreCase(tuyenbay.getMaSBCatCanh()) && tb.getMaSBHaCanh().equalsIgnoreCase(tuyenbay.getMaSBHaCanh())
                    && tb.getMaSBTrungGian().equalsIgnoreCase(tuyenbay.getMaSBTrungGian())){
                return false;
            }
        }
        return true;
    }
    
    public boolean KiemTraSB(ArrayList<SanBay> listSB,String masb){
        for(SanBay sb : listSB){
            if((sb.getMaSanBay().equalsIgnoreCase(masb)) && (masb.equalsIgnoreCase(""))==false){
                return true;
            }
        }
        return false;
    }
    
    public boolean KiemTraSBTG(ArrayList<SanBay> listSB,String masb){
        for(SanBay sb : listSB){
            if((sb.getMaSanBay().equalsIgnoreCase(masb))){
                return true;
            }
        }
        return false;
    }

    public boolean KiemTra(String a, String b, String c){
        if(a.equalsIgnoreCase(b) || a.equalsIgnoreCase(c) || b.equalsIgnoreCase(c)){
            return false;
        }
        
            return true;
        
    }
}
