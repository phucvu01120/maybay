/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.MyConnectUnit;
import DTO.MoHinhMayBay;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author PHUC
 */
public class BMoHinhMayBay {
    MyConnectUnit connect;
    public BMoHinhMayBay(){
        this.connect=DAL.DAL.getDAL();
    }
    public ArrayList<MoHinhMayBay> getMoHinhMayBays(String condition, String OrderBy) throws Exception{
        ResultSet rs=this.connect.Select("mohinhmaybay", condition, OrderBy);
        ArrayList<MoHinhMayBay> list=new ArrayList<>();
        while(rs.next()){
            list.add(new MoHinhMayBay(rs.getString("MaMHMB"), rs.getInt("SoGheThuongGia"), rs.getInt("SoGhePhoThong"), rs.getString("DSGheThuongGia"), rs.getString("DSGhePhoThong")));
        }
        return list;
    }
    public ArrayList<MoHinhMayBay> getMoHinhMayBays(String condition) throws Exception{
        return getMoHinhMayBays(condition, null);
    }
    public ArrayList<MoHinhMayBay> getMoHinhMayBays() throws Exception{
        return getMoHinhMayBays(null);
    }
    public MoHinhMayBay getByID(String id) throws Exception{
        ArrayList<MoHinhMayBay> list=this.getMoHinhMayBays("MaMHMB='"+id+"'");
        if(list.size()>0){
            return list.get(0);
        }
        return null;
    }
}
