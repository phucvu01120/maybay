/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.MyConnectUnit;
import DTO.CTKhuyenMai;
import DTO.CodeKM;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author LENOVO
 */
public class BCodeKM {
    MyConnectUnit connect;
    public BCodeKM(){
        this.connect=DAL.DAL.getDAL();
    }
    public ArrayList<CodeKM> getCodeKM(String Condition, String OrderBy) throws Exception{
        ResultSet rs = connect.Select("codekm", Condition, OrderBy);
        ArrayList<CodeKM> listCKM = new ArrayList<>();
        while(rs.next()){
            CodeKM ckm = new CodeKM();
            ckm.setMaCode(rs.getString(1));
            ckm.setTinhTrang(rs.getString(2));
            ckm.setMaCTKM(rs.getString(3));
            listCKM.add(ckm);
        }
        return listCKM;
    }
    public ArrayList<CodeKM> getCodeKM(String Condition) throws Exception{
        return getCodeKM(Condition,null);
    }
    public ArrayList<CodeKM> getCodeKM() throws Exception{
        return getCodeKM(null);
    }
    public void Insert(CodeKM ckm) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("MaCode",ckm.getMaCode());
        map.put("TinhTrang",ckm.getTinhTrang());
        map.put("MaCTKM",ckm.getMaCTKM());
        connect.insert("codekm", map);
    }
    public void Inserts(ArrayList<CodeKM> listCKM) throws Exception{
        for(CodeKM ctm:listCKM){
            this.Insert(ctm);
        }
    }
    public CodeKM getByMaCodeKM(String mackm) throws Exception{
        ArrayList<CodeKM> list=this.getCodeKM("MaCode='"+mackm+"'");
        if(list.size()>0){
            return list.get(0);
        } 
        return null;
     }
    public CodeKM getByMaCTKM(String mactkm) throws Exception{
        ArrayList<CodeKM> list=this.getCodeKM("MaCTKM='"+mactkm+"'");
        if(list.size()>0){
            return list.get(1);
        } 
        return null;
     }
     public void Update(CodeKM ckm) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("MaCode",ckm.getMaCode());
        map.put("TinhTrang",ckm.getTinhTrang());
        map.put("MaCTKM",ckm.getMaCTKM());
        connect.update("codekm",map,"MaCode='"+ckm.getMaCode()+"'");
    }
    public void Updates(ArrayList<CodeKM> listCKM) throws Exception{
        for(CodeKM ckm:listCKM){
            this.Update(ckm);
        }
    }
    public void Delete(CodeKM ckm) throws Exception{
        this.connect.delete("codekm", "MaCode='"+ckm.getMaCode()+"'");
    }
    public void DeleteMaCTKM(String ckm) throws Exception{
        this.connect.delete("codekm", "MaCTKM='"+ckm+"'");
    }
    public void Deletes(ArrayList<CodeKM> listCKM) throws Exception{
        for(CodeKM ckm:listCKM){
            this.Delete(ckm);
        }
    }
    public void Update(CTKhuyenMai ctkm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void updateTT(CodeKM code) throws Exception{
        HashMap<String,Object> map=new HashMap<>();
        map.put("TinhTrang","Đã sử dụng");
        connect.update("codekm",map,"MaCode='"+code.getMaCode()+"'");
    }
}
