/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.MyConnectUnit;
import DTO.KhachHangDTO;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PHUC
 */
public class KhachHangDAO {
    MyConnectUnit connect;
    public KhachHangDAO(){
        this.connect = DAL.DAL.getDAL();
    }
    public ArrayList<KhachHangDTO> getKhachHangs(String condition, String orderBy){
        ArrayList<KhachHangDTO> list_KH = new ArrayList<>();
        try {
            ResultSet rs = this.connect.Select("khachhang", condition, orderBy);
            while(rs.next()){
                KhachHangDTO kh = new KhachHangDTO();
                kh.setMaKH(rs.getString(1));
                kh.setHoKH(rs.getString(2));
                kh.setTenKH(rs.getString(3));
                kh.setSDT(rs.getString(4));
                kh.setGmail(rs.getString(5));
                kh.setGioiTinh(rs.getString(6));
                kh.setNgaySinh(rs.getString(7));
                list_KH.add(kh);
            }
        } catch (Exception ex) {
            Logger.getLogger(KhachHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list_KH;
    }
    public ArrayList<KhachHangDTO> getKhachHangs(String condition){
        return this.getKhachHangs(condition, null);
    }
    public ArrayList<KhachHangDTO> getKhachHangs(){
        return this.getKhachHangs(null);
    }
     public KhachHangDTO getKhachHangByMaKH(String makh) throws Exception{
        ArrayList<KhachHangDTO> list_kh=this.getKhachHangs("MaKH='"+makh+"'");
        if(list_kh.size()>0){
            return list_kh.get(0);
        }
        return null;
    }
    public void Insert(KhachHangDTO kh){
        try {
            HashMap<String, Object> hashM = new HashMap<>();
            hashM.put("MaKH", kh.getMaKH());
            hashM.put("HoKH", kh.getHoKH());
            hashM.put("TenKH", kh.getTenKH());
            hashM.put("SDT", kh.getSDT());
            hashM.put("Gmail", kh.getGmail());
            hashM.put("GioiTinh", kh.getGioiTinh());
            hashM.put("NgaySinh", kh.getNgaySinh());
            this.connect.insert("khachhang", hashM);//
        } catch (Exception ex) {
            Logger.getLogger(KhachHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void Inserts(ArrayList<KhachHangDTO> list){
        for(KhachHangDTO kh : list){
            this.Insert(kh);
        }
    }
    public void Delete(KhachHangDTO kh){
        try {
            this.connect.delete("khachhang", "MaKH='"+kh.getMaKH()+"'");
        } catch (Exception ex) {
            Logger.getLogger(KhachHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void Deletes(ArrayList<KhachHangDTO> list){
        for(KhachHangDTO kh : list){
            this.Delete(kh);
        }
    }
    public void Update(KhachHangDTO kh) throws Exception{
            HashMap<String, Object> map = new HashMap<>();
            map.put("HoKH", kh.getHoKH());
            map.put("TenKH", kh.getTenKH());
            map.put("SDT", kh.getSDT());
            map.put("Gmail", kh.getGmail());
            map.put("GioiTinh", kh.getGioiTinh());
            map.put("NgaySinh", kh.getNgaySinh());
            connect.update("khachhang",map,"MaKH='"+kh.getMaKH()+"'");
    }
    public void Updates(ArrayList<KhachHangDTO> list) throws Exception{
        for(KhachHangDTO kh : list){
            this.Update(kh);
        }
    }
}
