/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.MyConnectUnit;
import DTO.HanhLy;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author PHUC
 */
public class BHanhLy {
    MyConnectUnit connect;
    public BHanhLy(){
        this.connect=DAL.DAL.getDAL();
    }
    public ArrayList<HanhLy> getHanhLys(String condition, String OrderBy) throws Exception{
        ResultSet rs=this.connect.Select("hanhly", condition, OrderBy);
        ArrayList<HanhLy> list=new ArrayList<>();
        while(rs.next()){
            list.add(new HanhLy(rs.getString("MaHanhLy"), rs.getInt("GiaHanhLy"), rs.getInt("KhoiLuong")));
        }
        return list;
    }
    public ArrayList<HanhLy> getHanhLys(String condition) throws Exception{
        return getHanhLys(condition, null);
    }
    public ArrayList<HanhLy> getHanhLys() throws Exception{
        return getHanhLys(null);
    }
    public HanhLy getByID(String id) throws Exception{
        ArrayList<HanhLy> list=this.getHanhLys("MaHanhLy='"+id+"'");
        if(list.size()>0){
            return list.get(0);
        }
        return null;
    }
}
