/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Form.MoHinhChoNgoi;

import DTO.VeMayBay;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JButton;
import javax.swing.JOptionPane;

/**
 *
 * @author PHUC
 */
public class MoHinh14 extends javax.swing.JDialog {

    private static ArrayList<VeMayBay> list_vmb;
    ArrayList<JButton> list_button;
    private static String loaive;
    private static String Ghe_static;
    /** Creates new form MoHinh14 */
    public MoHinh14(java.awt.Frame parent, boolean modal,ArrayList<VeMayBay> list,String loaive) {
                super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.list_vmb=list;
        this.loaive=loaive;    
        list_button=new ArrayList<>();
        addListbutton();
        sort();
        setGhe();
    }
    public VeMayBay find(String ghe){
        for(VeMayBay vmb:list_vmb){
            if(vmb.getGheNgoi().equals(ghe))
                return vmb;
        }
        return null;
    }
    public void sort(){
        Collections.sort(list_vmb, (VeMayBay t, VeMayBay t1) -> (t.getGheNgoi().compareTo(t1.getGheNgoi())));
        Collections.sort(list_button, (JButton t, JButton t1) -> (t.getText().compareTo(t1.getText())));
        
    }
    public void setGhe(){
        for(int i=0;i<list_button.size();i++){
            if(list_vmb.get(i).getTinhTrang().equals("Đã thanh toán") || !list_vmb.get(i).getLoaiVe().equals(loaive)){
                list_button.get(i).setBackground(Color.red);               
            }
        }
    }
    public void addListbutton(){
        list_button=new ArrayList<>();
        list_button.add(btn1A);
        list_button.add(btn2A);
        list_button.add(btn3A);
        list_button.add(btn4A);
        list_button.add(btn5A);
        list_button.add(btn6A);
        list_button.add(btn7A);
        list_button.add(btn1B);
        list_button.add(btn2B);
        list_button.add(btn3B);
        list_button.add(btn4B);
        list_button.add(btn5B);
        list_button.add(btn6B);
        list_button.add(btn7B);   
    }
    public void setEventGhe(String ghe,JButton button){
        VeMayBay vmb=find(ghe);
        if(vmb.getTinhTrang().equals("Đã thanh toán") || !vmb.getLoaiVe().equals(loaive)){
                button.setBackground(Color.red);
                    JOptionPane.showMessageDialog(null, "Bạn không thể chọn ghế này");              
            }
            else{
                Ghe_static=vmb.getGheNgoi();
                this.dispose();
            }
        
    }

                                
    public String getGhechon(){
        return this.Ghe_static;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btn7B = new javax.swing.JButton();
        btn6A = new javax.swing.JButton();
        btn5A = new javax.swing.JButton();
        btn6B = new javax.swing.JButton();
        btn5B = new javax.swing.JButton();
        btn4A = new javax.swing.JButton();
        btn4B = new javax.swing.JButton();
        btn3A = new javax.swing.JButton();
        btn3B = new javax.swing.JButton();
        btn2A = new javax.swing.JButton();
        btn2B = new javax.swing.JButton();
        btn1A = new javax.swing.JButton();
        btn1B = new javax.swing.JButton();
        btn7A = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);

        btn7B.setBackground(new java.awt.Color(0, 204, 204));
        btn7B.setText("7B");
        btn7B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn7BActionPerformed(evt);
            }
        });
        jPanel1.add(btn7B);
        btn7B.setBounds(340, 430, 50, 90);

        btn6A.setBackground(new java.awt.Color(0, 204, 204));
        btn6A.setText("6A");
        btn6A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn6AActionPerformed(evt);
            }
        });
        jPanel1.add(btn6A);
        btn6A.setBounds(420, 240, 50, 90);

        btn5A.setBackground(new java.awt.Color(0, 204, 204));
        btn5A.setText("5A");
        btn5A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn5AActionPerformed(evt);
            }
        });
        jPanel1.add(btn5A);
        btn5A.setBounds(500, 240, 50, 90);

        btn6B.setBackground(new java.awt.Color(0, 204, 204));
        btn6B.setText("6B");
        btn6B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn6BActionPerformed(evt);
            }
        });
        jPanel1.add(btn6B);
        btn6B.setBounds(420, 430, 50, 90);

        btn5B.setBackground(new java.awt.Color(0, 204, 204));
        btn5B.setText("5B");
        btn5B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn5BActionPerformed(evt);
            }
        });
        jPanel1.add(btn5B);
        btn5B.setBounds(500, 430, 50, 90);

        btn4A.setBackground(new java.awt.Color(0, 204, 204));
        btn4A.setText("4A");
        btn4A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn4AActionPerformed(evt);
            }
        });
        jPanel1.add(btn4A);
        btn4A.setBounds(580, 240, 50, 90);

        btn4B.setBackground(new java.awt.Color(0, 204, 204));
        btn4B.setText("4B");
        btn4B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn4BActionPerformed(evt);
            }
        });
        jPanel1.add(btn4B);
        btn4B.setBounds(580, 430, 50, 90);

        btn3A.setBackground(new java.awt.Color(153, 153, 0));
        btn3A.setText("3A");
        btn3A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn3AActionPerformed(evt);
            }
        });
        jPanel1.add(btn3A);
        btn3A.setBounds(660, 240, 50, 90);

        btn3B.setBackground(new java.awt.Color(153, 153, 0));
        btn3B.setText("3B");
        btn3B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn3BActionPerformed(evt);
            }
        });
        jPanel1.add(btn3B);
        btn3B.setBounds(660, 430, 50, 90);

        btn2A.setBackground(new java.awt.Color(153, 153, 0));
        btn2A.setText("2A");
        btn2A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2AActionPerformed(evt);
            }
        });
        jPanel1.add(btn2A);
        btn2A.setBounds(740, 240, 50, 90);

        btn2B.setBackground(new java.awt.Color(153, 153, 0));
        btn2B.setText("2B");
        btn2B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2BActionPerformed(evt);
            }
        });
        jPanel1.add(btn2B);
        btn2B.setBounds(740, 430, 50, 90);

        btn1A.setBackground(new java.awt.Color(153, 153, 0));
        btn1A.setText("1A");
        btn1A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1AActionPerformed(evt);
            }
        });
        jPanel1.add(btn1A);
        btn1A.setBounds(820, 240, 50, 90);

        btn1B.setBackground(new java.awt.Color(153, 153, 0));
        btn1B.setText("1B");
        btn1B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1BActionPerformed(evt);
            }
        });
        jPanel1.add(btn1B);
        btn1B.setBounds(820, 430, 50, 90);

        btn7A.setBackground(new java.awt.Color(0, 204, 204));
        btn7A.setText("7A");
        btn7A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn7AActionPerformed(evt);
            }
        });
        jPanel1.add(btn7A);
        btn7A.setBounds(340, 240, 50, 90);

        jLabel1.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\MoHinhChoNgoi\\mh14.png")); // NOI18N
        jPanel1.add(jLabel1);
        jLabel1.setBounds(0, 0, 1450, 710);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1460, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 718, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn7BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn7BActionPerformed
        this.setEventGhe(btn7B.getText(), btn7B);
    }//GEN-LAST:event_btn7BActionPerformed

    private void btn6AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn6AActionPerformed
        this.setEventGhe(btn6A.getText(), btn6A);
    }//GEN-LAST:event_btn6AActionPerformed

    private void btn5AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn5AActionPerformed
        this.setEventGhe(btn5A.getText(), btn5A);
    }//GEN-LAST:event_btn5AActionPerformed

    private void btn6BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn6BActionPerformed
        this.setEventGhe(btn6B.getText(), btn6B);
    }//GEN-LAST:event_btn6BActionPerformed

    private void btn5BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn5BActionPerformed
        this.setEventGhe(btn5B.getText(), btn5B);
    }//GEN-LAST:event_btn5BActionPerformed

    private void btn4AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn4AActionPerformed
        this.setEventGhe(btn4A.getText(), btn4A);
    }//GEN-LAST:event_btn4AActionPerformed

    private void btn4BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn4BActionPerformed
        this.setEventGhe(btn4B.getText(), btn4B);
    }//GEN-LAST:event_btn4BActionPerformed

    private void btn3AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn3AActionPerformed
        this.setEventGhe(btn3A.getText(), btn3A);
    }//GEN-LAST:event_btn3AActionPerformed

    private void btn3BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn3BActionPerformed
        this.setEventGhe(btn3B.getText(), btn3B);
    }//GEN-LAST:event_btn3BActionPerformed

    private void btn2AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2AActionPerformed
        this.setEventGhe(btn2A.getText(), btn2A);
    }//GEN-LAST:event_btn2AActionPerformed

    private void btn2BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2BActionPerformed
        this.setEventGhe(btn2B.getText(), btn2B);
    }//GEN-LAST:event_btn2BActionPerformed

    private void btn1AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1AActionPerformed
        this.setEventGhe(btn1A.getText(), btn1A);
    }//GEN-LAST:event_btn1AActionPerformed

    private void btn1BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1BActionPerformed
        this.setEventGhe(btn1B.getText(), btn1B);
    }//GEN-LAST:event_btn1BActionPerformed

    private void btn7AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn7AActionPerformed
        this.setEventGhe(btn7A.getText(), btn7A);
    }//GEN-LAST:event_btn7AActionPerformed

    /**
     * @param args the command line arguments
     */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn1A;
    private javax.swing.JButton btn1B;
    private javax.swing.JButton btn2A;
    private javax.swing.JButton btn2B;
    private javax.swing.JButton btn3A;
    private javax.swing.JButton btn3B;
    private javax.swing.JButton btn4A;
    private javax.swing.JButton btn4B;
    private javax.swing.JButton btn5A;
    private javax.swing.JButton btn5B;
    private javax.swing.JButton btn6A;
    private javax.swing.JButton btn6B;
    private javax.swing.JButton btn7A;
    private javax.swing.JButton btn7B;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

}
