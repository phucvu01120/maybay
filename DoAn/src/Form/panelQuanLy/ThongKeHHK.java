/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Form.panelQuanLy;

/**
 *
 * @author PHUC
 */
public class ThongKeHHK {
    private String TenHang;
    private int tongve;
    private long tongtien;

    public ThongKeHHK() {
    }

    public ThongKeHHK(String TenHang, int tongve, long tongtien) {
        this.TenHang = TenHang;
        this.tongve = tongve;
        this.tongtien = tongtien;
    }

    public String getTenHang() {
        return TenHang;
    }

    public void setTenHang(String TenHang) {
        this.TenHang = TenHang;
    }

    public int getTongve() {
        return tongve;
    }

    public void setTongve(int tongve) {
        this.tongve = tongve;
    }

    public long getTongtien() {
        return tongtien;
    }

    public void setTongtien(int tongtien) {
        this.tongtien = tongtien;
    }
    
}
