/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Form.panelQuanLy;

import BLL.BVeMayBay;
import BLL.KhachHangDAO;
import BUS.CTHoaDonBUS;
import DTO.CTHoaDonDTO;
import DTO.KhachHangDTO;
import DTO.VeMayBay;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.collections4.comparators.ComparatorChain;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

/**
 *
 * @author PHUC
 */
public class panelThongKeVe extends javax.swing.JPanel {
    DecimalFormat currency = new DecimalFormat ("#,###,###,### VND");
    DefaultTableModel modelHHK;
    long tienVN;
    long tienVJ;
    long tienBL;
    long tienQH;

    /**
     * Creates new form panelThongKeVe
     */
    public panelThongKeVe() throws Exception {
        initComponents();
        modelHHK=(DefaultTableModel) tblHHK.getModel();
        addHHK();
        addLB();
    }
    public void addHHK() throws Exception{
        
        CTHoaDonBUS cthdBUS=new CTHoaDonBUS();
        cthdBUS.getListCTHD();
        int soveVN=0;
        int soveVJ=0;
        int soveBL=0;
        int soveQH=0;
        tienVN=0;
        tienVJ=0;
        tienBL=0;
        tienQH=0;
        for(CTHoaDonDTO cthd:cthdBUS.list_CTHD){
            String Hang=cthd.getMaCB().substring(0,2);
            if(Hang.equals("VN")){
                soveVN+=1;
                tienVN+=cthd.getGiaVe();
            }
            if(Hang.equals("VJ")){
                soveVJ+=1;
                tienVJ+=cthd.getGiaVe();
            }
            if(Hang.equals("BL")){
                soveBL+=1;
                tienBL+=cthd.getGiaVe();
            }
            if(Hang.equals("QH")){
                soveQH+=1;
                tienQH+=cthd.getGiaVe();
            }
        }
        ArrayList<ThongKeHHK> listtk=new ArrayList<>();
        listtk.add(new ThongKeHHK("VietnamAirlines", soveVN, tienVN));
        listtk.add(new ThongKeHHK("Jetstar Pacific Airlines", soveBL, tienBL));
        listtk.add(new ThongKeHHK("Vietjet Air", soveVJ, tienVJ));
        listtk.add(new ThongKeHHK("Bamboo Airways", soveQH, tienQH));
        Collections.sort(listtk, new Comparator<ThongKeHHK>(){
            @Override
            public int compare(ThongKeHHK t, ThongKeHHK t1) {
                if(t.getTongtien() > t1.getTongtien())
                    return -1;
                else{
                    if(t.getTongtien()==t1.getTongtien())
                        return 0;
                    else 
                        return 1;
                }
            }     
        });
        long tongdt=tienVN+tienVJ+tienBL+tienQH;
        modelHHK.addRow(new Object[]{
            1,listtk.get(0).getTenHang(),listtk.get(0).getTongve(),currency.format(listtk.get(0).getTongtien()),(float)listtk.get(0).getTongtien()*100/tongdt+"%"
        });
        modelHHK.addRow(new Object[]{
            2,listtk.get(1).getTenHang(),listtk.get(1).getTongve(),currency.format(listtk.get(1).getTongtien()),(float)listtk.get(1).getTongtien()*100/tongdt+"%"
        });
        modelHHK.addRow(new Object[]{
            3,listtk.get(2).getTenHang(),listtk.get(2).getTongve(),currency.format(listtk.get(2).getTongtien()),(float)listtk.get(2).getTongtien()*100/tongdt+"%"
        });
        modelHHK.addRow(new Object[]{
            4,listtk.get(3).getTenHang(),listtk.get(3).getTongve(),currency.format(listtk.get(3).getTongtien()),(float)listtk.get(3).getTongtien()*100/tongdt+"%"
        });
        
    }
    public long getTienVn(){
        return tienVN;
    }
    public long getTienVj(){
        return tienVJ;
    }
    public long getTienBl(){
        return tienBL;
    }
    public long getTienQh(){
        return tienQH;
    }
    public void addLB() throws Exception{
        BVeMayBay b=new BVeMayBay();
        ArrayList<VeMayBay> list=b.getVeMayBay();
        
        lbTongSoVe.setText("TỔNG SỐ VÉ : "+list.size());
        int dem=0;
        int embe=0;
        int hocsinh=0;
        int nguoilon=0;
        int nguoigia=0;
        int embenam=0;
        int hocsinhnam=0;
        int nguoilonnam=0;
        int nguoigianam=0;
        int embenu=0;
        int hocsinhnu=0;
        int nguoilonnu=0;
        int nguoigianu=0;
        for(VeMayBay vmb:list){
            if(vmb.getMaKH()==null)
                dem++;
            else{
                KhachHangDAO khDAO=new KhachHangDAO();
                KhachHangDTO kh=khDAO.getKhachHangByMaKH(vmb.getMaKH());
                float tuoi=tinhtuoi(kh.getNgaySinh());
                if(tuoi>0 && tuoi<=5){
                   embe++;
                   if(kh.getGioiTinh().equals("Nam")){
                       embenam++;
                   }
                   else{
                       embenu++;
                   }
                }
                if(tuoi>5 && tuoi<=18){
                    hocsinh++;
                    if(kh.getGioiTinh().equals("Nam")){
                       hocsinhnam++;
                   }
                   else{
                       hocsinhnu++;
                   }
                }
                if(tuoi>18 && tuoi<=59){
                    nguoilon++;
                    if(kh.getGioiTinh().equals("Nam")){
                       nguoilonnam++;
                   }
                   else{
                       nguoilonnu++;
                   }
                }
                if(tuoi>59){
                    nguoigia++;
                    if(kh.getGioiTinh().equals("Nam")){
                       nguoigianam++;
                   }
                   else{
                       nguoigianu++;
                   }
                }
            }
        }
        lbTongSoVeConTrong.setText(" Tồng số vé còn trống : "+dem);
        lbTongSoVeBanDuoc.setText(" Tổng số vé bán được : "+(list.size()-dem));
        lbEmBe.setText("  EM BÉ (0-5 tuổi) : "+embe);
        lbHocSinh.setText("  HỌC SINH (5-18 tuổi) : "+hocsinh);
        lbNguoiLon.setText("  NGƯỜI TRƯỞNG THÀNH (18-59 tuổi) : "+nguoilon);
        lbNguoiGia.setText("  NGƯỜI LỚN TUỔI (>59 tuổi) : "+nguoigia);
        lbEmBeNam.setText("  Nam : "+embenam);
        lbEmBeNu.setText("  Nữ : "+embenu);
        lbHSNam.setText("  Nam : "+hocsinhnam);
        lbHSNu.setText("  Nữ : "+hocsinhnu);
        lbNLNam.setText("  Nam : "+nguoilonnam);
        lbNLNu.setText("  Nữ : "+nguoilonnu);
        lbNGNam.setText("  Nam : "+nguoigianam);
        lbNGNu.setText("  Nữ : "+nguoigianu);
        
        
        
        
    }
    public float tinhtuoi(String ngay) throws ParseException{
        Date HomNay=new Date();
        java.util.Date NgaySinh =  new SimpleDateFormat("yyyy-MM-dd").parse(ngay);
        Calendar c1=Calendar.getInstance();
        Calendar c2=Calendar.getInstance();
        c1.setTime(HomNay);
        c2.setTime(NgaySinh);
        float a=(c1.getTime().getTime()-c2.getTime().getTime())/(24*3600*1000);
        float tuoi=a/365;
        return tuoi;
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblHHK = new rojerusan.RSTableMetro();
        lbTongSoVe = new javax.swing.JLabel();
        lbTongSoVeBanDuoc = new javax.swing.JLabel();
        lbTongSoVeConTrong = new javax.swing.JLabel();
        lbEmBe = new javax.swing.JLabel();
        lbNguoiLon = new javax.swing.JLabel();
        lbHocSinh = new javax.swing.JLabel();
        lbNguoiGia = new javax.swing.JLabel();
        lbHSNam = new javax.swing.JLabel();
        lbEmBeNu = new javax.swing.JLabel();
        lbEmBeNam = new javax.swing.JLabel();
        lbHSNu = new javax.swing.JLabel();
        lbNLNam = new javax.swing.JLabel();
        lbNLNu = new javax.swing.JLabel();
        lbNGNam = new javax.swing.JLabel();
        lbNGNu = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Calibri Light", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(102, 0, 0));
        jLabel1.setText("THEO HÃNG HÀNG KHÔNG");

        tblHHK.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Hạng", "Tên Hãng", "Tổng vé", "Tổng tiền bán được", "% doanh thu"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblHHK.setColorBackgoundHead(new java.awt.Color(102, 0, 0));
        tblHHK.setColorFilasBackgound2(new java.awt.Color(255, 255, 255));
        tblHHK.setColorFilasForeground1(new java.awt.Color(102, 0, 0));
        tblHHK.setColorFilasForeground2(new java.awt.Color(102, 0, 0));
        tblHHK.setColorSelBackgound(new java.awt.Color(255, 255, 255));
        tblHHK.setColorSelForeground(new java.awt.Color(102, 0, 0));
        tblHHK.setFuenteFilas(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        tblHHK.setFuenteFilasSelect(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        tblHHK.setFuenteHead(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        tblHHK.setRowHeight(30);
        tblHHK.setSelectionBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setViewportView(tblHHK);
        if (tblHHK.getColumnModel().getColumnCount() > 0) {
            tblHHK.getColumnModel().getColumn(0).setPreferredWidth(0);
            tblHHK.getColumnModel().getColumn(1).setPreferredWidth(250);
            tblHHK.getColumnModel().getColumn(2).setPreferredWidth(20);
            tblHHK.getColumnModel().getColumn(3).setPreferredWidth(150);
        }

        lbTongSoVe.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        lbTongSoVe.setForeground(new java.awt.Color(102, 0, 0));
        lbTongSoVe.setText("TỔNG SỐ VÉ :");

        lbTongSoVeBanDuoc.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        lbTongSoVeBanDuoc.setForeground(new java.awt.Color(102, 0, 0));
        lbTongSoVeBanDuoc.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbTongSoVeBanDuoc.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\icons8-new-ticket-64.png")); // NOI18N
        lbTongSoVeBanDuoc.setText(" Tổng số vé bán được :");

        lbTongSoVeConTrong.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        lbTongSoVeConTrong.setForeground(new java.awt.Color(102, 0, 0));
        lbTongSoVeConTrong.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\icons8-delete-ticket-64.png")); // NOI18N
        lbTongSoVeConTrong.setText("Tổng số vé còn trống :");

        lbEmBe.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        lbEmBe.setForeground(new java.awt.Color(102, 0, 0));
        lbEmBe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbEmBe.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\BABY.png")); // NOI18N
        lbEmBe.setText("  EM BÉ (0-5 tuổi) : ");

        lbNguoiLon.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        lbNguoiLon.setForeground(new java.awt.Color(102, 0, 0));
        lbNguoiLon.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbNguoiLon.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\TRUONGTHANH.png")); // NOI18N
        lbNguoiLon.setText("  NGƯỜI TRƯỞNG THÀNH (18-59 tuổi) : ");

        lbHocSinh.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        lbHocSinh.setForeground(new java.awt.Color(102, 0, 0));
        lbHocSinh.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbHocSinh.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\STUDENT.png")); // NOI18N
        lbHocSinh.setText("  HỌC SINH (5-18 tuổi) : ");

        lbNguoiGia.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        lbNguoiGia.setForeground(new java.awt.Color(102, 0, 0));
        lbNguoiGia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbNguoiGia.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\LONTUOI.png")); // NOI18N
        lbNguoiGia.setText("  NGƯỜI LỚN TUỔI (>59 tuổi) : ");

        lbHSNam.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        lbHSNam.setForeground(new java.awt.Color(102, 0, 0));
        lbHSNam.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbHSNam.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\icons8-student-male-64.png")); // NOI18N
        lbHSNam.setText("  NAM :");

        lbEmBeNu.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        lbEmBeNu.setForeground(new java.awt.Color(102, 0, 0));
        lbEmBeNu.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbEmBeNu.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\babyboy.jpg")); // NOI18N
        lbEmBeNu.setText("  NỮ :");

        lbEmBeNam.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        lbEmBeNam.setForeground(new java.awt.Color(102, 0, 0));
        lbEmBeNam.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbEmBeNam.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\babyboy.jpg")); // NOI18N
        lbEmBeNam.setText("  NAM :");

        lbHSNu.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        lbHSNu.setForeground(new java.awt.Color(102, 0, 0));
        lbHSNu.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbHSNu.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\icons8-graduate-64.png")); // NOI18N
        lbHSNu.setText("  NỮ :");

        lbNLNam.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        lbNLNam.setForeground(new java.awt.Color(102, 0, 0));
        lbNLNam.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbNLNam.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\icons8-user-male-skin-type-4-64.png")); // NOI18N
        lbNLNam.setText("  NAM :");

        lbNLNu.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        lbNLNu.setForeground(new java.awt.Color(102, 0, 0));
        lbNLNu.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbNLNu.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\icons8-user-female-skin-type-4-64.png")); // NOI18N
        lbNLNu.setText("  NỮ :");

        lbNGNam.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        lbNGNam.setForeground(new java.awt.Color(102, 0, 0));
        lbNGNam.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbNGNam.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\icons8-grandpa-64.png")); // NOI18N
        lbNGNam.setText("  NAM :");

        lbNGNu.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        lbNGNu.setForeground(new java.awt.Color(102, 0, 0));
        lbNGNu.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbNGNu.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\icons8-grandmother-64.png")); // NOI18N
        lbNGNu.setText("  NỮ :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 835, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbTongSoVe, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                        .addComponent(lbTongSoVeBanDuoc, javax.swing.GroupLayout.PREFERRED_SIZE, 472, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(45, 45, 45)
                        .addComponent(lbTongSoVeConTrong, javax.swing.GroupLayout.PREFERRED_SIZE, 449, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lbNguoiLon, javax.swing.GroupLayout.PREFERRED_SIZE, 738, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lbHocSinh, javax.swing.GroupLayout.PREFERRED_SIZE, 530, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(47, 47, 47))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(lbEmBe, javax.swing.GroupLayout.PREFERRED_SIZE, 468, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(317, 317, 317)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lbNguoiGia, javax.swing.GroupLayout.PREFERRED_SIZE, 633, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(152, 152, 152)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lbNGNam, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lbNGNu, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(lbEmBeNam, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lbEmBeNu, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(lbHSNam, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lbHSNu, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(lbNLNam, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(43, 43, 43)
                                    .addComponent(lbNLNu, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbTongSoVe, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbTongSoVeBanDuoc)
                    .addComponent(lbTongSoVeConTrong, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbEmBe, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbEmBeNu, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbEmBeNam, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbHocSinh, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbHSNam, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbHSNu, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbNguoiLon, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbNLNam, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbNLNu, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbNguoiGia, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbNGNam, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbNGNu, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(129, 129, 129))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbEmBe;
    private javax.swing.JLabel lbEmBeNam;
    private javax.swing.JLabel lbEmBeNu;
    private javax.swing.JLabel lbHSNam;
    private javax.swing.JLabel lbHSNu;
    private javax.swing.JLabel lbHocSinh;
    private javax.swing.JLabel lbNGNam;
    private javax.swing.JLabel lbNGNu;
    private javax.swing.JLabel lbNLNam;
    private javax.swing.JLabel lbNLNu;
    private javax.swing.JLabel lbNguoiGia;
    private javax.swing.JLabel lbNguoiLon;
    private javax.swing.JLabel lbTongSoVe;
    private javax.swing.JLabel lbTongSoVeBanDuoc;
    private javax.swing.JLabel lbTongSoVeConTrong;
    private rojerusan.RSTableMetro tblHHK;
    // End of variables declaration//GEN-END:variables
}
