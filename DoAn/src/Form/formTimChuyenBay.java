/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Form;

import BLL.BChuyenBay;
import BLL.BHangHangKhong;
import BLL.BSanBay;
import BLL.BTuyenBay;
import DTO.ChuyenBay;
import DTO.HangHangKhong;
import DTO.NhanVienDTO;
import DTO.SanBay;
import DTO.TuyenBay;
import java.awt.Color;
import java.awt.Image;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author PHUC
 */
public class formTimChuyenBay extends javax.swing.JFrame {

    BHangHangKhong BHHK;
    ArrayList<HangHangKhong> list_hhk;
    BTuyenBay bTB;
    ArrayList<TuyenBay> list_tb;
    BSanBay bSB;
    ArrayList<SanBay> list_sb;
    BChuyenBay bCB=new BChuyenBay();
    ArrayList<ChuyenBay> list_cb;
    DefaultTableModel modelCB;
    int SoLuongKhach;
    String LoaiVe;
    private static NhanVienDTO nv;
    
    /**
     *
     * @param nv
     * @throws Exception
     */
    public formTimChuyenBay(NhanVienDTO nv) throws Exception {
        initComponents();
        this.setLocationRelativeTo(null);
        BHHK=new BHangHangKhong();
        list_hhk=BHHK.getHangHangKhongs();
        bTB=new BTuyenBay();
        list_tb=bTB.getTuyenBays();
        bSB=new BSanBay();
        list_sb=bSB.getSanBays();
        bCB=new BChuyenBay();
        list_cb=new ArrayList<>();
        addCBHB();
        addCBTB();
        modelCB=(DefaultTableModel) tblChuyenBay.getModel();
        cbDiemDi.setSelectedIndex(0);
        cbDiemDen.setSelectedIndex(1);
        this.nv=nv;
        lbTenNV.setText(nv.getMaNV()+" - "+nv.getHoNV()+" "+nv.getTenNV());
    }
    public boolean checkTB(ChuyenBay cb) throws Exception{
        String sbcc=(String) cbDiemDi.getSelectedItem();
        String masbcc=sbcc.substring(0,3);
        String sbhc=(String) cbDiemDen.getSelectedItem();
        String masbhc=sbhc.substring(0,3);
        TuyenBay tb=bTB.getByID(cb.getMaTuyenBay());
            if(tb.getMaSBCatCanh().equals(masbcc) && tb.getMaSBHaCanh().equals(masbhc))
                return true;
        
        return false;
    }
    public boolean checkTime(String ngaybay){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        String ngay=sdf.format(jdcNgayDi.getDate());
        if(ngay.equals(ngaybay)){
            return true;
        }
        return false;
    }
    public boolean checkSove(ChuyenBay cb){
        int SoLuongChon=(int) jspSoLuongKhach.getValue();
        if(cbHangVe.getSelectedItem().equals("Phổ thông")){
            if(cb.getSLGhePhoThong_CL()<SoLuongChon){
                return false;
            }
        }
        if(cbHangVe.getSelectedItem().equals("Thương gia")){
            if(cb.getSLGheThuongGia_CL()<SoLuongChon){
                return false;
            }
        }
        return true;
    }
    public boolean checkHHK(ChuyenBay cb){
        if(cbHangHangKhong.getSelectedItem().equals("Tất cả")){
            return true;
        }
        String hang=(String) cbHangHangKhong.getSelectedItem();
        String mahang=hang.substring(0,2);
        if(mahang.equals(cb.getMaHang())){
            return true;
        }
        return false;
    }
    public ArrayList<ChuyenBay> loadCBbySearch() throws Exception{
        ArrayList<ChuyenBay> all_cb=bCB.getChuyenBays();
        ArrayList<ChuyenBay> cb_search=new ArrayList<>();
            for(ChuyenBay cb:all_cb){
              if(!cb.getTrangThai().equals("Đã bị hủy")){
                String ngaybay=cb.getNgayBay().substring(0,10);
                String tt=bCB.capnhatTrangThai(ngaybay, cb.getMaMB(), cb.getSLGheThuongGia_CL()+cb.getSLGhePhoThong_CL());
                    cb.setTrangThai(tt);
                    bCB.UpdateTrangThai(cb);
                    if((cb.getTrangThai().equals("Bình Thường") || cb.getTrangThai().equals("Báo động")) && checkTB(cb)==true && checkTime(cb.getNgayBay().substring(0,10))==true 
                            && checkSove(cb)==true && checkHHK(cb)==true){
                        cb_search.add(cb);
                    }
            }
            }
            return cb_search;
    }
    public void addCBHB(){
        for(HangHangKhong hhk : list_hhk){
            cbHangHangKhong.addItem(hhk.getMaHang()+" - "+hhk.getTenHang());
        }
    }
    public void addCBTB(){
        for(int i=1;i<list_sb.size();i++){
            cbDiemDi.addItem(list_sb.get(i).getMaSanBay()+" - "+list_sb.get(i).getThanhPho());
        }
        for(int i=1;i<list_sb.size();i++){
            cbDiemDen.addItem(list_sb.get(i).getMaSanBay()+" - "+list_sb.get(i).getThanhPho());
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel10 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblChuyenBay = new rojerusan.RSTableMetro();
        jPanel1 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        cbDiemDen = new javax.swing.JComboBox<>();
        jLabel38 = new javax.swing.JLabel();
        jdcNgayDi = new com.toedter.calendar.JDateChooser();
        jLabel33 = new javax.swing.JLabel();
        cbDiemDi = new javax.swing.JComboBox<>();
        jButton7 = new javax.swing.JButton();
        cbHangVe = new javax.swing.JComboBox<>();
        jLabel35 = new javax.swing.JLabel();
        btnTimKiem = new javax.swing.JButton();
        jLabel39 = new javax.swing.JLabel();
        cbHangHangKhong = new javax.swing.JComboBox<>();
        jspSoLuongKhach = new javax.swing.JSpinner();
        jPanel2 = new javax.swing.JPanel();
        jLabel34 = new javax.swing.JLabel();
        lbTenNV = new javax.swing.JLabel();
        btnReturn = new javax.swing.JButton();
        pnlCTCB = new javax.swing.JPanel();
        lbLoGo = new javax.swing.JLabel();
        lbHHK = new javax.swing.JLabel();
        lbTuyenBay = new javax.swing.JLabel();
        lbNgayBay = new javax.swing.JLabel();
        lbSoghe = new javax.swing.JLabel();
        lbGiaVe = new javax.swing.JLabel();
        btnChonChuyenBay = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));

        tblChuyenBay.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Hãng", "Mã Chuyến", "Điểm đi", "Điểm đến", "Ngày bay", "Giờ bay", "Giá vé"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblChuyenBay.setColorBackgoundHead(new java.awt.Color(0, 102, 102));
        tblChuyenBay.setColorFilasForeground1(new java.awt.Color(0, 102, 102));
        tblChuyenBay.setColorFilasForeground2(new java.awt.Color(0, 102, 102));
        tblChuyenBay.setColorSelBackgound(new java.awt.Color(0, 102, 102));
        tblChuyenBay.setFuenteFilas(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        tblChuyenBay.setFuenteFilasSelect(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        tblChuyenBay.setFuenteHead(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        tblChuyenBay.setPreferredScrollableViewportSize(new java.awt.Dimension(150, 400));
        tblChuyenBay.setRowHeight(30);
        tblChuyenBay.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblChuyenBayMouseClicked(evt);
            }
        });
        tblChuyenBay.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                tblChuyenBayInputMethodTextChanged(evt);
            }
        });
        jScrollPane7.setViewportView(tblChuyenBay);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createMatteBorder(4, 4, 4, 4, new java.awt.Color(0, 102, 102)));

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel31.setText("Điểm đi:");

        jLabel36.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel36.setText("Điểm đến:");

        cbDiemDen.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        cbDiemDen.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbDiemDenItemStateChanged(evt);
            }
        });
        cbDiemDen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbDiemDenjComboBox1ActionPerformed(evt);
            }
        });

        jLabel38.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel38.setText("Chọn ngày đi:");

        jdcNgayDi.setDateFormatString("yyyy-MM-dd");
        jdcNgayDi.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jLabel33.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel33.setText("Chọn hạng vé:");

        cbDiemDi.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        cbDiemDi.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbDiemDiItemStateChanged(evt);
            }
        });
        cbDiemDi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbDiemDijComboBox1ActionPerformed(evt);
            }
        });

        jButton7.setBackground(new java.awt.Color(255, 255, 255));
        jButton7.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\icons8-repeat-26.png")); // NOI18N
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        cbHangVe.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        cbHangVe.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Phổ thông", "Thương gia" }));
        cbHangVe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbHangVejComboBox1ActionPerformed(evt);
            }
        });

        jLabel35.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel35.setText("Nhập số lượng vé:");

        btnTimKiem.setBackground(new java.awt.Color(0, 102, 102));
        btnTimKiem.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnTimKiem.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\icons8-search-40.png")); // NOI18N
        btnTimKiem.setText("Tìm chuyến bay");
        btnTimKiem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTimKiemActionPerformed(evt);
            }
        });

        jLabel39.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel39.setText("Chọn hãng:");

        cbHangHangKhong.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        cbHangHangKhong.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả" }));
        cbHangHangKhong.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbHangHangKhongjComboBox1ActionPerformed(evt);
            }
        });

        jspSoLuongKhach.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jspSoLuongKhach.setModel(new javax.swing.SpinnerNumberModel(1, 1, 100, 1));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel33)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbHangVe, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel31)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbDiemDi, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel36)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbDiemDen, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel39)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbHangHangKhong, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel38)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jdcNgayDi, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel35)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jspSoLuongKhach, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(37, 37, 37))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel31)
                            .addComponent(cbDiemDi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton7)
                            .addComponent(jdcNgayDi, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel36)
                                .addComponent(cbDiemDen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel38)))))
                .addGap(43, 43, 43)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel33)
                    .addComponent(cbHangVe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel39)
                    .addComponent(cbHangHangKhong, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jspSoLuongKhach, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35))
                .addGap(18, 18, 18)
                .addComponent(btnTimKiem)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(0, 102, 102));

        jLabel34.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel34.setForeground(new java.awt.Color(255, 255, 255));
        jLabel34.setText("TRA CỨU CHUYẾN BAY");

        lbTenNV.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        lbTenNV.setForeground(new java.awt.Color(255, 255, 255));
        lbTenNV.setText("Nhân viên:");

        btnReturn.setBackground(new java.awt.Color(255, 255, 255));
        btnReturn.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\icons8-return-100.png")); // NOI18N
        btnReturn.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(255, 255, 255)));
        btnReturn.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnReturn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReturnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnReturn, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel34)
                .addGap(83, 83, 83)
                .addComponent(lbTenNV, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel34)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnReturn, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 53, Short.MAX_VALUE)
                        .addComponent(lbTenNV)))
                .addContainerGap())
        );

        pnlCTCB.setBackground(new java.awt.Color(255, 255, 255));
        pnlCTCB.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 102, 102)));

        lbLoGo.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        lbHHK.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbHHK.setForeground(new java.awt.Color(51, 51, 51));

        lbTuyenBay.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbTuyenBay.setForeground(new java.awt.Color(51, 51, 51));

        lbNgayBay.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbNgayBay.setForeground(new java.awt.Color(51, 51, 51));

        lbSoghe.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbSoghe.setForeground(new java.awt.Color(51, 51, 51));

        lbGiaVe.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbGiaVe.setForeground(new java.awt.Color(51, 51, 51));

        javax.swing.GroupLayout pnlCTCBLayout = new javax.swing.GroupLayout(pnlCTCB);
        pnlCTCB.setLayout(pnlCTCBLayout);
        pnlCTCBLayout.setHorizontalGroup(
            pnlCTCBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCTCBLayout.createSequentialGroup()
                .addGroup(pnlCTCBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlCTCBLayout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(pnlCTCBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbNgayBay, javax.swing.GroupLayout.PREFERRED_SIZE, 978, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbSoghe, javax.swing.GroupLayout.PREFERRED_SIZE, 829, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbGiaVe, javax.swing.GroupLayout.PREFERRED_SIZE, 829, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbTuyenBay, javax.swing.GroupLayout.PREFERRED_SIZE, 978, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlCTCBLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lbLoGo, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lbHHK, javax.swing.GroupLayout.PREFERRED_SIZE, 812, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 26, Short.MAX_VALUE))
        );
        pnlCTCBLayout.setVerticalGroup(
            pnlCTCBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCTCBLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCTCBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbLoGo, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbHHK, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbTuyenBay, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbNgayBay, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbSoghe, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbGiaVe, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btnChonChuyenBay.setBackground(new java.awt.Color(0, 102, 0));
        btnChonChuyenBay.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnChonChuyenBay.setForeground(new java.awt.Color(255, 255, 255));
        btnChonChuyenBay.setIcon(new javax.swing.ImageIcon("D:\\xinchao\\DoAn\\src\\image\\icons8-buy-48.png")); // NOI18N
        btnChonChuyenBay.setText("Chọn chuyến bay");
        btnChonChuyenBay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChonChuyenBayActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane7)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlCTCB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnChonChuyenBay, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlCTCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChonChuyenBay, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbDiemDenjComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbDiemDenjComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbDiemDenjComboBox1ActionPerformed

    private void cbDiemDijComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbDiemDijComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbDiemDijComboBox1ActionPerformed

    private void cbHangVejComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbHangVejComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbHangVejComboBox1ActionPerformed

    private void btnTimKiemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTimKiemActionPerformed
        try {
            // TODO add your handling code here:
            removeAllRow();
            list_cb=loadCBbySearch();
            for(ChuyenBay cb:list_cb){
                TuyenBay tb=bTB.getByID(cb.getMaTuyenBay());
                int giave = 0;
                if(cbHangVe.getSelectedItem().equals("Phổ thông")){
                    giave=cb.getGiaVePhoThong();
                }
                if(cbHangVe.getSelectedItem().equals("Thương gia")){
                    giave=cb.getGiaVeThuongGia();
                }
                SanBay sbcc=bSB.getSanBayByMaSB(tb.getMaSBCatCanh());
                SanBay sbhc=bSB.getSanBayByMaSB(tb.getMaSBHaCanh());
                
                modelCB.addRow(new Object[]{
                    cb.getMaHang(),cb.getMaCB(),sbcc.getThanhPho(),sbhc.getThanhPho(),cb.getNgayBay().substring(0, 10),cb.getNgayBay().substring(11, 16),giave
                });
                SoLuongKhach=(int) jspSoLuongKhach.getValue();
                LoaiVe=(String) cbHangVe.getSelectedItem();
            }
        } catch (Exception ex) {
            Logger.getLogger(formTimChuyenBay.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnTimKiemActionPerformed

    private void cbHangHangKhongjComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbHangHangKhongjComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbHangHangKhongjComboBox1ActionPerformed

    private void cbDiemDiItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbDiemDiItemStateChanged
        // TODO add your handling code here:
        if(cbDiemDi.getSelectedItem().equals(cbDiemDen.getSelectedItem())){
                cbDiemDen.setSelectedIndex(cbDiemDen.getSelectedIndex()+1);
       }
    }//GEN-LAST:event_cbDiemDiItemStateChanged

    private void cbDiemDenItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbDiemDenItemStateChanged
        // TODO add your handling code here:
        if(cbDiemDi.getSelectedItem().equals(cbDiemDen.getSelectedItem())){
                cbDiemDi.setSelectedIndex(cbDiemDi.getSelectedIndex()+1);                   
        }
    }//GEN-LAST:event_cbDiemDenItemStateChanged

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        String tem=(String) cbDiemDi.getSelectedItem();
        cbDiemDi.setSelectedItem(cbDiemDen.getSelectedItem());
        cbDiemDen.setSelectedItem(tem);
        
    }//GEN-LAST:event_jButton7ActionPerformed

    private void btnChonChuyenBayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChonChuyenBayActionPerformed
        // TODO add your handling code here:
        int i=tblChuyenBay.getSelectedRow();
        if(i>=0){
            try {
                ChuyenBay cb=list_cb.get(i);
                int soluong=SoLuongKhach;
                String loai=LoaiVe;
                this.setVisible(false);
                formNhapThongTinKhach f=new formNhapThongTinKhach(cb, soluong, loai, this.nv);
                f.setVisible(true);
                
            } catch (Exception ex) {
                Logger.getLogger(formTimChuyenBay.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnChonChuyenBayActionPerformed

    private void tblChuyenBayInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_tblChuyenBayInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_tblChuyenBayInputMethodTextChanged

    private void tblChuyenBayMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblChuyenBayMouseClicked
        // TODO add your handling code here:
        try {
            // TODO add your handling code here:
            
            pnlCTCB.setBorder(new MatteBorder(2, 2, 2, 2, new Color(0, 141, 141)));
            int i=tblChuyenBay.getSelectedRow();
            ChuyenBay cb=list_cb.get(i);
            HangHangKhong hhk=BHHK.getHangHangKhongByMaHang(cb.getMaHang());
            lbHHK.setText(hhk.getTenHang()+"     Mã chuyến bay: "+cb.getMaCB()+"    Máy bay: "+cb.getMaMB());
            lbLoGo.setIcon(sizeOfImage(hhk.getPathLoGo(), lbLoGo));
            TuyenBay tb=bTB.getByID(cb.getMaTuyenBay());
            SanBay sbcc=bSB.getSanBayByMaSB(tb.getMaSBCatCanh());
            SanBay sbhc=bSB.getSanBayByMaSB(tb.getMaSBHaCanh());
            if(tb.getMaSBTrungGian().equals("")){
                lbTuyenBay.setText(sbcc.getTenSanBay()+" - "+sbcc.getThanhPho()+"   --->   "+sbhc.getTenSanBay()+" - "+sbhc.getThanhPho());
                lbNgayBay.setText("Ngày bay: "+cb.getNgayBay().substring(0,16));
            }
            else{
                SanBay sbtg=bSB.getSanBayByMaSB(tb.getMaSBTrungGian());
                lbTuyenBay.setText(sbcc.getTenSanBay()+" - "+sbcc.getThanhPho()+"   --->   "+sbtg.getTenSanBay()+" - "+sbtg.getThanhPho()+"   --->   "+sbhc.getTenSanBay()+" - "+sbhc.getThanhPho());
                lbNgayBay.setText("Ngày bay: "+cb.getNgayBay().substring(0,16)+"   Ngày bay tt: "+cb.getNgayBayTT().substring(0, 16));
            }
           lbSoghe.setText("Số ghế thương gia còn trống: "+cb.getSLGheThuongGia_CL()+" _____ Số ghế phổ thông còn trống: "+cb.getSLGhePhoThong_CL());
           lbGiaVe.setText("Giá vé thương gia: "+cb.getGiaVeThuongGia()+" _____ Giá vé phổ thông: "+cb.getGiaVePhoThong());
        } catch (Exception ex) {
            Logger.getLogger(formTimChuyenBay.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_tblChuyenBayMouseClicked

    private void btnReturnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReturnActionPerformed
        // TODO add your handling code here:
        this.dispose();
        new formNHANVIEN(nv).setVisible(true);
    }//GEN-LAST:event_btnReturnActionPerformed

    public void removeAllRow(){
        int rowCount = modelCB.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            modelCB.removeRow(i);
        }
    }
    public ImageIcon sizeOfImage (String pathImage, JLabel lbl ){
        ImageIcon myimage=null;
        if(pathImage!=null){
            myimage=new ImageIcon(pathImage);
        }
        else{
            myimage=new ImageIcon("D:/xinchao/DoAn/src/logoHHK/NoImage.jpg");
        }
        
        Image img1 = myimage.getImage();
        Image img2 = img1.getScaledInstance(lbl.getWidth(), lbl.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon img=new ImageIcon(img2);
        return img;
    }
    /**
     * @param args the command line arguments
     */
    
 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnChonChuyenBay;
    private javax.swing.JButton btnReturn;
    private javax.swing.JButton btnTimKiem;
    private javax.swing.JComboBox<String> cbDiemDen;
    private javax.swing.JComboBox<String> cbDiemDi;
    private javax.swing.JComboBox<String> cbHangHangKhong;
    private javax.swing.JComboBox<String> cbHangVe;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane7;
    private com.toedter.calendar.JDateChooser jdcNgayDi;
    private javax.swing.JSpinner jspSoLuongKhach;
    private javax.swing.JLabel lbGiaVe;
    private javax.swing.JLabel lbHHK;
    private javax.swing.JLabel lbLoGo;
    private javax.swing.JLabel lbNgayBay;
    private javax.swing.JLabel lbSoghe;
    private javax.swing.JLabel lbTenNV;
    private javax.swing.JLabel lbTuyenBay;
    private javax.swing.JPanel pnlCTCB;
    private rojerusan.RSTableMetro tblChuyenBay;
    // End of variables declaration//GEN-END:variables
}
