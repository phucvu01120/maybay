-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 25, 2020 at 12:24 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vemaybay`
--

-- --------------------------------------------------------

--
-- Table structure for table `chitiethoadon`
--

CREATE TABLE `chitiethoadon` (
  `MaHD` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MaCB` varchar(10) CHARACTER SET utf8 NOT NULL,
  `GheNgoi` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MaHanhLy` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GiaVe` int(10) NOT NULL,
  `GiaHanhLi` int(10) NOT NULL,
  `ThanhTien` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chitiethoadon`
--

INSERT INTO `chitiethoadon` (`MaHD`, `MaCB`, `GheNgoi`, `MaHanhLy`, `GiaVe`, `GiaHanhLi`, `ThanhTien`) VALUES
('HD1', 'VN5868', '10A', 'HL020', 40000000, 500000, 40500000),
('HD2', 'VN5868', '12B', 'HL050', 40000000, 4000000, 44000000),
('HD2', 'VN5868', '12A', 'HL050', 40000000, 4000000, 44000000),
('HD3', 'VJ8589', '1A', 'HL007', 35000000, 0, 35000000),
('HD4', 'VJ8589', '5A', 'HL007', 18000000, 0, 18000000),
('HD5', 'QH6706', '6A', 'HL007', 16000000, 0, 16000000),
('HD6', 'QH6706', '4B', 'HL050', 16000000, 4000000, 20000000),
('HD7', 'VJ8589', '7A', 'HL050', 18000000, 4000000, 22000000),
('HD8', 'VN5868', '9B', 'HL050', 40000000, 4000000, 44000000),
('HD9', 'BL2925', '4A', 'HL007', 15000000, 0, 15000000),
('HD10', 'BL2925', '6A', 'HL007', 15000000, 0, 15000000),
('HD11', 'VJ8589', '1B', 'HL007', 35000000, 0, 35000000),
('HD12', 'VN5868', '11B', 'HL050', 40000000, 4000000, 44000000),
('HD13', 'VN5868', '2A', 'HL007', 22000000, 0, 22000000),
('HD13', 'VN5868', '2B', 'HL050', 22000000, 4000000, 26000000),
('HD13', 'VN5868', '3B', 'HL050', 22000000, 4000000, 26000000),
('HD13', 'VN5868', '3A', 'HL050', 22000000, 4000000, 26000000),
('HD14', 'BL2925', '1A', 'HL050', 25000000, 4000000, 29000000),
('HD15', 'QH6706', '16A', 'HL007', 16000000, 0, 16000000),
('HD16', 'BL2925', '3A', 'HL050', 25000000, 4000000, 29000000),
('HD16', 'BL2925', '3B', 'HL050', 25000000, 4000000, 29000000),
('HD16', 'BL2925', '2B', 'HL050', 25000000, 4000000, 29000000),
('HD16', 'BL2925', '1B', 'HL050', 25000000, 4000000, 29000000),
('HD17', 'VN5868', '9A', 'HL050', 40000000, 4000000, 44000000),
('HD18', 'BL2925', '2A', 'HL050', 25000000, 4000000, 29000000),
('HD19', 'BL2925', '7B', 'HL050', 15000000, 4000000, 19000000),
('HD20', 'BL2925', '6B', 'HL007', 15000000, 0, 15000000),
('HD21', 'BL2925', '7A', 'HL050', 15000000, 4000000, 19000000),
('HD22', 'VN5868', '11A', 'HL050', 40000000, 4000000, 44000000),
('HD22', 'VN5868', '10B', 'HL050', 40000000, 4000000, 44000000),
('HD23', 'VJ8589', '2A', 'HL050', 35000000, 4000000, 39000000),
('HD24', 'BL2925', '5A', 'HL007', 15000000, 0, 15000000),
('HD25', 'QH6706', '1A', 'HL050', 16000000, 4000000, 20000000),
('HD25', 'QH6706', '3A', 'HL050', 16000000, 4000000, 20000000),
('HD25', 'QH6706', '2A', 'HL050', 16000000, 4000000, 20000000),
('HD26', 'QH6706', '7A', 'HL050', 16000000, 4000000, 20000000),
('HD36', 'VJ3703', '5A', 'HL007', 75000000, 0, 75000000),
('HD36', 'VJ3703', '5B', 'HL050', 75000000, 4000000, 79000000),
('HD36', 'VJ3703', '6A', 'HL050', 75000000, 4000000, 79000000),
('HD36', 'VJ3703', '4A', 'HL007', 75000000, 0, 75000000),
('HD36', 'VJ3703', '4B', 'HL050', 75000000, 4000000, 79000000),
('HD37', 'VJ3703', '10A', 'HL050', 110000000, 4000000, 114000000),
('HD37', 'VJ3703', '10B', 'HL050', 110000000, 4000000, 114000000),
('HD37', 'VJ3703', '9A', 'HL050', 110000000, 4000000, 114000000),
('HD27', 'VJ3703', '8A', 'HL007', 75000000, 0, 75000000),
('HD27', 'VJ3703', '8B', 'HL050', 75000000, 4000000, 79000000),
('HD27', 'VJ3703', '7A', 'HL007', 75000000, 0, 75000000),
('HD28', 'VJ3703', '11A', 'HL050', 110000000, 4000000, 114000000),
('HD28', 'VJ3703', '11B', 'HL050', 110000000, 4000000, 114000000),
('HD29', 'VJ3703', '1A', 'HL050', 75000000, 4000000, 79000000),
('HD29', 'VJ3703', '1B', 'HL050', 75000000, 4000000, 79000000),
('HD29', 'VJ3703', '2A', 'HL050', 75000000, 4000000, 79000000),
('HD29', 'VJ3703', '2B', 'HL007', 75000000, 0, 75000000),
('HD29', 'VJ3703', '3B', 'HL050', 75000000, 4000000, 79000000),
('HD29', 'VJ3703', '3A', 'HL007', 75000000, 0, 75000000),
('HD30', 'VJ3703', '7B', 'HL007', 75000000, 0, 75000000),
('HD30', 'VJ3703', '6B', 'HL050', 75000000, 4000000, 79000000),
('HD31', 'VJ4882', '9A', 'HL020', 120000000, 500000, 120500000),
('HD31', 'VJ4882', '9B', 'HL020', 120000000, 500000, 120500000),
('HD31', 'VJ4882', '10A', 'HL050', 120000000, 4000000, 124000000),
('HD31', 'VJ4882', '10B', 'HL007', 120000000, 0, 120000000),
('HD31', 'VJ4882', '11A', 'HL020', 120000000, 500000, 120500000),
('HD31', 'VJ4882', '11B', 'HL050', 120000000, 4000000, 124000000),
('HD31', 'VJ4882', '12A', 'HL050', 120000000, 4000000, 124000000),
('HD32', 'VJ4882', '12B', 'HL050', 120000000, 4000000, 124000000),
('HD33', 'VJ4882', '1A', 'HL020', 70000000, 500000, 70500000),
('HD33', 'VJ4882', '1B', 'HL050', 70000000, 4000000, 74000000),
('HD33', 'VJ4882', '2A', 'HL050', 70000000, 4000000, 74000000),
('HD33', 'VJ4882', '2B', 'HL007', 70000000, 0, 70000000),
('HD33', 'VJ4882', '3A', 'HL007', 70000000, 0, 70000000),
('HD33', 'VJ4882', '3B', 'HL020', 70000000, 500000, 70500000),
('HD33', 'VJ4882', '4A', 'HL020', 70000000, 500000, 70500000),
('HD33', 'VJ4882', '4B', 'HL007', 70000000, 0, 70000000),
('HD33', 'VJ4882', '5A', 'HL050', 70000000, 4000000, 74000000),
('HD33', 'VJ4882', '5B', 'HL020', 70000000, 500000, 70500000),
('HD34', 'VJ4882', '14A', 'HL020', 70000000, 500000, 70500000),
('HD34', 'VJ4882', '14B', 'HL007', 70000000, 0, 70000000),
('HD35', 'VJ4882', '16A', 'HL007', 70000000, 0, 70000000),
('HD35', 'VJ4882', '15A', 'HL020', 70000000, 500000, 70500000),
('HD35', 'VJ4882', '13A', 'HL007', 70000000, 0, 70000000),
('HD35', 'VJ4882', '13B', 'HL007', 70000000, 0, 70000000),
('HD38', 'BL4894', '1A', 'HL050', 40000000, 4000000, 44000000),
('HD38', 'BL4894', '2A', 'HL050', 40000000, 4000000, 44000000),
('HD38', 'BL4894', '3A', 'HL050', 40000000, 4000000, 44000000),
('HD38', 'BL4894', '1B', 'HL050', 40000000, 4000000, 44000000),
('HD38', 'BL4894', '2B', 'HL050', 40000000, 4000000, 44000000),
('HD39', 'BL4894', '4A', 'HL050', 25000000, 4000000, 29000000),
('HD39', 'BL4894', '4B', 'HL050', 25000000, 4000000, 29000000),
('HD40', 'BL4894', '5A', 'HL050', 25000000, 4000000, 29000000),
('HD41', 'QH1655', '1A', 'HL050', 20000000, 4000000, 24000000),
('HD42', 'VJ3863', '4A', 'HL050', 25000000, 4000000, 29000000),
('HD43', 'VN4541', '4A', 'HL050', 35000000, 4000000, 39000000),
('HD44', 'VN4541', '1A', 'HL007', 60000000, 0, 60000000),
('HD45', 'BL4894', '3B', 'HL050', 40000000, 4000000, 44000000),
('HD46', 'VJ3863', '9A', 'HL050', 50000000, 4000000, 54000000),
('HD47', 'BL7492', '4A', 'HL007', 90000000, 0, 90000000),
('HD48', 'BL7492', '1A', 'HL050', 150000000, 4000000, 154000000),
('HD49', 'QH8496', '4A', 'HL007', 60000000, 0, 60000000),
('HD50', 'VN7632', '10A', 'HL050', 170000000, 4000000, 174000000),
('HD50', 'VN7632', '9B', 'HL050', 170000000, 4000000, 174000000),
('HD50', 'VN7632', '9A', 'HL050', 170000000, 4000000, 174000000),
('HD50', 'VN7632', '10B', 'HL050', 170000000, 4000000, 174000000),
('HD51', 'VN7632', '11A', 'HL007', 170000000, 0, 170000000),
('HD51', 'VN7632', '11B', 'HL050', 170000000, 4000000, 174000000),
('HD51', 'VN7632', '12A', 'HL050', 170000000, 4000000, 174000000),
('HD51', 'VN7632', '12B', 'HL050', 170000000, 4000000, 174000000),
('HD52', 'BL7492', '2A', 'HL007', 150000000, 0, 150000000),
('HD52', 'BL7492', '1B', 'HL050', 150000000, 4000000, 154000000),
('HD52', 'BL7492', '3A', 'HL050', 150000000, 4000000, 154000000),
('HD52', 'BL7492', '2B', 'HL007', 150000000, 0, 150000000),
('HD52', 'BL7492', '3B', 'HL050', 150000000, 4000000, 154000000),
('HD53', 'QH8496', '1A', 'HL050', 100000000, 4000000, 104000000),
('HD53', 'QH8496', '2A', 'HL007', 100000000, 0, 100000000),
('HD53', 'QH8496', '3A', 'HL007', 100000000, 0, 100000000),
('HD53', 'QH8496', '1B', 'HL007', 100000000, 0, 100000000),
('HD53', 'QH8496', '2B', 'HL050', 100000000, 4000000, 104000000),
('HD53', 'QH8496', '3B', 'HL050', 100000000, 4000000, 104000000),
('HD54', 'QH8496', '5A', 'HL007', 60000000, 0, 60000000),
('HD54', 'QH8496', '4B', 'HL007', 60000000, 0, 60000000),
('HD54', 'QH8496', '6A', 'HL050', 60000000, 4000000, 64000000),
('HD55', 'BL7492', '5A', 'HL007', 90000000, 0, 90000000),
('HD56', 'BL9673', '5A', 'HL050', 80000000, 4000000, 84000000),
('HD57', 'VN7642', '10A', 'HL050', 150000000, 4000000, 154000000),
('HD58', 'BL9673', '1A', 'HL050', 80000000, 4000000, 84000000);

-- --------------------------------------------------------

--
-- Table structure for table `chuongtrinhkm`
--

CREATE TABLE `chuongtrinhkm` (
  `MaCTKM` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TenCTKM` varchar(90) CHARACTER SET utf8 NOT NULL,
  `NgayBatDau` date NOT NULL,
  `NgayKetThuc` date NOT NULL,
  `PhanTramKM` int(10) NOT NULL,
  `SLCode` int(3) NOT NULL,
  `TinhTrang` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chuongtrinhkm`
--

INSERT INTO `chuongtrinhkm` (`MaCTKM`, `TenCTKM`, `NgayBatDau`, `NgayKetThuc`, `PhanTramKM`, `SLCode`, `TinhTrang`) VALUES
('KM001', 'Chúc mừng Xuân 2020', '2020-01-25', '2020-02-25', 20, 9, 'Đã kết thúc'),
('KM002', 'Chúc mừng quốc tế phụ nữ 8/3', '2020-03-01', '2020-03-30', 10, 5, 'Đã kết thúc'),
('KM003', 'Chúc mừng sinh nhật VietNam Ariline', '2020-06-23', '2020-07-30', 30, 20, 'Đang diễn ra'),
('KM004', 'Chúc mừng sinh nhật VietJes', '2020-06-24', '2020-06-30', 20, 20, 'Đang diễn ra'),
('KM005', 'Chào mừng ngày phụ nữ việt nam 20/10', '2020-06-24', '2020-06-30', 10, 20, 'Đang diễn ra'),
('KM006', 'Hỗ trợ sinh viên về quê ăn tết', '2020-01-01', '2020-01-30', 40, 15, 'Đã kết thúc'),
('KM007', 'Chúc mừng sinh nhật Jestart', '2020-06-23', '2020-06-30', 20, 20, 'Đang diễn ra'),
('KM008', 'Chúc mừng sinh nhật Bamboo', '2020-07-01', '2020-07-30', 20, 20, 'Chưa diễn ra'),
('KM009', 'Ưu đãi tháng 6', '2020-06-01', '2020-06-30', 20, 20, 'Đang diễn ra'),
('KM010', 'Ưu đãi tháng 7', '2020-07-01', '2020-07-30', 20, 33, 'Chưa diễn ra');

-- --------------------------------------------------------

--
-- Table structure for table `chuyenbay`
--

CREATE TABLE `chuyenbay` (
  `MaCB` varchar(10) CHARACTER SET utf8 NOT NULL,
  `MaHang` varchar(3) CHARACTER SET utf8 NOT NULL,
  `MaTuyenBay` varchar(20) CHARACTER SET utf8 NOT NULL,
  `MaMB` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SLGheThuongGia_CL` int(11) NOT NULL,
  `SLGhePhoThong_CL` int(11) NOT NULL,
  `GiaVeThuongGia` int(10) NOT NULL,
  `GiaVePhoThong` int(10) NOT NULL,
  `NgayBay` datetime NOT NULL,
  `NgayBayTT` datetime DEFAULT NULL,
  `TrangThai` varchar(30) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chuyenbay`
--

INSERT INTO `chuyenbay` (`MaCB`, `MaHang`, `MaTuyenBay`, `MaMB`, `SLGheThuongGia_CL`, `SLGhePhoThong_CL`, `GiaVeThuongGia`, `GiaVePhoThong`, `NgayBay`, `NgayBayTT`, `TrangThai`) VALUES
('BL2925', 'BL', 'QT001', 'AE56', 0, 2, 25000000, 15000000, '2020-06-20 06:00:00', '2020-06-20 10:00:00', 'Đã cất cánh'),
('BL4894', 'BL', 'TN004', 'AE56', 0, 5, 40000000, 25000000, '2020-06-12 10:00:00', NULL, 'Đã cất cánh'),
('BL7492', 'BL', 'QT005', 'AE56', 0, 6, 150000000, 90000000, '2020-07-30 00:00:00', '2020-07-31 01:00:00', 'Bình Thường'),
('BL7830', 'BL', 'TN030', '32A0', 8, 24, 50000000, 20000000, '2020-07-10 03:00:00', NULL, 'Đã bị hủy'),
('BL9673', 'BL', 'QT005', '32A0', 8, 22, 160000000, 80000000, '2020-07-30 03:00:00', '2020-07-31 05:00:00', 'Bình Thường'),
('QH1655', 'QH', 'TN004', '32A0', 8, 23, 45000000, 20000000, '2020-07-30 12:00:00', NULL, 'Bình Thường'),
('QH6706', 'QH', 'QT001', '32A0', 8, 17, 30000000, 16000000, '2020-06-27 13:00:00', '2020-06-27 19:00:00', 'Đã bị hủy'),
('QH8496', 'QH', 'QT005', 'AE56', 0, 4, 100000000, 60000000, '2020-07-30 03:00:00', '2020-07-31 02:00:00', 'Bình Thường'),
('VJ1618', 'VJ', 'QT005', 'MH17', 6, 8, 160000000, 80000000, '2020-07-30 03:00:00', '2020-07-31 09:00:00', 'Bình Thường'),
('VJ3703', 'VJ', 'QT005', '32A0', 3, 8, 110000000, 75000000, '2020-05-30 03:15:00', '2020-05-31 09:00:00', 'Đã cất cánh'),
('VJ3863', 'VJ', 'TN004', '32A0', 7, 23, 50000000, 25000000, '2020-07-30 14:00:00', NULL, 'Bình Thường'),
('VJ4882', 'VJ', 'QT028', '32A0', 0, 8, 120000000, 70000000, '2020-07-30 02:00:00', '2020-07-31 20:30:00', 'Bình Thường'),
('VJ4894', 'VJ', 'TN016', '32A0', 8, 24, 15000000, 6000000, '2020-07-23 02:00:00', NULL, 'Đã bị hủy'),
('VJ8589', 'VJ', 'QT001', 'MH17', 3, 6, 35000000, 18000000, '2020-06-27 09:00:00', '2020-06-27 19:00:00', 'Bình Thường'),
('VN1838', 'VN', 'QT002', '32A0', 8, 24, 20000000, 12000000, '2020-06-27 03:00:00', NULL, 'Báo động'),
('VN4541', 'VN', 'TN004', 'AK45', 5, 7, 60000000, 35000000, '2020-07-30 23:00:00', NULL, 'Bình Thường'),
('VN5868', 'VN', 'QT001', '32A0', 0, 20, 40000000, 22000000, '2020-06-27 09:00:00', '2020-06-27 16:00:00', 'Bình Thường'),
('VN7632', 'VN', 'QT005', '32A0', 0, 24, 170000000, 100000000, '2020-07-30 03:00:00', '2020-07-31 02:00:00', 'Bình Thường'),
('VN7642', 'VN', 'QT005', '32A0', 7, 24, 150000000, 90000000, '2020-07-30 03:00:00', '2020-07-31 06:00:00', 'Bình Thường');

-- --------------------------------------------------------

--
-- Table structure for table `codekm`
--

CREATE TABLE `codekm` (
  `MaCode` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TinhTrang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MaCTKM` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `codekm`
--

INSERT INTO `codekm` (`MaCode`, `TinhTrang`, `MaCTKM`) VALUES
('6h8uv', 'Chưa sử dụng', 'KM005'),
('6kihm', 'Đã sử dụng', 'KM002'),
('6oq79', 'Đã được tặng', 'KM007'),
('6wqz6', 'Đã được tặng', 'KM007'),
('6x17h', 'Chưa diễn ra', 'KM008'),
('6xm60', 'Chưa sử dụng', 'KM004'),
('6yo3y', 'Chưa sử dụng', 'KM005'),
('72bwt', 'Đã được tặng', 'KM004'),
('76od8', 'Đã sử dụng', 'KM001'),
('7o83n', 'Chưa sử dụng', 'KM004'),
('7x8ni', 'Chưa sử dụng', 'KM005'),
('89ft0', 'Đã sử dụng', 'KM009'),
('8og21', 'Đã sử dụng', 'KM006'),
('8q279', 'Đã được tặng', 'KM004'),
('8sv8n', 'Chưa sử dụng', 'KM005'),
('8vck9', 'Đã được tặng', 'KM008'),
('9des8', 'Chưa sử dụng', 'KM005'),
('9e0sj', 'Chưa sử dụng', 'KM004'),
('a28zi', 'Đã sử dụng', 'KM002'),
('ad69p', 'Đã sử dụng', 'KM009'),
('af61k', 'Đã sử dụng', 'KM006'),
('atw44', 'Chưa diễn ra', 'KM008'),
('awj0o', 'Đã được tặng', 'KM007'),
('bf9g1', 'Đã sử dụng', 'KM006'),
('biwi6', 'Đã sử dụng', 'KM006'),
('bjh87', 'Đã được tặng', 'KM007'),
('c08wd', 'Chưa sử dụng', 'KM005'),
('d0u7s', 'Đã sử dụng', 'KM007'),
('de16r', 'Đã được tặng', 'KM004'),
('dizzn', 'Đã được tặng', 'KM003'),
('dx3cr', 'Đã sử dụng', 'KM006'),
('e276s', 'Đã sử dụng', 'KM001'),
('e2ev5', 'Đã được tặng', 'KM003'),
('f4tcp', 'Đã sử dụng', 'KM001'),
('fbs19', 'Đã được tặng', 'KM008'),
('fs90j', 'Đã sử dụng', 'KM003'),
('fxb2c', 'Đã được tặng', 'KM004'),
('g0d0s', 'Đã được tặng', 'KM003'),
('g2017', 'Chưa sử dụng', 'KM004'),
('gl8a8', 'Đã được tặng', 'KM007'),
('gn14p', 'Đã được tặng', 'KM007'),
('gv3e7', 'Đã được tặng', 'KM009'),
('gx53e', 'Đã sử dụng', 'KM002'),
('gycjr', 'Đã được tặng', 'KM004'),
('h9gw8', 'Đã được tặng', 'KM007'),
('i2neg', 'Đã sử dụng', 'KM006'),
('i3r6c', 'Chưa diễn ra', 'KM008'),
('ik62d', 'Đã được tặng', 'KM003'),
('iq312', 'Đã sử dụng', 'KM001'),
('j16t0', 'Đã được tặng', 'KM003'),
('j1j46', 'Đã sử dụng', 'KM002'),
('j2kva', 'Chưa sử dụng', 'KM005'),
('k0o56', 'Đã được tặng', 'KM009'),
('k3awu', 'Đã sử dụng', 'KM006'),
('kmt8s', 'Chưa sử dụng', 'KM004'),
('ktia5', 'Đã sử dụng', 'KM006'),
('m2o20', 'Đã được tặng', 'KM007'),
('m57s1', 'Đã được tặng', 'KM010'),
('m5uqv', 'Đã sử dụng', 'KM006'),
('m5wk2', 'Đã sử dụng', 'KM009'),
('m6bgp', 'Đã sử dụng', 'KM006'),
('m9m4v', 'Đã sử dụng', 'KM009'),
('ml4pg', 'Đã được tặng', 'KM008'),
('mxh0y', 'Chưa diễn ra', 'KM010'),
('mzyb2', 'Đã sử dụng', 'KM006'),
('nwbs6', 'Đã được tặng', 'KM007'),
('o124z', 'Đã được tặng', 'KM003'),
('om88f', 'Đã được tặng', 'KM008'),
('osy53', 'Chưa sử dụng', 'KM009'),
('ou17h', 'Đã sử dụng', 'KM009'),
('pj8vt', 'Đã được tặng', 'KM008'),
('pyxy9', 'Đã sử dụng', 'KM009'),
('q0ho8', 'Đã sử dụng', 'KM001'),
('q4hpq', 'Đã sử dụng', 'KM001'),
('q64ts', 'Chưa diễn ra', 'KM008'),
('q94hk', 'Đã sử dụng', 'KM006'),
('q95jy', 'Đã được tặng', 'KM003'),
('qya1t', 'Đã sử dụng', 'KM007'),
('r89tl', 'Chưa sử dụng', 'KM004'),
('roy6j', 'Đã sử dụng', 'KM007'),
('rvv9b', 'Chưa sử dụng', 'KM005'),
('ry075', 'Chưa sử dụng', 'KM007'),
('s1je7', 'Chưa diễn ra', 'KM008'),
('s4h8b', 'Đã được tặng', 'KM004'),
('ss82c', 'Đã sử dụng', 'KM002'),
('swovc', 'Đã được tặng', 'KM008'),
('sx0n8', 'Đã sử dụng', 'KM001'),
('taiw3', 'Chưa sử dụng', 'KM004'),
('ty50a', 'Chưa sử dụng', 'KM009'),
('tzc9j', 'Đã sử dụng', 'KM001'),
('u1l28', 'Chưa sử dụng', 'KM005'),
('u809u', 'Chưa sử dụng', 'KM003'),
('ufog4', 'Đã sử dụng', 'KM001'),
('v31s0', 'Chưa sử dụng', 'KM005'),
('v3290', 'Chưa diễn ra', 'KM010'),
('vsg29', 'Đã sử dụng', 'KM006'),
('vx76y', 'Chưa sử dụng', 'KM009'),
('w85v3', 'Đã sử dụng', 'KM006'),
('wcwbl', 'Chưa sử dụng', 'KM009'),
('wh10h', 'Chưa sử dụng', 'KM009'),
('x0jk7', 'Đã sử dụng', 'KM007'),
('x85tx', 'Chưa sử dụng', 'KM009'),
('y3124', 'Chưa sử dụng', 'KM003'),
('yqg11', 'Đã sử dụng', 'KM006'),
('zlvc3', 'Chưa sử dụng', 'KM004');

-- --------------------------------------------------------

--
-- Table structure for table `hanghangkhong`
--

CREATE TABLE `hanghangkhong` (
  `MaHang` varchar(2) CHARACTER SET utf8 NOT NULL,
  `TenHang` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `QuocGia` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `pathLoGo` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hanghangkhong`
--

INSERT INTO `hanghangkhong` (`MaHang`, `TenHang`, `QuocGia`, `pathLoGo`) VALUES
('BL', 'Jetstar Pacific Airlines', 'Việt Nam', 'D:/xinchao/DoAn/src/logoHHK/BL.jpg'),
('QH', 'Bamboo Airways', 'Việt Nam', 'D:/xinchao/DoAn/src/logoHHK/QH.jpg'),
('VJ', 'Vietjet Air', 'Việt Nam', 'D:/xinchao/DoAn/src/logoHHK/VJ.jpg'),
('VN', 'Vietnam Airlines', 'Việt Nam', 'D:/xinchao/DoAn/src/logoHHK/VN.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `hanhly`
--

CREATE TABLE `hanhly` (
  `MaHanhLy` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GiaHanhLy` int(10) NOT NULL,
  `KhoiLuong` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hanhly`
--

INSERT INTO `hanhly` (`MaHanhLy`, `GiaHanhLy`, `KhoiLuong`) VALUES
('HL007', 0, 7),
('HL020', 500000, 20),
('HL050', 4000000, 50);

-- --------------------------------------------------------

--
-- Table structure for table `hoadon`
--

CREATE TABLE `hoadon` (
  `MaHD` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MaNV` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NgayBan` date NOT NULL,
  `MaKH` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TongTien` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hoadon`
--

INSERT INTO `hoadon` (`MaHD`, `MaNV`, `NgayBan`, `MaKH`, `TongTien`) VALUES
('HD1', 'NV01', '2020-05-17', '0123456789123', 40500000),
('HD10', 'NV01', '2020-05-17', '0123456789123', 15000000),
('HD11', 'NV01', '2020-05-17', '0123456789123', 35000000),
('HD12', 'NV01', '2020-05-18', '1234567891235', 44000000),
('HD13', 'NV01', '2020-05-18', '0123456789123', 100000000),
('HD14', 'NV01', '2020-05-18', '7894561230123', 29000000),
('HD15', 'NV01', '2020-06-18', '0123456789123', 16000000),
('HD16', 'NV01', '2020-01-18', '7894564567894', 116000000),
('HD17', 'NV01', '2020-01-18', '0123456789123', 44000000),
('HD18', 'NV01', '2020-01-18', '0123456789123', 29000000),
('HD19', 'NV01', '2020-06-19', '0123456789123', 19000000),
('HD2', 'NV01', '2020-05-17', '0123456789456', 88000000),
('HD20', 'NV01', '2020-06-19', '0123456789123', 15000000),
('HD21', 'NV01', '2020-06-19', '0123456789123', 19000000),
('HD22', 'NV01', '2020-06-20', '7531594862456', 88000000),
('HD23', 'NV01', '2020-06-20', '7568945125478', 39000000),
('HD24', 'NV01', '2020-06-20', '0123456789123', 15000000),
('HD25', 'NV01', '2020-05-22', '4893697459821', 60000000),
('HD26', 'NV03', '2020-01-22', '0123456789123', 20000000),
('HD27', 'NV03', '2020-05-15', '3295697528392', 229000000),
('HD28', 'NV03', '2020-04-15', '4589374589733', 228000000),
('HD29', 'NV09', '2020-03-15', '9283492834932', 466000000),
('HD3', 'NV01', '2020-06-17', '0123456789123', 35000000),
('HD30', 'NV09', '2020-03-15', '0932859323213', 154000000),
('HD31', 'NV08', '2020-02-15', '3247239852234', 853500000),
('HD32', 'NV08', '2020-03-15', '3248234234212', 124000000),
('HD33', 'NV07', '2020-03-15', '9032829308421', 714000000),
('HD34', 'NV01', '2020-04-15', '3204923042312', 140500000),
('HD35', 'NV01', '2020-04-15', '9984590348523', 280500000),
('HD36', 'NV01', '2020-04-15', '2429427489123', 387000000),
('HD37', 'NV03', '2020-04-15', '0948328793412', 342000000),
('HD38', 'NV03', '2020-05-23', '7514863021485', 220000000),
('HD39', 'NV03', '2020-06-23', '0123456789123', 58000000),
('HD4', 'NV01', '2020-06-17', '0123456789123', 18000000),
('HD40', 'NV03', '2020-06-23', '0123456789123', 29000000),
('HD41', 'NV03', '2020-06-23', '0123456789123', 24000000),
('HD42', 'NV03', '2020-01-23', '0123456789123', 29000000),
('HD43', 'NV03', '2020-01-23', '0123456789123', 39000000),
('HD44', 'NV03', '2020-06-23', '0123456789123', 60000000),
('HD45', 'NV03', '2020-06-23', '0123456789123', 44000000),
('HD46', 'NV03', '2020-06-23', '0123456789123', 54000000),
('HD47', 'NV03', '2020-06-23', '0123456789123', 90000000),
('HD48', 'NV03', '2020-05-23', '0123456789123', 154000000),
('HD49', 'NV03', '2020-06-23', '0123456789123', 60000000),
('HD5', 'NV01', '2020-06-17', '0123456789123', 16000000),
('HD50', 'QL01', '2020-01-23', '0584136985214', 696000000),
('HD51', 'NV08', '2020-03-23', '8753698521456', 692000000),
('HD52', 'QL01', '2020-06-23', '0123456789123', 762000000),
('HD53', 'QL01', '2020-05-23', '0930492312345', 612000000),
('HD54', 'QL01', '2020-05-23', '3204923042312', 184000000),
('HD55', 'QL01', '2020-06-24', '0123456789123', 90000000),
('HD56', 'QL01', '2020-06-24', '0123456789123', 84000000),
('HD57', 'QL01', '2020-06-24', '0123456789123', 154000000),
('HD58', 'QL01', '2020-06-25', '0123456789123', 84000000),
('HD6', 'NV01', '2020-06-17', '0123456789123', 20000000),
('HD7', 'NV01', '2020-06-17', '0123456789123', 22000000),
('HD8', 'NV01', '2020-01-17', '0123456789123', 44000000),
('HD9', 'NV01', '2020-06-17', '0123456789123', 15000000);

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

CREATE TABLE `khachhang` (
  `MaKH` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `HoKH` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TenKH` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SDT` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Gmail` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GioiTinh` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NgaySinh` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `khachhang`
--

INSERT INTO `khachhang` (`MaKH`, `HoKH`, `TenKH`, `SDT`, `Gmail`, `GioiTinh`, `NgaySinh`) VALUES
('0123456789123', 'Vũ', 'Anh Phúc', '0903198906', 'phucvu01120@gmail.com', 'Nam', '2000-02-11'),
('0123456789456', 'Phạm', 'Minh Mẫn', '0906159874', 'man21082015@gmail.com', 'Nam', '1997-06-20'),
('0584136985214', 'Trung', 'VĂn Trạng', '0487695123', 'trangtrung@gmail.com', 'Nam', '1996-02-16'),
('0930492312345', 'Vũ', 'Văn Thanh', '2234234869', 'thanh@gmail.com', 'Nam', '1996-09-14'),
('0932859323213', 'Phạm', 'Thị Anh', '', 'anh@gmail.com', 'Nữ', '2000-06-15'),
('0948328793412', 'Ông', 'Cao Thắng', '0907772345', 'thang@gmail.com', 'Nam', '1987-06-04'),
('1204983242912', 'Đặng', 'Văn Lâm', '0992304932', 'lam@gmail.com', 'Nam', '1999-11-16'),
('1234567891235', 'Pham', 'Minh', '0124891234', 'minhpham@gmail.com', 'Nam', '1992-06-12'),
('2095834908512', 'Nguyễn', 'Văn Đức', '0094023940', 'duc@gmail.com', 'Nam', '1999-06-05'),
('2342409029341', 'Điện', 'Biên Phủ', '0909345009', 'phu@gmail.com', 'Nam', '1918-09-14'),
('2429427489123', 'Vũ', 'Anh Em', '0905552098', 'Em@gmail.com', 'Nam', '1999-06-11'),
('2578935793445', 'Ông', 'Văn Thắng', '0342234232', 'thang@gmail.com', 'Nam', '1995-10-31'),
('3204923042312', 'Vũ', 'Mạnh Hùng', '0921039131', 'hung@gmail.com', 'Nam', '1919-06-21'),
('3247239852234', 'Huỳnh', 'Công Trí', '0901230213', 'tri@gmail.com', 'Nam', '1993-10-07'),
('3248234234212', 'Lý', 'Như Kiệt', '0920492342', 'kiet@gmail.com', 'Nam', '2000-06-08'),
('3295697528392', 'Nguyễn', 'Thị Thơm', '0908889292', 'thom@gmail.com', 'Nữ', '1987-06-12'),
('3445345341234', 'Park', 'Hang Seo', '2343239439', 'seo@gmail.com', 'Nam', '1976-09-04'),
('4234234223454', 'Nguyễn', 'Công Phượng', '0923402423', 'phuong@gmail.com', 'Nam', '1995-06-14'),
('4385345345534', 'Nguyễn', 'Văn Toàn', '0293042543', 'toan@gmail.com', 'Nam', '1995-06-01'),
('4589374589733', 'Hứa', 'Gia Vinh', '0808945345', 'vinh@gmail.com', 'Nam', '2000-06-22'),
('4782365012368', 'Nguyễn', 'Ngọc Nguyên', '0789123586', 'nguyennguyen@gmail.com', 'Nữ', '1959-06-18'),
('4862159875362', 'Lê', 'Văn Hải', '', '', 'Nữ', '1950-06-16'),
('4893697459821', 'Cao', 'văn Cao', '0123456123', 'caocao@gmail.com', 'Nam', '1992-06-12'),
('4983274827678', 'Trịnh', 'Thị Hoà', '0902202893', 'hoa@gmail.com', 'Nữ', '1993-06-13'),
('7514863021485', 'Lê', 'Thị Riêng', '0486214862', 'rienglethi@gmail.com', 'Nữ', '1946-06-14'),
('7531594862456', 'Lê', 'Văn Sĩ', '0456789123', 'sile11@gmail.com', 'Nam', '1993-06-24'),
('7568945125478', 'Lê', 'Văn Long', '0456789123', 'longlevan@gmail.com', 'Nam', '1991-06-14'),
('7589321452012', 'Nguyễn', 'Ngọc Ngạn', '0458771236', 'ngannguyen@gmail.com', 'Nam', '1957-06-21'),
('7894561230123', 'Phạm', 'Hiếu Hiếu', '0456789123', 'hieuhieu@gmail.com', 'Nam', '1990-06-07'),
('7894561231598', 'Phạm', 'Trọng Nhân', '0456789123', 'trongnhan@gmail.com', 'Nam', '1996-06-07'),
('7894564567894', 'Nguyễn', 'Văn Lang', '0456789123', 'vanlang@gmail.com', 'Nam', '1996-06-07'),
('8746148625987', 'Nguyễn', 'Thúy Vân', '', '', 'Nữ', '1943-02-19'),
('8753698521456', 'Nguyễn', 'Thúy Kiều', '', '', 'Nữ', '1940-02-15'),
('9032829308421', 'Lương', 'Xuân Trường', '0920931203', 'truong@gmail.com', 'Nam', '1995-09-11'),
('9073432489212', 'Nguyễn', 'Công Minh', '0908882098', 'minh@gmail.com', 'Nam', '1987-06-19'),
('9283492834932', 'Hứa', 'Kim Chi', '0920348832', 'Chi@gmail.com', 'Nữ', '1993-06-30'),
('9485345439304', 'Huỳnh', 'Tấn Đạt', '9845345855', 'dat@gmail.com', 'Nam', '1992-06-13'),
('9848238475893', 'Nguyễn', 'Hồng Ngọc', '9347589312', 'ngoc@gmail.com', 'Nam', '1990-06-09'),
('9898965469456', 'Đặng', 'Phương Như', '0903323235', 'nhu@gmail.com', 'Nam', '2000-06-08'),
('9984590348523', 'Vũ', 'Công Anh', '', '', 'Nam', '2000-06-03'),
('TE-1073341541', 'Phạm', 'Công Anh', '', '', 'Nam', '2017-06-16'),
('TE-1085946287', 'Vũ', 'Hương', '', '', 'Nữ', '2018-02-02'),
('TE-1109311094', 'Phạm', 'Minh Minh', '', '', 'Nam', '2020-06-01'),
('TE-1151195381', 'Vũ', 'Minh', '', '', 'Nam', '2013-05-04'),
('TE-1156276307', 'Vũ', 'Trường', '', '', 'Nữ', '2017-02-17'),
('TE-1159798221', 'Hứa', 'Gia Vy', '', '', 'Nam', '2013-06-14'),
('TE-1160099092', 'Lý', 'Thường Kiệt', '', '', 'Nam', '2019-06-01'),
('TE-1208306296', 'Nguyễn', 'Sinh Trưởng', '', '', 'Nữ', '2018-06-16'),
('TE-1217238722', 'Phạm', 'Minh Mèo', '', '', 'Nam', '2015-06-25'),
('TE-1239077925', 'Vũ', 'Trọng', '', '', 'Nam', '2017-02-17'),
('TE-1249890232', 'Nguyễn', 'Thực', '', '', 'Nam', '2017-06-17'),
('TE-1315513174', 'Ông', 'Toàn Như', '', '', 'Nam', '2017-12-19'),
('TE-1338128927', 'Vũ', 'Trường Vũ', '', '', 'Nữ', '2018-02-16'),
('TE-1457322144', 'Lê', 'văn Van', '', '', 'Nam', '2017-06-15'),
('TE-1513711479', 'Lê', 'Văn Súng', '', '', 'Nam', '2018-06-14'),
('TE-1522686372', 'Lương', 'Thị Của', '', '', 'Nam', '2018-09-08'),
('TE-1597503394', 'Cao', 'Cao Cao', '', '', 'Nam', '2014-06-19'),
('TE-1598916005', 'Nguyễn', 'Luios', '', '', 'Nữ', '2018-02-16'),
('TE-1626971832', 'Lê', 'Lỏng', '', '', 'Nam', '2014-06-19'),
('TE-1631287490', 'Lê', 'Lisa', '', '', 'Nữ', '2018-02-16'),
('TE-1643291267', 'Nguyễn', 'Lê', '', '', 'Nam', '2019-06-14'),
('TE-1659992983', 'Lý', 'Mặc Sầu', '', '', 'Nam', '2019-06-22'),
('TE-1736379815', 'Lý', 'Liên Kiệt', '', '', 'Nam', '2020-06-01'),
('TE-1784308007', 'Tiểu', 'Long Nữ', '', '', 'Nam', '2015-02-20'),
('TE-1796633873', 'Lương', 'Định Của', '', '', 'Nữ', '2018-09-29'),
('TE-1826546421', 'Ro', 'naldo', '', '', 'Nam', '2019-02-08'),
('TE-1861582291', 'Vũ', 'Công', '', '', 'Nam', '2017-02-17'),
('TE-1865690344', 'David', 'Move', '', '', 'Nam', '2018-02-16'),
('TE-1865743428', 'Huỳnh', 'Như Như', '', '', 'Nam', '2015-10-10'),
('TE-1879502189', 'Phạm', 'Thông', '', '', 'Nam', '2017-06-28'),
('TE-1934831164', 'Vũ', 'Huy Hoàng', '', '', 'Nam', '2019-06-01'),
('TN-1132377182', 'Nguyễn', 'David', '', '', 'Nam', '2004-06-05'),
('TN-1138093344', 'Hứa', 'Vĩnh Cửu', '', '', 'Nam', '2003-06-20'),
('TN-1141179100', 'Nguyễn', 'Công Anh', '', '', 'Nam', '2003-06-19'),
('TN-1219331237', 'Lê ', 'Trung Kiên', '', '', 'Nam', '2004-02-20'),
('TN-1430470020', 'Huỳnh', 'Quỳnh Như', '4534534233', 'nhu@gmail.com', 'Nam', '2003-06-14'),
('TN-1757005992', 'Vũ', 'Anh Phu', '', '', 'Nữ', '2003-06-13'),
('TN-1844897221', 'Nguyễn', 'Cục Cưng', '', '', 'Nam', '2003-06-03'),
('TN-1892481412', 'Phạm', 'Hu Hu', '', '', 'Nam', '2004-05-13'),
('TN-1947461140', 'Lý', 'Quý Phái', '9249283948', 'phai@gmail.com', 'Nam', '2003-06-20');

-- --------------------------------------------------------

--
-- Table structure for table `maybay`
--

CREATE TABLE `maybay` (
  `MaMB` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MoTa` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `MaMHMB` varchar(20) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `maybay`
--

INSERT INTO `maybay` (`MaMB`, `MoTa`, `MaMHMB`) VALUES
('32A0', 'Sản xuất năm 2005 bởi công ty S23', 'a32'),
('AE56', 'Sản xuất năm 1980', 'a14'),
('AK45', 'Máy bay đặt biệt của hảng hàng không Đức 234', 'a14'),
('MH17', 'Máy bay sản xuất giớ hạn bởi TYq\n', 'a14'),
('QC342', 'Hãng sản xuất máy bay IUO', 'a32'),
('T351', 'Sản xuất năm 2009', 'a32'),
('T351A', 'Sản xuất năm 2009', 'a32'),
('T351B', 'Sản xuất năm 2009', 'a32'),
('T351C', 'Sản xuất năm 2009', 'a32'),
('T351D', 'Sản xuất năm 2009', 'a32'),
('T351E', 'Sản xuất năm 2009', 'a32'),
('T351F', 'Sản xuất năm 2009', 'a32'),
('T351G', 'Sản xuất năm 2009', 'a32'),
('T351H', 'Sản xuất năm 2009', 'a32'),
('TT2A', 'Sản xuất năm 2009', 'a32'),
('TT56', 'Sản xuất năm 2009', 'a32'),
('UI2', 'Sản xuất năm 2005\n', 'a14');

-- --------------------------------------------------------

--
-- Table structure for table `mohinhmaybay`
--

CREATE TABLE `mohinhmaybay` (
  `MaMHMB` varchar(20) CHARACTER SET utf8 NOT NULL,
  `SoGheThuongGia` int(11) NOT NULL,
  `SoGhePhoThong` int(11) NOT NULL,
  `DSGheThuongGia` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DSGhePhoThong` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mohinhmaybay`
--

INSERT INTO `mohinhmaybay` (`MaMHMB`, `SoGheThuongGia`, `SoGhePhoThong`, `DSGheThuongGia`, `DSGhePhoThong`) VALUES
('a14', 6, 8, 'A1-A3,B1-B3', 'A4-A7,B4-B7'),
('a32', 8, 24, 'A9-A12,B9-B12', 'A1-A8,B1-B8,A13-A16,B13-B16');

-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE `nhanvien` (
  `MaNV` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `HoNV` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TenNV` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GioiTinh` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NgaySinh` date NOT NULL,
  `DiaChi` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SDT` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Gmail` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NgayBatDau` date NOT NULL,
  `ChucVu` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Luong` int(10) NOT NULL,
  `MatKhau` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pathImg` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nhanvien`
--

INSERT INTO `nhanvien` (`MaNV`, `HoNV`, `TenNV`, `GioiTinh`, `NgaySinh`, `DiaChi`, `SDT`, `Gmail`, `NgayBatDau`, `ChucVu`, `Luong`, `MatKhau`, `pathImg`) VALUES
('NV01', 'Nguyễn', 'Thị Hiền', 'Nữ', '2000-01-12', 'tphcm', '1234567890', 'hien@gmail.com', '2019-03-12', 'Nhân Viên', 14000000, '113', 'D:/xinchao/DoAn/src/imageNV/NV1.jpg'),
('NV02', 'Trần', 'Thị Ngân', 'Nữ', '2000-05-04', 'tphcm', '1231267890', 'ngan@gmail.com', '2019-05-18', 'Nhân Viên', 13000000, '123456', 'D:/xinchao/DoAn/src/imageNV/1.jpg'),
('NV03', 'Bùi', 'Nguyễn Anh ', 'Nam', '1999-05-03', 'tphcm', '0123012300', 'anhbui@gmail.com', '2019-02-03', 'Quản lý', 15000000, '123', 'D:/xinchao/DoAn/src/imageNV/1unnamed.jpg'),
('NV04', 'Nguyễn', 'Tuấn Mạnh', 'Nam', '1998-05-04', 'tphcm', '0000000002', 'manh@gmail.com', '2019-05-24', 'Nhân Viên', 16000000, '123', 'D:/xinchao/DoAn/src/imageNV/3x4.jpg'),
('NV05', 'Đinh', 'An', 'Nam', '2000-01-03', 'tphcm', '0123123000', 'an@gmail.com', '2019-05-03', 'Nhân Viên', 14000000, '123', 'D:/xinchao/DoAn/src/imageNV/anh4x6.jpg'),
('NV06', 'Đoàn', 'Văn Hậu', 'Nam', '1998-05-04', 'tphcm', '0011156789', 'hau@gmail.com', '2019-02-03', 'Nhân Viên', 17000000, '123', 'D:/xinchao/DoAn/src/imageNV/bti1412957527.JPG'),
('NV07', 'Lê', 'Văn Đại', 'Nam', '1997-05-04', 'tphcm', '0011567987', 'dai@gmail.com', '2019-04-03', 'Nhân Viên', 17000000, '123', 'D:/xinchao/DoAn/src/imageNV/CvQkhSg.jpg'),
('NV08', 'Đào', 'Ngọc Mỹ', 'Nữ', '2000-05-04', 'tphcm', '0098798700', 'my@gmail.com', '2019-05-04', 'Nhân Viên', 15000000, '123', 'D:/xinchao/DoAn/src/imageNV/images.jpg'),
('NV09', 'Lê', 'Thị Ngọc Hương', 'Nữ', '1997-05-11', 'tphcm', '0098798711', 'huong@gmail.com', '2019-05-12', 'Nhân Viên', 16000000, '123', 'D:/xinchao/DoAn/src/imageNV/NV2.jpg'),
('NV10', 'Nguyễn', 'Xuân Huy', 'Nam', '1998-02-23', 'tphcm', '0099123123', 'huy@gmail.com', '2019-02-21', 'Nhân Viên', 16000000, '123', 'D:/xinchao/DoAn/src/imageNV/NoImage.jpg'),
('NV11', 'Nguyễn', 'Thanh Hải', 'Nam', '1997-03-21', 'tphcm', '0099123789', 'hai@gmail.com', '2019-03-11', 'Nhân Viên', 17000000, '123', 'D:/xinchao/DoAn/src/imageNV/thu 30.jpg'),
('NV12', 'Vũ', 'Thị Tuyền', 'Nữ', '1997-04-11', 'tphcm', '0012345611', 'tuyen@gmail.com', '2019-05-03', 'Nhân Viên', 15000000, '123', 'D:/xinchao/DoAn/src/imageNV/un1named.jpg'),
('NV13', 'Hồ', 'Thị Kim', 'Nữ', '1998-03-21', 'tphcm', '0012332189', 'kim@gmail.com', '2019-01-21', 'Nhân Viên', 18000000, '123', 'D:/xinchao/DoAn/src/imageNV/unnamed.jpg'),
('NV14', 'Lê', 'Quốc Luân', 'Nam', '1997-05-10', 'tphcm', '0909123123', 'luan@gmail.com', '2019-04-12', 'Nhân Viên', 17000000, '123', 'D:/xinchao/DoAn/src/imageNV/unnamed1.jpg'),
('NV15', 'Nguyễn', 'Thị Minh Huyền', 'Nữ', '1998-09-12', 'tphcm', '0932327889', 'huyenng@gmail.com', '2019-05-02', 'Nhân Viên', 16000000, '123', 'D:/xinchao/DoAn/src/imageNV/NoImage.jpg'),
('NV16', 'Bùi', 'Xuân Thu', 'Nữ', '1997-05-04', 'tphcm', '0954354300', 'xuanthu@gmail.com', '2019-03-11', 'Nhân Viên', 18000000, '123', 'D:/xinchao/DoAn/src/imageNV/NoImage.jpg'),
('NV17', 'Trần', 'Bích Hợp', 'Nữ', '1996-04-23', 'tphcm', '0967812310', 'bichhop@gmail.com', '2019-04-12', 'Nhân Viên', 17000000, '123', 'D:/xinchao/DoAn/src/imageNV/NoImage.jpg'),
('NV18', 'Bùi', 'Trung Kiên', 'Nam', '1997-03-29', 'tphcm', '0961236540', 'trungkien@gmail.com', '2019-04-20', 'Nhân Viên', 18000000, '123', 'D:/xinchao/DoAn/src/imageNV/NoImage.jpg'),
('NV19', 'Nguyễn', 'Kim Lân', 'Nữ', '1996-08-23', 'tphcm', '0961239870', 'tphcm', '2019-03-03', 'Nhân Viên', 17000000, '123', 'D:/xinchao/DoAn/src/imageNV/NoImage.jpg'),
('NV20', 'Trần', 'Thị Phương', 'Nữ', '1997-09-23', 'tphcm', '0965466123', 'thiphuong@gmail.com', '2019-02-02', 'Nhân Viên', 17000000, '123', 'D:/xinchao/DoAn/src/imageNV/NoImage.jpg'),
('QL01', 'Vũ', 'Anh Phúc', 'Nam', '2000-02-11', 'B57e Mỹ Hòa 2 Xuân Thới Đông Hóc Môn', '0903198906', 'phucvu@gmail.com', '2020-06-23', 'Quản lý', 40000000, '123', 'D:/xinchao/DoAn/src/imageNV/NV2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sanbay`
--

CREATE TABLE `sanbay` (
  `MaSanBay` varchar(4) CHARACTER SET utf8 NOT NULL,
  `TenSanBay` varchar(100) CHARACTER SET utf8 NOT NULL,
  `ThanhPho` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sanbay`
--

INSERT INTO `sanbay` (`MaSanBay`, `TenSanBay`, `ThanhPho`) VALUES
('', '', ''),
('ACY', 'Atlantic', 'Atlantic, New Jersey'),
('ALB', 'Albany', 'Albany, New York'),
('AMQ', 'Pattimura', 'Ambon'),
('AOC', 'Altenburg-Nobitz', 'Altenburg'),
('AOT', 'Aosta', 'Aosta'),
('AUH', 'Abu Dhabi', 'Abu Dhabi'),
('AXT', 'Akita', 'Akita'),
('AZI', 'Bateen Executive', 'Abu Dhabi'),
('BFL', 'Meadows Field', 'Bakersfield, California'),
('BMV', 'Buôn Mê Thuột', 'Đắk Lắk'),
('BON', 'Flamingo', 'Kralendijk'),
('BRE', 'Bremen', 'Bremen'),
('BUF', 'Buffalo Niagara', 'Buffalo, New York'),
('BWN', 'Brunei', 'Bandar Seri Begawan'),
('CAH', 'Cà Mau', 'Cà Mau'),
('CJU', 'Jeju', 'Jeju'),
('CQF', 'Calais', 'Dunkerque'),
('CUR', 'Hato', 'Willemstad'),
('CXR', 'Cam Ranh', 'Khánh Hoà'),
('DAD', 'Đà Nẵng', 'Đà Nẵng'),
('DIN', 'Điện Biên Phủ', 'Điện Biên'),
('DLI', 'Liên Khương', 'Lâm Đồng'),
('DPE', 'Dieppe', 'Saint-Aubin'),
('DPS', 'Ngurah Rai', 'Denpasar'),
('FNJ', 'Sunan', 'Bình Nhưỡng'),
('GMP', 'Gimpo', 'Seoul'),
('GTF', 'Great Falls', 'Great Falls, Montana'),
('HAN', 'Nội Bài', 'Hà Nội'),
('HIJ', 'Hiroshima', 'Hiroshima'),
('HND', 'Tokyo', 'Tokyo'),
('HNL', 'Honolulu', 'Honolulu'),
('HPH', 'Cát Bi', 'Hải Phòng'),
('HRL', 'Valley', 'Harlingen, Texas'),
('HUI', 'Phú Bài', 'Thừa Thiên-Huế'),
('IAH', 'George Bush', 'Houston'),
('ICN', 'Incheon', 'Incheon'),
('KIX', 'Kansai', 'Osaka'),
('LOP', 'Lombok', 'Mataram'),
('LRH', 'La Rochelle', 'Laleu'),
('MDW', 'Chicago Midway', 'Chicago'),
('MFM', 'Ma Cao', 'Ma Cao'),
('MNL', 'Ninoy Aquino', 'Manila'),
('NGS', 'Nagasaki', 'Nagasaki'),
('PNH', 'Phnôm Pênh', 'Phnôm Pênh'),
('PQC', 'Phú Quốc', 'Kiên Giang'),
('PSR', 'Abruzzo', 'Pescara'),
('PTY', 'Tocumen', 'Panama'),
('PUS', 'Gimhae', 'Busan'),
('PXU', 'Pleiku', 'Gia Lai'),
('REP', 'Xiêm Riệp', 'Xiêm Riệp'),
('RNI', 'Corn Island', 'Corn'),
('RSW', 'Southwest Florida', 'Fort Myers, Florida'),
('SGN', 'Tân Sơn Nhất', 'Hồ Chí Minh'),
('SIN', 'Singapore Changi Airport', 'Singapore'),
('TAE', 'Daegu', 'Daegu'),
('TBB', 'Tuy Hoà', 'Phú Yên'),
('THD', 'Thọ Xuân', 'Thanh Hoá'),
('TSF', 'Treviso', 'Treviso'),
('TXL', 'Tegel', 'Berlin'),
('UIH', 'Phù Cát', 'Bình Định'),
('ULN', 'Thành Cát Tự Hãn', 'Ulaanbaatar'),
('VCA', 'Cần Thơ', 'Cần Thơ'),
('VCL', 'Chu Lai', 'Quảng Nam'),
('VCS', 'Côn Đảo', 'Bà Rịa-Vũng Tàu'),
('VDH', 'Đồng Hới', 'Quảng Bình'),
('VII', 'Vinh', 'Nghệ An'),
('YEG', 'Edmonton', 'Edmonton'),
('YQM', 'Greater Moncton', 'Moncton'),
('YYC', 'Calgary', 'Calgary'),
('ZDY', 'Dalma', 'Đảo Dalma');

-- --------------------------------------------------------

--
-- Table structure for table `tuyenbay`
--

CREATE TABLE `tuyenbay` (
  `MaTuyenBay` varchar(20) CHARACTER SET utf8 NOT NULL,
  `MaSBCatCanh` varchar(3) CHARACTER SET utf8 NOT NULL,
  `MaSBHaCanh` varchar(3) CHARACTER SET utf8 NOT NULL,
  `MaSBTrungGian` varchar(3) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tuyenbay`
--

INSERT INTO `tuyenbay` (`MaTuyenBay`, `MaSBCatCanh`, `MaSBHaCanh`, `MaSBTrungGian`) VALUES
('QT001', 'SGN', 'GMP', 'HAN'),
('QT002', 'HAN', 'GMP', ''),
('QT003', 'DAD', 'HND', ''),
('QT004', 'SGN', 'AUH', 'SIN'),
('QT005', 'SGN', 'ALB', 'HND'),
('QT006', 'DAD', 'GMP', ''),
('QT007', 'DAD', 'ALB', 'HND'),
('QT008', 'DAD', 'HIJ', ''),
('QT009', 'HAN', 'ALB', 'HND'),
('QT010', 'SGN', 'ACY', 'CJU'),
('QT011', 'SGN', 'BWN', 'FNJ'),
('QT012', 'SGN', 'HND', 'SIN'),
('QT013', 'SGN', 'HRL', 'FNJ'),
('QT014', 'SGN', 'BRE', 'GMP'),
('QT015', 'SGN', 'GTF', 'HIJ'),
('QT016', 'SGN', 'IAH', 'HIJ'),
('QT017', 'DAD', 'KIX', 'ICN'),
('QT018', 'HAN', 'LOP', 'MFM'),
('QT019', 'HAN', 'NGS', 'PSR'),
('QT020', 'HAN', 'PUS', 'MFM'),
('QT021', 'HAN', 'TAE', 'YYC'),
('QT022', 'HAN', 'ZDY', 'PSR'),
('QT023', 'HAN', 'TSF', 'TXL'),
('QT024', 'HAN', 'YEG', 'YYC'),
('QT025', 'HAN', 'AMQ', 'SIN'),
('QT026', 'DAD', 'BON', 'AXT'),
('QT027', 'DAD', 'AZI', 'SIN'),
('QT028', 'DAD', 'BFL', 'CQF'),
('QT029', 'DAD', 'CUR', 'DPE'),
('QT030', 'DAD', 'IAH', 'SIN'),
('TN001', 'HAN', 'SGN', ''),
('TN002', 'DIN', 'DAD', ''),
('TN003', 'HPH', 'SGN', ''),
('TN004', 'SGN', 'HAN', ''),
('TN005', 'SGN', 'PQC', ''),
('TN006', 'HAN', 'PQC', ''),
('TN007', 'DAD', 'PQC', ''),
('TN008', 'SGN', 'VCS', ''),
('TN009', 'SGN', 'VII', ''),
('TN010', 'HAN', 'VII', ''),
('TN011', 'HAN', 'VCA', ''),
('TN012', 'PQC', 'SGN', ''),
('TN013', 'PQC', 'HAN', ''),
('TN014', 'PQC', 'DAD', ''),
('TN015', 'BMV', 'SGN', ''),
('TN016', 'SGN', 'BMV', ''),
('TN017', 'SGN', 'CAH', ''),
('TN018', 'HAN', 'CAH', ''),
('TN019', 'CAH', 'SGN', ''),
('TN020', 'CXR', 'PQC', ''),
('TN021', 'SGN', 'DLI', ''),
('TN022', 'HAN', 'DLI', ''),
('TN023', 'DAD', 'DLI', ''),
('TN024', 'SGN', 'HUI', ''),
('TN025', 'HUI', 'SGN', ''),
('TN026', 'HAN', 'HUI', ''),
('TN027', 'SGN', 'TBB', ''),
('TN028', 'DAD', 'TBB', ''),
('TN029', 'SGN', 'VCL', ''),
('TN030', 'HAN', 'VCL', '');

-- --------------------------------------------------------

--
-- Table structure for table `vemaybay`
--

CREATE TABLE `vemaybay` (
  `MaCB` varchar(10) CHARACTER SET utf8 NOT NULL,
  `LoaiVe` varchar(20) CHARACTER SET utf8 NOT NULL,
  `GheNgoi` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MaKH` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MaCode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TinhTrang` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vemaybay`
--

INSERT INTO `vemaybay` (`MaCB`, `LoaiVe`, `GheNgoi`, `MaKH`, `MaCode`, `TinhTrang`) VALUES
('BL2925', 'Thương gia', '1A', '7894561230123', NULL, 'Đã thanh toán'),
('BL2925', 'Thương gia', '1B', 'TN-1132377182', NULL, 'Đã thanh toán'),
('BL2925', 'Thương gia', '2A', '0123456789123', NULL, 'Đã thanh toán'),
('BL2925', 'Thương gia', '2B', 'TE-1457322144', NULL, 'Đã thanh toán'),
('BL2925', 'Thương gia', '3A', '7894561231598', NULL, 'Đã thanh toán'),
('BL2925', 'Thương gia', '3B', '7894564567894', NULL, 'Đã thanh toán'),
('BL2925', 'Phổ thông', '4A', '0123456789123', NULL, 'Đã thanh toán'),
('BL2925', 'Phổ thông', '4B', NULL, NULL, 'Còn trống'),
('BL2925', 'Phổ thông', '5A', '0123456789123', NULL, 'Đã thanh toán'),
('BL2925', 'Phổ thông', '5B', NULL, NULL, 'Còn trống'),
('BL2925', 'Phổ thông', '6A', '0123456789123', NULL, 'Đã thanh toán'),
('BL2925', 'Phổ thông', '6B', '0123456789123', NULL, 'Đã thanh toán'),
('BL2925', 'Phổ thông', '7A', '0123456789123', NULL, 'Đã thanh toán'),
('BL2925', 'Phổ thông', '7B', '0123456789123', NULL, 'Đã thanh toán'),
('BL4894', 'Thương gia', '1A', '7589321452012', NULL, 'Đã thanh toán'),
('BL4894', 'Thương gia', '1B', 'TE-1208306296', NULL, 'Đã thanh toán'),
('BL4894', 'Thương gia', '2A', '4782365012368', NULL, 'Đã thanh toán'),
('BL4894', 'Thương gia', '2B', '7514863021485', NULL, 'Đã thanh toán'),
('BL4894', 'Thương gia', '3A', 'TE-1643291267', NULL, 'Đã thanh toán'),
('BL4894', 'Thương gia', '3B', '0123456789123', NULL, 'Đã thanh toán'),
('BL4894', 'Phổ thông', '4A', '0123456789123', NULL, 'Đã thanh toán'),
('BL4894', 'Phổ thông', '4B', 'TE-1338128927', NULL, 'Đã thanh toán'),
('BL4894', 'Phổ thông', '5A', '0123456789123', NULL, 'Đã thanh toán'),
('BL4894', 'Phổ thông', '5B', NULL, NULL, 'Còn trống'),
('BL4894', 'Phổ thông', '6A', NULL, NULL, 'Còn trống'),
('BL4894', 'Phổ thông', '6B', NULL, NULL, 'Còn trống'),
('BL4894', 'Phổ thông', '7A', NULL, NULL, 'Còn trống'),
('BL4894', 'Phổ thông', '7B', NULL, NULL, 'Còn trống'),
('BL7492', 'Thương gia', '1A', '0123456789123', NULL, 'Đã thanh toán'),
('BL7492', 'Thương gia', '1B', 'TE-1598916005', NULL, 'Đã thanh toán'),
('BL7492', 'Thương gia', '2A', '0123456789123', NULL, 'Đã thanh toán'),
('BL7492', 'Thương gia', '2B', 'TE-1865690344', NULL, 'Đã thanh toán'),
('BL7492', 'Thương gia', '3A', 'TE-1631287490', NULL, 'Đã thanh toán'),
('BL7492', 'Thương gia', '3B', 'TE-1826546421', NULL, 'Đã thanh toán'),
('BL7492', 'Phổ thông', '4A', '0123456789123', NULL, 'Đã thanh toán'),
('BL7492', 'Phổ thông', '4B', NULL, NULL, 'Còn trống'),
('BL7492', 'Phổ thông', '5A', '0123456789123', NULL, 'Đã thanh toán'),
('BL7492', 'Phổ thông', '5B', NULL, NULL, 'Còn trống'),
('BL7492', 'Phổ thông', '6A', NULL, NULL, 'Còn trống'),
('BL7492', 'Phổ thông', '6B', NULL, NULL, 'Còn trống'),
('BL7492', 'Phổ thông', '7A', NULL, NULL, 'Còn trống'),
('BL7492', 'Phổ thông', '7B', NULL, NULL, 'Còn trống'),
('BL7830', 'Thương gia', '10A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Thương gia', '10B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Thương gia', '11A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Thương gia', '11B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Thương gia', '12A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Thương gia', '12B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '13A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '13B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '14A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '14B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '15A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '15B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '16A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '16B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '1A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '1B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '2A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '2B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '3A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '3B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '4A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '4B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '5A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '5B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '6A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '6B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '7A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '7B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '8A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Phổ thông', '8B', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Thương gia', '9A', NULL, NULL, 'Đã bị hủy'),
('BL7830', 'Thương gia', '9B', NULL, NULL, 'Đã bị hủy'),
('BL9673', 'Thương gia', '10A', NULL, NULL, 'Còn trống'),
('BL9673', 'Thương gia', '10B', NULL, NULL, 'Còn trống'),
('BL9673', 'Thương gia', '11A', NULL, NULL, 'Còn trống'),
('BL9673', 'Thương gia', '11B', NULL, NULL, 'Còn trống'),
('BL9673', 'Thương gia', '12A', NULL, NULL, 'Còn trống'),
('BL9673', 'Thương gia', '12B', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '13A', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '13B', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '14A', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '14B', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '15A', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '15B', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '16A', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '16B', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '1A', '0123456789123', NULL, 'Đã thanh toán'),
('BL9673', 'Phổ thông', '1B', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '2A', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '2B', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '3A', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '3B', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '4A', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '4B', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '5A', '0123456789123', NULL, 'Đã thanh toán'),
('BL9673', 'Phổ thông', '5B', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '6A', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '6B', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '7A', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '7B', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '8A', NULL, NULL, 'Còn trống'),
('BL9673', 'Phổ thông', '8B', NULL, NULL, 'Còn trống'),
('BL9673', 'Thương gia', '9A', NULL, NULL, 'Còn trống'),
('BL9673', 'Thương gia', '9B', NULL, NULL, 'Còn trống'),
('QH1655', 'Thương gia', '10A', NULL, NULL, 'Còn trống'),
('QH1655', 'Thương gia', '10B', NULL, NULL, 'Còn trống'),
('QH1655', 'Thương gia', '11A', NULL, NULL, 'Còn trống'),
('QH1655', 'Thương gia', '11B', NULL, NULL, 'Còn trống'),
('QH1655', 'Thương gia', '12A', NULL, NULL, 'Còn trống'),
('QH1655', 'Thương gia', '12B', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '13A', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '13B', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '14A', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '14B', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '15A', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '15B', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '16A', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '16B', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '1A', '0123456789123', NULL, 'Đã thanh toán'),
('QH1655', 'Phổ thông', '1B', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '2A', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '2B', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '3A', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '3B', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '4A', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '4B', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '5A', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '5B', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '6A', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '6B', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '7A', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '7B', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '8A', NULL, NULL, 'Còn trống'),
('QH1655', 'Phổ thông', '8B', NULL, NULL, 'Còn trống'),
('QH1655', 'Thương gia', '9A', NULL, NULL, 'Còn trống'),
('QH1655', 'Thương gia', '9B', NULL, NULL, 'Còn trống'),
('QH6706', 'Thương gia', '10A', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Thương gia', '10B', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Thương gia', '11A', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Thương gia', '11B', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Thương gia', '12A', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Thương gia', '12B', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '13A', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '13B', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '14A', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '14B', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '15A', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '15B', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '16A', '0123456789123', NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '16B', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '1A', '4893697459821', NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '1B', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '2A', 'TE-1626971832', NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '2B', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '3A', 'TE-1597503394', NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '3B', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '4A', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '4B', '0123456789123', NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '5A', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '5B', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '6A', '0123456789123', NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '6B', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '7A', '0123456789123', NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '7B', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '8A', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Phổ thông', '8B', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Thương gia', '9A', NULL, NULL, 'Đã bị hủy'),
('QH6706', 'Thương gia', '9B', NULL, NULL, 'Đã bị hủy'),
('QH8496', 'Thương gia', '1A', '4862159875362', NULL, 'Đã thanh toán'),
('QH8496', 'Thương gia', '1B', '0930492312345', NULL, 'Đã thanh toán'),
('QH8496', 'Thương gia', '2A', '0123456789456', NULL, 'Đã thanh toán'),
('QH8496', 'Thương gia', '2B', '0932859323213', NULL, 'Đã thanh toán'),
('QH8496', 'Thương gia', '3A', '0584136985214', NULL, 'Đã thanh toán'),
('QH8496', 'Thương gia', '3B', '0948328793412', NULL, 'Đã thanh toán'),
('QH8496', 'Phổ thông', '4A', '0123456789123', NULL, 'Đã thanh toán'),
('QH8496', 'Phổ thông', '4B', '3247239852234', NULL, 'Đã thanh toán'),
('QH8496', 'Phổ thông', '5A', '3204923042312', NULL, 'Đã thanh toán'),
('QH8496', 'Phổ thông', '5B', NULL, NULL, 'Còn trống'),
('QH8496', 'Phổ thông', '6A', '3295697528392', NULL, 'Đã thanh toán'),
('QH8496', 'Phổ thông', '6B', NULL, NULL, 'Còn trống'),
('QH8496', 'Phổ thông', '7A', NULL, NULL, 'Còn trống'),
('QH8496', 'Phổ thông', '7B', NULL, NULL, 'Còn trống'),
('VJ1618', 'Thương gia', '1A', NULL, NULL, 'Còn trống'),
('VJ1618', 'Thương gia', '1B', NULL, NULL, 'Còn trống'),
('VJ1618', 'Thương gia', '2A', NULL, NULL, 'Còn trống'),
('VJ1618', 'Thương gia', '2B', NULL, NULL, 'Còn trống'),
('VJ1618', 'Thương gia', '3A', NULL, NULL, 'Còn trống'),
('VJ1618', 'Thương gia', '3B', NULL, NULL, 'Còn trống'),
('VJ1618', 'Phổ thông', '4A', NULL, NULL, 'Còn trống'),
('VJ1618', 'Phổ thông', '4B', NULL, NULL, 'Còn trống'),
('VJ1618', 'Phổ thông', '5A', NULL, NULL, 'Còn trống'),
('VJ1618', 'Phổ thông', '5B', NULL, NULL, 'Còn trống'),
('VJ1618', 'Phổ thông', '6A', NULL, NULL, 'Còn trống'),
('VJ1618', 'Phổ thông', '6B', NULL, NULL, 'Còn trống'),
('VJ1618', 'Phổ thông', '7A', NULL, NULL, 'Còn trống'),
('VJ1618', 'Phổ thông', '7B', NULL, NULL, 'Còn trống'),
('VJ3703', 'Thương gia', '10A', '9073432489212', NULL, 'Đã thanh toán'),
('VJ3703', 'Thương gia', '10B', '0948328793412', NULL, 'Đã thanh toán'),
('VJ3703', 'Thương gia', '11A', '4589374589733', NULL, 'Đã thanh toán'),
('VJ3703', 'Thương gia', '11B', 'TN-1138093344', NULL, 'Đã thanh toán'),
('VJ3703', 'Thương gia', '12A', NULL, NULL, 'Còn trống'),
('VJ3703', 'Thương gia', '12B', NULL, NULL, 'Còn trống'),
('VJ3703', 'Phổ thông', '13A', NULL, NULL, 'Còn trống'),
('VJ3703', 'Phổ thông', '13B', NULL, NULL, 'Còn trống'),
('VJ3703', 'Phổ thông', '14A', NULL, NULL, 'Còn trống'),
('VJ3703', 'Phổ thông', '14B', NULL, NULL, 'Còn trống'),
('VJ3703', 'Phổ thông', '15A', NULL, NULL, 'Còn trống'),
('VJ3703', 'Phổ thông', '15B', NULL, NULL, 'Còn trống'),
('VJ3703', 'Phổ thông', '16A', NULL, NULL, 'Còn trống'),
('VJ3703', 'Phổ thông', '16B', NULL, NULL, 'Còn trống'),
('VJ3703', 'Phổ thông', '1A', 'TN-1757005992', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '1B', '4983274827678', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '2A', '9283492834932', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '2B', 'TE-1659992983', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '3A', 'TN-1947461140', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '3B', 'TE-1160099092', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '4A', 'TN-1892481412', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '4B', 'TE-1151195381', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '5A', 'TE-1109311094', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '5B', 'TN-1844897221', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '6A', '2429427489123', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '6B', 'TE-1073341541', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '7A', 'TE-1159798221', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '7B', '0932859323213', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '8A', '3295697528392', NULL, 'Đã thanh toán'),
('VJ3703', 'Phổ thông', '8B', 'TN-1141179100', NULL, 'Đã thanh toán'),
('VJ3703', 'Thương gia', '9A', '9848238475893', NULL, 'Đã thanh toán'),
('VJ3703', 'Thương gia', '9B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Thương gia', '10A', NULL, NULL, 'Còn trống'),
('VJ3863', 'Thương gia', '10B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Thương gia', '11A', NULL, NULL, 'Còn trống'),
('VJ3863', 'Thương gia', '11B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Thương gia', '12A', NULL, NULL, 'Còn trống'),
('VJ3863', 'Thương gia', '12B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '13A', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '13B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '14A', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '14B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '15A', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '15B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '16A', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '16B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '1A', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '1B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '2A', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '2B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '3A', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '3B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '4A', '0123456789123', NULL, 'Đã thanh toán'),
('VJ3863', 'Phổ thông', '4B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '5A', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '5B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '6A', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '6B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '7A', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '7B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '8A', NULL, NULL, 'Còn trống'),
('VJ3863', 'Phổ thông', '8B', NULL, NULL, 'Còn trống'),
('VJ3863', 'Thương gia', '9A', '0123456789123', NULL, 'Đã thanh toán'),
('VJ3863', 'Thương gia', '9B', NULL, NULL, 'Còn trống'),
('VJ4882', 'Thương gia', '10A', 'TN-1430470020', NULL, 'Đã thanh toán'),
('VJ4882', 'Thương gia', '10B', '3247239852234', NULL, 'Đã thanh toán'),
('VJ4882', 'Thương gia', '11A', 'TE-1865743428', NULL, 'Đã thanh toán'),
('VJ4882', 'Thương gia', '11B', '2578935793445', NULL, 'Đã thanh toán'),
('VJ4882', 'Thương gia', '12A', 'TE-1315513174', NULL, 'Đã thanh toán'),
('VJ4882', 'Thương gia', '12B', '3248234234212', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '13A', 'TE-1249890232', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '13B', 'TE-1879502189', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '14A', '3204923042312', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '14B', 'TE-1934831164', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '15A', '9898965469456', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '15B', NULL, NULL, 'Còn trống'),
('VJ4882', 'Phổ thông', '16A', '9984590348523', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '16B', NULL, NULL, 'Còn trống'),
('VJ4882', 'Phổ thông', '1A', '2095834908512', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '1B', '1204983242912', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '2A', '4234234223454', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '2B', '4385345345534', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '3A', '9032829308421', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '3B', '0930492312345', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '4A', '3445345341234', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '4B', 'TE-1522686372', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '5A', 'TE-1796633873', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '5B', '2342409029341', NULL, 'Đã thanh toán'),
('VJ4882', 'Phổ thông', '6A', NULL, NULL, 'Còn trống'),
('VJ4882', 'Phổ thông', '6B', NULL, NULL, 'Còn trống'),
('VJ4882', 'Phổ thông', '7A', NULL, NULL, 'Còn trống'),
('VJ4882', 'Phổ thông', '7B', NULL, NULL, 'Còn trống'),
('VJ4882', 'Phổ thông', '8A', NULL, NULL, 'Còn trống'),
('VJ4882', 'Phổ thông', '8B', NULL, NULL, 'Còn trống'),
('VJ4882', 'Thương gia', '9A', 'TE-1736379815', NULL, 'Đã thanh toán'),
('VJ4882', 'Thương gia', '9B', '9485345439304', NULL, 'Đã thanh toán'),
('VJ4894', 'Thương gia', '10A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Thương gia', '10B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Thương gia', '11A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Thương gia', '11B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Thương gia', '12A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Thương gia', '12B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '13A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '13B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '14A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '14B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '15A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '15B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '16A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '16B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '1A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '1B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '2A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '2B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '3A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '3B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '4A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '4B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '5A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '5B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '6A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '6B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '7A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '7B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '8A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Phổ thông', '8B', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Thương gia', '9A', NULL, NULL, 'Đã bị hủy'),
('VJ4894', 'Thương gia', '9B', NULL, NULL, 'Đã bị hủy'),
('VJ8589', 'Thương gia', '1A', '0123456789123', NULL, 'Đã thanh toán'),
('VJ8589', 'Thương gia', '1B', '0123456789123', NULL, 'Đã thanh toán'),
('VJ8589', 'Thương gia', '2A', '7568945125478', NULL, 'Đã thanh toán'),
('VJ8589', 'Thương gia', '2B', NULL, NULL, 'Còn trống'),
('VJ8589', 'Thương gia', '3A', NULL, NULL, 'Còn trống'),
('VJ8589', 'Thương gia', '3B', NULL, NULL, 'Còn trống'),
('VJ8589', 'Phổ thông', '4A', NULL, NULL, 'Còn trống'),
('VJ8589', 'Phổ thông', '4B', NULL, NULL, 'Còn trống'),
('VJ8589', 'Phổ thông', '5A', '0123456789123', NULL, 'Đã thanh toán'),
('VJ8589', 'Phổ thông', '5B', NULL, NULL, 'Còn trống'),
('VJ8589', 'Phổ thông', '6A', NULL, NULL, 'Còn trống'),
('VJ8589', 'Phổ thông', '6B', NULL, NULL, 'Còn trống'),
('VJ8589', 'Phổ thông', '7A', '0123456789123', NULL, 'Đã thanh toán'),
('VJ8589', 'Phổ thông', '7B', NULL, NULL, 'Còn trống'),
('VN1838', 'Thương gia', '10A', NULL, NULL, 'Còn trống'),
('VN1838', 'Thương gia', '10B', NULL, NULL, 'Còn trống'),
('VN1838', 'Thương gia', '11A', NULL, NULL, 'Còn trống'),
('VN1838', 'Thương gia', '11B', NULL, NULL, 'Còn trống'),
('VN1838', 'Thương gia', '12A', NULL, NULL, 'Còn trống'),
('VN1838', 'Thương gia', '12B', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '13A', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '13B', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '14A', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '14B', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '15A', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '15B', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '16A', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '16B', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '1A', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '1B', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '2A', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '2B', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '3A', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '3B', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '4A', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '4B', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '5A', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '5B', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '6A', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '6B', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '7A', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '7B', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '8A', NULL, NULL, 'Còn trống'),
('VN1838', 'Phổ thông', '8B', NULL, NULL, 'Còn trống'),
('VN1838', 'Thương gia', '9A', NULL, NULL, 'Còn trống'),
('VN1838', 'Thương gia', '9B', NULL, NULL, 'Còn trống'),
('VN4541', 'Thương gia', '1A', '0123456789123', NULL, 'Đã thanh toán'),
('VN4541', 'Thương gia', '1B', NULL, NULL, 'Còn trống'),
('VN4541', 'Thương gia', '2A', NULL, NULL, 'Còn trống'),
('VN4541', 'Thương gia', '2B', NULL, NULL, 'Còn trống'),
('VN4541', 'Thương gia', '3A', NULL, NULL, 'Còn trống'),
('VN4541', 'Thương gia', '3B', NULL, NULL, 'Còn trống'),
('VN4541', 'Phổ thông', '4A', '0123456789123', NULL, 'Đã thanh toán'),
('VN4541', 'Phổ thông', '4B', NULL, NULL, 'Còn trống'),
('VN4541', 'Phổ thông', '5A', NULL, NULL, 'Còn trống'),
('VN4541', 'Phổ thông', '5B', NULL, NULL, 'Còn trống'),
('VN4541', 'Phổ thông', '6A', NULL, NULL, 'Còn trống'),
('VN4541', 'Phổ thông', '6B', NULL, NULL, 'Còn trống'),
('VN4541', 'Phổ thông', '7A', NULL, NULL, 'Còn trống'),
('VN4541', 'Phổ thông', '7B', NULL, NULL, 'Còn trống'),
('VN5868', 'Thương gia', '10A', '0123456789123', NULL, 'Đã thanh toán'),
('VN5868', 'Thương gia', '10B', 'TE-1513711479', NULL, 'Đã thanh toán'),
('VN5868', 'Thương gia', '11A', '7531594862456', NULL, 'Đã thanh toán'),
('VN5868', 'Thương gia', '11B', '1234567891235', NULL, 'Đã thanh toán'),
('VN5868', 'Thương gia', '12A', 'TE-1217238722', NULL, 'Đã thanh toán'),
('VN5868', 'Thương gia', '12B', '0123456789456', NULL, 'Đã thanh toán'),
('VN5868', 'Phổ thông', '13A', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '13B', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '14A', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '14B', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '15A', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '15B', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '16A', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '16B', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '1A', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '1B', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '2A', '0123456789123', NULL, 'Đã thanh toán'),
('VN5868', 'Phổ thông', '2B', 'TE-1861582291', NULL, 'Đã thanh toán'),
('VN5868', 'Phổ thông', '3A', 'TE-1156276307', NULL, 'Đã thanh toán'),
('VN5868', 'Phổ thông', '3B', 'TE-1239077925', NULL, 'Đã thanh toán'),
('VN5868', 'Phổ thông', '4A', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '4B', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '5A', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '5B', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '6A', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '6B', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '7A', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '7B', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '8A', NULL, NULL, 'Còn trống'),
('VN5868', 'Phổ thông', '8B', NULL, NULL, 'Còn trống'),
('VN5868', 'Thương gia', '9A', '0123456789123', NULL, 'Đã thanh toán'),
('VN5868', 'Thương gia', '9B', '0123456789123', NULL, 'Đã thanh toán'),
('VN7632', 'Thương gia', '10A', '0123456789123', NULL, 'Đã thanh toán'),
('VN7632', 'Thương gia', '10B', 'TE-1784308007', NULL, 'Đã thanh toán'),
('VN7632', 'Thương gia', '11A', '0123456789123', NULL, 'Đã thanh toán'),
('VN7632', 'Thương gia', '11B', '8753698521456', NULL, 'Đã thanh toán'),
('VN7632', 'Thương gia', '12A', '8746148625987', NULL, 'Đã thanh toán'),
('VN7632', 'Thương gia', '12B', 'TE-1085946287', NULL, 'Đã thanh toán'),
('VN7632', 'Phổ thông', '13A', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '13B', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '14A', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '14B', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '15A', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '15B', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '16A', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '16B', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '1A', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '1B', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '2A', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '2B', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '3A', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '3B', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '4A', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '4B', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '5A', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '5B', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '6A', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '6B', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '7A', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '7B', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '8A', NULL, NULL, 'Còn trống'),
('VN7632', 'Phổ thông', '8B', NULL, NULL, 'Còn trống'),
('VN7632', 'Thương gia', '9A', 'TN-1219331237', NULL, 'Đã thanh toán'),
('VN7632', 'Thương gia', '9B', '0584136985214', NULL, 'Đã thanh toán'),
('VN7642', 'Thương gia', '10A', '0123456789123', NULL, 'Đã thanh toán'),
('VN7642', 'Thương gia', '10B', NULL, NULL, 'Còn trống'),
('VN7642', 'Thương gia', '11A', NULL, NULL, 'Còn trống'),
('VN7642', 'Thương gia', '11B', NULL, NULL, 'Còn trống'),
('VN7642', 'Thương gia', '12A', NULL, NULL, 'Còn trống'),
('VN7642', 'Thương gia', '12B', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '13A', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '13B', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '14A', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '14B', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '15A', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '15B', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '16A', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '16B', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '1A', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '1B', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '2A', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '2B', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '3A', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '3B', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '4A', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '4B', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '5A', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '5B', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '6A', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '6B', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '7A', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '7B', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '8A', NULL, NULL, 'Còn trống'),
('VN7642', 'Phổ thông', '8B', NULL, NULL, 'Còn trống'),
('VN7642', 'Thương gia', '9A', NULL, NULL, 'Còn trống'),
('VN7642', 'Thương gia', '9B', NULL, NULL, 'Còn trống');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chitiethoadon`
--
ALTER TABLE `chitiethoadon`
  ADD KEY `MaHD` (`MaHD`),
  ADD KEY `MaCB` (`MaCB`,`GheNgoi`),
  ADD KEY `MaHanhLy` (`MaHanhLy`);

--
-- Indexes for table `chuongtrinhkm`
--
ALTER TABLE `chuongtrinhkm`
  ADD PRIMARY KEY (`MaCTKM`);

--
-- Indexes for table `chuyenbay`
--
ALTER TABLE `chuyenbay`
  ADD PRIMARY KEY (`MaCB`),
  ADD KEY `MaTuyenBay` (`MaTuyenBay`),
  ADD KEY `MaHang` (`MaHang`),
  ADD KEY `MaMB` (`MaMB`);

--
-- Indexes for table `codekm`
--
ALTER TABLE `codekm`
  ADD PRIMARY KEY (`MaCode`),
  ADD KEY `MaCTKM` (`MaCTKM`);

--
-- Indexes for table `hanghangkhong`
--
ALTER TABLE `hanghangkhong`
  ADD PRIMARY KEY (`MaHang`);

--
-- Indexes for table `hanhly`
--
ALTER TABLE `hanhly`
  ADD PRIMARY KEY (`MaHanhLy`);

--
-- Indexes for table `hoadon`
--
ALTER TABLE `hoadon`
  ADD PRIMARY KEY (`MaHD`),
  ADD KEY `MaNV` (`MaNV`),
  ADD KEY `MaKH` (`MaKH`);

--
-- Indexes for table `khachhang`
--
ALTER TABLE `khachhang`
  ADD PRIMARY KEY (`MaKH`);

--
-- Indexes for table `maybay`
--
ALTER TABLE `maybay`
  ADD PRIMARY KEY (`MaMB`),
  ADD KEY `fk_mhmb_MaMHMB` (`MaMHMB`);

--
-- Indexes for table `mohinhmaybay`
--
ALTER TABLE `mohinhmaybay`
  ADD PRIMARY KEY (`MaMHMB`);

--
-- Indexes for table `nhanvien`
--
ALTER TABLE `nhanvien`
  ADD PRIMARY KEY (`MaNV`),
  ADD UNIQUE KEY `Gmail` (`Gmail`),
  ADD UNIQUE KEY `SDT` (`SDT`);

--
-- Indexes for table `sanbay`
--
ALTER TABLE `sanbay`
  ADD PRIMARY KEY (`MaSanBay`);

--
-- Indexes for table `tuyenbay`
--
ALTER TABLE `tuyenbay`
  ADD PRIMARY KEY (`MaTuyenBay`),
  ADD KEY `MaSBCatCanh` (`MaSBCatCanh`),
  ADD KEY `MaSBHaCanh` (`MaSBHaCanh`),
  ADD KEY `MaSBTrungGian` (`MaSBTrungGian`);

--
-- Indexes for table `vemaybay`
--
ALTER TABLE `vemaybay`
  ADD PRIMARY KEY (`MaCB`,`GheNgoi`),
  ADD KEY `MaCode` (`MaCode`),
  ADD KEY `MaKH` (`MaKH`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chitiethoadon`
--
ALTER TABLE `chitiethoadon`
  ADD CONSTRAINT `chitiethoadon_ibfk_1` FOREIGN KEY (`MaHD`) REFERENCES `hoadon` (`MaHD`),
  ADD CONSTRAINT `chitiethoadon_ibfk_2` FOREIGN KEY (`MaCB`,`GheNgoi`) REFERENCES `vemaybay` (`MaCB`, `GheNgoi`),
  ADD CONSTRAINT `chitiethoadon_ibfk_3` FOREIGN KEY (`MaHanhLy`) REFERENCES `hanhly` (`MaHanhLy`);

--
-- Constraints for table `chuyenbay`
--
ALTER TABLE `chuyenbay`
  ADD CONSTRAINT `chuyenbay_ibfk_1` FOREIGN KEY (`MaTuyenBay`) REFERENCES `tuyenbay` (`MaTuyenBay`),
  ADD CONSTRAINT `chuyenbay_ibfk_2` FOREIGN KEY (`MaHang`) REFERENCES `hanghangkhong` (`MaHang`),
  ADD CONSTRAINT `chuyenbay_ibfk_3` FOREIGN KEY (`MaMB`) REFERENCES `maybay` (`MaMB`);

--
-- Constraints for table `codekm`
--
ALTER TABLE `codekm`
  ADD CONSTRAINT `MaCTKM` FOREIGN KEY (`MaCTKM`) REFERENCES `chuongtrinhkm` (`MaCTKM`);

--
-- Constraints for table `hoadon`
--
ALTER TABLE `hoadon`
  ADD CONSTRAINT `hoadon_ibfk_1` FOREIGN KEY (`MaNV`) REFERENCES `nhanvien` (`MaNV`),
  ADD CONSTRAINT `hoadon_ibfk_2` FOREIGN KEY (`MaKH`) REFERENCES `khachhang` (`MaKH`);

--
-- Constraints for table `maybay`
--
ALTER TABLE `maybay`
  ADD CONSTRAINT `fk_mhmb_MaMHMB` FOREIGN KEY (`MaMHMB`) REFERENCES `mohinhmaybay` (`MaMHMB`);

--
-- Constraints for table `tuyenbay`
--
ALTER TABLE `tuyenbay`
  ADD CONSTRAINT `tuyenbay_ibfk_1` FOREIGN KEY (`MaSBCatCanh`) REFERENCES `sanbay` (`MaSanBay`),
  ADD CONSTRAINT `tuyenbay_ibfk_2` FOREIGN KEY (`MaSBHaCanh`) REFERENCES `sanbay` (`MaSanBay`),
  ADD CONSTRAINT `tuyenbay_ibfk_3` FOREIGN KEY (`MaSBTrungGian`) REFERENCES `sanbay` (`MaSanBay`);

--
-- Constraints for table `vemaybay`
--
ALTER TABLE `vemaybay`
  ADD CONSTRAINT `vemaybay_ibfk_1` FOREIGN KEY (`MaCB`) REFERENCES `chuyenbay` (`MaCB`),
  ADD CONSTRAINT `vemaybay_ibfk_2` FOREIGN KEY (`MaCode`) REFERENCES `codekm` (`MaCode`),
  ADD CONSTRAINT `vemaybay_ibfk_3` FOREIGN KEY (`MaKH`) REFERENCES `khachhang` (`MaKH`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
